/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { VendedorComponentsPage, VendedorDeleteDialog, VendedorUpdatePage } from './vendedor.page-object';
import * as path from 'path';

const expect = chai.expect;

describe('Vendedor e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let vendedorUpdatePage: VendedorUpdatePage;
    let vendedorComponentsPage: VendedorComponentsPage;
    let vendedorDeleteDialog: VendedorDeleteDialog;
    const fileNameToUpload = 'logo-jhipster.png';
    const fileToUpload = '../../../../../main/webapp/content/images/' + fileNameToUpload;
    const absolutePath = path.resolve(__dirname, fileToUpload);

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Vendedors', async () => {
        await navBarPage.goToEntity('vendedor');
        vendedorComponentsPage = new VendedorComponentsPage();
        await browser.wait(ec.visibilityOf(vendedorComponentsPage.title), 5000);
        expect(await vendedorComponentsPage.getTitle()).to.eq('pagboletosApp.vendedor.home.title');
    });

    it('should load create Vendedor page', async () => {
        await vendedorComponentsPage.clickOnCreateButton();
        vendedorUpdatePage = new VendedorUpdatePage();
        expect(await vendedorUpdatePage.getPageTitle()).to.eq('pagboletosApp.vendedor.home.createOrEditLabel');
        await vendedorUpdatePage.cancel();
    });

    it('should create and save Vendedors', async () => {
        const nbButtonsBeforeCreate = await vendedorComponentsPage.countDeleteButtons();

        await vendedorComponentsPage.clickOnCreateButton();
        await promise.all([
            vendedorUpdatePage.setIdVendedorInput('idVendedor'),
            vendedorUpdatePage.setSelfieInput(absolutePath),
            vendedorUpdatePage.localizacaoSelectLastOption()
        ]);
        expect(await vendedorUpdatePage.getIdVendedorInput()).to.eq('idVendedor');
        expect(await vendedorUpdatePage.getSelfieInput()).to.endsWith(fileNameToUpload);
        await vendedorUpdatePage.save();
        expect(await vendedorUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await vendedorComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Vendedor', async () => {
        const nbButtonsBeforeDelete = await vendedorComponentsPage.countDeleteButtons();
        await vendedorComponentsPage.clickOnLastDeleteButton();

        vendedorDeleteDialog = new VendedorDeleteDialog();
        expect(await vendedorDeleteDialog.getDialogTitle()).to.eq('pagboletosApp.vendedor.delete.question');
        await vendedorDeleteDialog.clickOnConfirmButton();

        expect(await vendedorComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
