import { by, element, ElementFinder } from 'protractor';

export class VendedorComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-vendedor div table .btn-danger'));
    title = element.all(by.css('jhi-vendedor div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class VendedorUpdatePage {
    pageTitle = element(by.id('jhi-vendedor-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    idVendedorInput = element(by.id('field_idVendedor'));
    selfieInput = element(by.id('file_selfie'));
    localizacaoSelect = element(by.id('field_localizacao'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setIdVendedorInput(idVendedor) {
        await this.idVendedorInput.sendKeys(idVendedor);
    }

    async getIdVendedorInput() {
        return this.idVendedorInput.getAttribute('value');
    }

    async setSelfieInput(selfie) {
        await this.selfieInput.sendKeys(selfie);
    }

    async getSelfieInput() {
        return this.selfieInput.getAttribute('value');
    }

    async localizacaoSelectLastOption() {
        await this.localizacaoSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async localizacaoSelectOption(option) {
        await this.localizacaoSelect.sendKeys(option);
    }

    getLocalizacaoSelect(): ElementFinder {
        return this.localizacaoSelect;
    }

    async getLocalizacaoSelectedOption() {
        return this.localizacaoSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class VendedorDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-vendedor-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-vendedor'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
