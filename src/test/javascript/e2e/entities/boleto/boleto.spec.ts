/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { BoletoComponentsPage, BoletoDeleteDialog, BoletoUpdatePage } from './boleto.page-object';

const expect = chai.expect;

describe('Boleto e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let boletoUpdatePage: BoletoUpdatePage;
    let boletoComponentsPage: BoletoComponentsPage;
    let boletoDeleteDialog: BoletoDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Boletos', async () => {
        await navBarPage.goToEntity('boleto');
        boletoComponentsPage = new BoletoComponentsPage();
        await browser.wait(ec.visibilityOf(boletoComponentsPage.title), 5000);
        expect(await boletoComponentsPage.getTitle()).to.eq('pagboletosApp.boleto.home.title');
    });

    it('should load create Boleto page', async () => {
        await boletoComponentsPage.clickOnCreateButton();
        boletoUpdatePage = new BoletoUpdatePage();
        expect(await boletoUpdatePage.getPageTitle()).to.eq('pagboletosApp.boleto.home.createOrEditLabel');
        await boletoUpdatePage.cancel();
    });

    it('should create and save Boletos', async () => {
        const nbButtonsBeforeCreate = await boletoComponentsPage.countDeleteButtons();

        await boletoComponentsPage.clickOnCreateButton();
        await promise.all([
            boletoUpdatePage.setIdBoletosInput('idBoletos'),
            boletoUpdatePage.setCodigoDeBarrasInput('codigoDeBarras'),
            boletoUpdatePage.setVencimentoInput('2000-12-31'),
            boletoUpdatePage.setBeneficiarioInput('beneficiario'),
            boletoUpdatePage.setDescricaoInput('descricao'),
            boletoUpdatePage.setDataDePagamentoInput('2000-12-31'),
            boletoUpdatePage.pagamentoSelectLastOption(),
            boletoUpdatePage.vendedorSelectLastOption()
        ]);
        expect(await boletoUpdatePage.getIdBoletosInput()).to.eq('idBoletos');
        expect(await boletoUpdatePage.getCodigoDeBarrasInput()).to.eq('codigoDeBarras');
        expect(await boletoUpdatePage.getVencimentoInput()).to.eq('2000-12-31');
        expect(await boletoUpdatePage.getBeneficiarioInput()).to.eq('beneficiario');
        expect(await boletoUpdatePage.getDescricaoInput()).to.eq('descricao');
        expect(await boletoUpdatePage.getDataDePagamentoInput()).to.eq('2000-12-31');
        await boletoUpdatePage.save();
        expect(await boletoUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await boletoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Boleto', async () => {
        const nbButtonsBeforeDelete = await boletoComponentsPage.countDeleteButtons();
        await boletoComponentsPage.clickOnLastDeleteButton();

        boletoDeleteDialog = new BoletoDeleteDialog();
        expect(await boletoDeleteDialog.getDialogTitle()).to.eq('pagboletosApp.boleto.delete.question');
        await boletoDeleteDialog.clickOnConfirmButton();

        expect(await boletoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
