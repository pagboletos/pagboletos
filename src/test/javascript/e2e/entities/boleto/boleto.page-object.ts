import { by, element, ElementFinder } from 'protractor';

export class BoletoComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-boleto div table .btn-danger'));
    title = element.all(by.css('jhi-boleto div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class BoletoUpdatePage {
    pageTitle = element(by.id('jhi-boleto-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    idBoletosInput = element(by.id('field_idBoletos'));
    codigoDeBarrasInput = element(by.id('field_codigoDeBarras'));
    vencimentoInput = element(by.id('field_vencimento'));
    beneficiarioInput = element(by.id('field_beneficiario'));
    descricaoInput = element(by.id('field_descricao'));
    dataDePagamentoInput = element(by.id('field_dataDePagamento'));
    pagamentoSelect = element(by.id('field_pagamento'));
    vendedorSelect = element(by.id('field_vendedor'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setIdBoletosInput(idBoletos) {
        await this.idBoletosInput.sendKeys(idBoletos);
    }

    async getIdBoletosInput() {
        return this.idBoletosInput.getAttribute('value');
    }

    async setCodigoDeBarrasInput(codigoDeBarras) {
        await this.codigoDeBarrasInput.sendKeys(codigoDeBarras);
    }

    async getCodigoDeBarrasInput() {
        return this.codigoDeBarrasInput.getAttribute('value');
    }

    async setVencimentoInput(vencimento) {
        await this.vencimentoInput.sendKeys(vencimento);
    }

    async getVencimentoInput() {
        return this.vencimentoInput.getAttribute('value');
    }

    async setBeneficiarioInput(beneficiario) {
        await this.beneficiarioInput.sendKeys(beneficiario);
    }

    async getBeneficiarioInput() {
        return this.beneficiarioInput.getAttribute('value');
    }

    async setDescricaoInput(descricao) {
        await this.descricaoInput.sendKeys(descricao);
    }

    async getDescricaoInput() {
        return this.descricaoInput.getAttribute('value');
    }

    async setDataDePagamentoInput(dataDePagamento) {
        await this.dataDePagamentoInput.sendKeys(dataDePagamento);
    }

    async getDataDePagamentoInput() {
        return this.dataDePagamentoInput.getAttribute('value');
    }

    async pagamentoSelectLastOption() {
        await this.pagamentoSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async pagamentoSelectOption(option) {
        await this.pagamentoSelect.sendKeys(option);
    }

    getPagamentoSelect(): ElementFinder {
        return this.pagamentoSelect;
    }

    async getPagamentoSelectedOption() {
        return this.pagamentoSelect.element(by.css('option:checked')).getText();
    }

    async vendedorSelectLastOption() {
        await this.vendedorSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async vendedorSelectOption(option) {
        await this.vendedorSelect.sendKeys(option);
    }

    getVendedorSelect(): ElementFinder {
        return this.vendedorSelect;
    }

    async getVendedorSelectedOption() {
        return this.vendedorSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class BoletoDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-boleto-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-boleto'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
