/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { PagamentoComponentsPage, PagamentoDeleteDialog, PagamentoUpdatePage } from './pagamento.page-object';

const expect = chai.expect;

describe('Pagamento e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let pagamentoUpdatePage: PagamentoUpdatePage;
    let pagamentoComponentsPage: PagamentoComponentsPage;
    let pagamentoDeleteDialog: PagamentoDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Pagamentos', async () => {
        await navBarPage.goToEntity('pagamento');
        pagamentoComponentsPage = new PagamentoComponentsPage();
        await browser.wait(ec.visibilityOf(pagamentoComponentsPage.title), 5000);
        expect(await pagamentoComponentsPage.getTitle()).to.eq('pagboletosApp.pagamento.home.title');
    });

    it('should load create Pagamento page', async () => {
        await pagamentoComponentsPage.clickOnCreateButton();
        pagamentoUpdatePage = new PagamentoUpdatePage();
        expect(await pagamentoUpdatePage.getPageTitle()).to.eq('pagboletosApp.pagamento.home.createOrEditLabel');
        await pagamentoUpdatePage.cancel();
    });

    it('should create and save Pagamentos', async () => {
        const nbButtonsBeforeCreate = await pagamentoComponentsPage.countDeleteButtons();

        await pagamentoComponentsPage.clickOnCreateButton();
        await promise.all([
            pagamentoUpdatePage.setIdTransacaoCartaoInput('idTransacaoCartao'),
            pagamentoUpdatePage.setIdTransacaoBoletoInput('idTransacaoBoleto')
        ]);
        expect(await pagamentoUpdatePage.getIdTransacaoCartaoInput()).to.eq('idTransacaoCartao');
        expect(await pagamentoUpdatePage.getIdTransacaoBoletoInput()).to.eq('idTransacaoBoleto');
        await pagamentoUpdatePage.save();
        expect(await pagamentoUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await pagamentoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Pagamento', async () => {
        const nbButtonsBeforeDelete = await pagamentoComponentsPage.countDeleteButtons();
        await pagamentoComponentsPage.clickOnLastDeleteButton();

        pagamentoDeleteDialog = new PagamentoDeleteDialog();
        expect(await pagamentoDeleteDialog.getDialogTitle()).to.eq('pagboletosApp.pagamento.delete.question');
        await pagamentoDeleteDialog.clickOnConfirmButton();

        expect(await pagamentoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
