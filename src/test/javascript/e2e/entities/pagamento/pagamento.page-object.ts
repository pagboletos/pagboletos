import { by, element, ElementFinder } from 'protractor';

export class PagamentoComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-pagamento div table .btn-danger'));
    title = element.all(by.css('jhi-pagamento div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class PagamentoUpdatePage {
    pageTitle = element(by.id('jhi-pagamento-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    idTransacaoCartaoInput = element(by.id('field_idTransacaoCartao'));
    idTransacaoBoletoInput = element(by.id('field_idTransacaoBoleto'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setIdTransacaoCartaoInput(idTransacaoCartao) {
        await this.idTransacaoCartaoInput.sendKeys(idTransacaoCartao);
    }

    async getIdTransacaoCartaoInput() {
        return this.idTransacaoCartaoInput.getAttribute('value');
    }

    async setIdTransacaoBoletoInput(idTransacaoBoleto) {
        await this.idTransacaoBoletoInput.sendKeys(idTransacaoBoleto);
    }

    async getIdTransacaoBoletoInput() {
        return this.idTransacaoBoletoInput.getAttribute('value');
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class PagamentoDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-pagamento-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-pagamento'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
