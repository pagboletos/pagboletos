/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { EtapaComponentsPage, EtapaDeleteDialog, EtapaUpdatePage } from './etapa.page-object';

const expect = chai.expect;

describe('Etapa e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let etapaUpdatePage: EtapaUpdatePage;
    let etapaComponentsPage: EtapaComponentsPage;
    let etapaDeleteDialog: EtapaDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Etapas', async () => {
        await navBarPage.goToEntity('etapa');
        etapaComponentsPage = new EtapaComponentsPage();
        await browser.wait(ec.visibilityOf(etapaComponentsPage.title), 5000);
        expect(await etapaComponentsPage.getTitle()).to.eq('pagboletosApp.etapa.home.title');
    });

    it('should load create Etapa page', async () => {
        await etapaComponentsPage.clickOnCreateButton();
        etapaUpdatePage = new EtapaUpdatePage();
        expect(await etapaUpdatePage.getPageTitle()).to.eq('pagboletosApp.etapa.home.createOrEditLabel');
        await etapaUpdatePage.cancel();
    });

    it('should create and save Etapas', async () => {
        const nbButtonsBeforeCreate = await etapaComponentsPage.countDeleteButtons();

        await etapaComponentsPage.clickOnCreateButton();
        await promise.all([
            etapaUpdatePage.setNomeInput('nome'),
            etapaUpdatePage.setDescricaoInput('descricao'),
            etapaUpdatePage.setDataEfetuacaoInput('2000-12-31'),
            etapaUpdatePage.pagamentoSelectLastOption()
        ]);
        expect(await etapaUpdatePage.getNomeInput()).to.eq('nome');
        expect(await etapaUpdatePage.getDescricaoInput()).to.eq('descricao');
        expect(await etapaUpdatePage.getDataEfetuacaoInput()).to.eq('2000-12-31');
        const selectedFinalizado = etapaUpdatePage.getFinalizadoInput();
        if (await selectedFinalizado.isSelected()) {
            await etapaUpdatePage.getFinalizadoInput().click();
            expect(await etapaUpdatePage.getFinalizadoInput().isSelected()).to.be.false;
        } else {
            await etapaUpdatePage.getFinalizadoInput().click();
            expect(await etapaUpdatePage.getFinalizadoInput().isSelected()).to.be.true;
        }
        await etapaUpdatePage.save();
        expect(await etapaUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await etapaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Etapa', async () => {
        const nbButtonsBeforeDelete = await etapaComponentsPage.countDeleteButtons();
        await etapaComponentsPage.clickOnLastDeleteButton();

        etapaDeleteDialog = new EtapaDeleteDialog();
        expect(await etapaDeleteDialog.getDialogTitle()).to.eq('pagboletosApp.etapa.delete.question');
        await etapaDeleteDialog.clickOnConfirmButton();

        expect(await etapaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
