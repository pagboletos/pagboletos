import { by, element, ElementFinder } from 'protractor';

export class EtapaComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-etapa div table .btn-danger'));
    title = element.all(by.css('jhi-etapa div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class EtapaUpdatePage {
    pageTitle = element(by.id('jhi-etapa-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    nomeInput = element(by.id('field_nome'));
    descricaoInput = element(by.id('field_descricao'));
    dataEfetuacaoInput = element(by.id('field_dataEfetuacao'));
    finalizadoInput = element(by.id('field_finalizado'));
    pagamentoSelect = element(by.id('field_pagamento'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setNomeInput(nome) {
        await this.nomeInput.sendKeys(nome);
    }

    async getNomeInput() {
        return this.nomeInput.getAttribute('value');
    }

    async setDescricaoInput(descricao) {
        await this.descricaoInput.sendKeys(descricao);
    }

    async getDescricaoInput() {
        return this.descricaoInput.getAttribute('value');
    }

    async setDataEfetuacaoInput(dataEfetuacao) {
        await this.dataEfetuacaoInput.sendKeys(dataEfetuacao);
    }

    async getDataEfetuacaoInput() {
        return this.dataEfetuacaoInput.getAttribute('value');
    }

    getFinalizadoInput() {
        return this.finalizadoInput;
    }

    async pagamentoSelectLastOption() {
        await this.pagamentoSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async pagamentoSelectOption(option) {
        await this.pagamentoSelect.sendKeys(option);
    }

    getPagamentoSelect(): ElementFinder {
        return this.pagamentoSelect;
    }

    async getPagamentoSelectedOption() {
        return this.pagamentoSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class EtapaDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-etapa-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-etapa'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
