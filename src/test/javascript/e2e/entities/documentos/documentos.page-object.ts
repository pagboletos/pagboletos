import { by, element, ElementFinder } from 'protractor';

export class DocumentosComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-documentos div table .btn-danger'));
    title = element.all(by.css('jhi-documentos div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class DocumentosUpdatePage {
    pageTitle = element(by.id('jhi-documentos-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    idDocumentoInput = element(by.id('field_idDocumento'));
    nomeInput = element(by.id('field_nome'));
    categoriaSelect = element(by.id('field_categoria'));
    imagemInput = element(by.id('file_imagem'));
    statusInput = element(by.id('field_status'));
    vendedorSelect = element(by.id('field_vendedor'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setIdDocumentoInput(idDocumento) {
        await this.idDocumentoInput.sendKeys(idDocumento);
    }

    async getIdDocumentoInput() {
        return this.idDocumentoInput.getAttribute('value');
    }

    async setNomeInput(nome) {
        await this.nomeInput.sendKeys(nome);
    }

    async getNomeInput() {
        return this.nomeInput.getAttribute('value');
    }

    async setCategoriaSelect(categoria) {
        await this.categoriaSelect.sendKeys(categoria);
    }

    async getCategoriaSelect() {
        return this.categoriaSelect.element(by.css('option:checked')).getText();
    }

    async categoriaSelectLastOption() {
        await this.categoriaSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setImagemInput(imagem) {
        await this.imagemInput.sendKeys(imagem);
    }

    async getImagemInput() {
        return this.imagemInput.getAttribute('value');
    }

    getStatusInput() {
        return this.statusInput;
    }

    async vendedorSelectLastOption() {
        await this.vendedorSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async vendedorSelectOption(option) {
        await this.vendedorSelect.sendKeys(option);
    }

    getVendedorSelect(): ElementFinder {
        return this.vendedorSelect;
    }

    async getVendedorSelectedOption() {
        return this.vendedorSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class DocumentosDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-documentos-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-documentos'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
