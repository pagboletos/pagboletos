/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { DocumentosComponentsPage, DocumentosDeleteDialog, DocumentosUpdatePage } from './documentos.page-object';
import * as path from 'path';

const expect = chai.expect;

describe('Documentos e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let documentosUpdatePage: DocumentosUpdatePage;
    let documentosComponentsPage: DocumentosComponentsPage;
    let documentosDeleteDialog: DocumentosDeleteDialog;
    const fileNameToUpload = 'logo-jhipster.png';
    const fileToUpload = '../../../../../main/webapp/content/images/' + fileNameToUpload;
    const absolutePath = path.resolve(__dirname, fileToUpload);

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Documentos', async () => {
        await navBarPage.goToEntity('documentos');
        documentosComponentsPage = new DocumentosComponentsPage();
        await browser.wait(ec.visibilityOf(documentosComponentsPage.title), 5000);
        expect(await documentosComponentsPage.getTitle()).to.eq('pagboletosApp.documentos.home.title');
    });

    it('should load create Documentos page', async () => {
        await documentosComponentsPage.clickOnCreateButton();
        documentosUpdatePage = new DocumentosUpdatePage();
        expect(await documentosUpdatePage.getPageTitle()).to.eq('pagboletosApp.documentos.home.createOrEditLabel');
        await documentosUpdatePage.cancel();
    });

    it('should create and save Documentos', async () => {
        const nbButtonsBeforeCreate = await documentosComponentsPage.countDeleteButtons();

        await documentosComponentsPage.clickOnCreateButton();
        await promise.all([
            documentosUpdatePage.setIdDocumentoInput('idDocumento'),
            documentosUpdatePage.setNomeInput('nome'),
            documentosUpdatePage.categoriaSelectLastOption(),
            documentosUpdatePage.setImagemInput(absolutePath),
            documentosUpdatePage.vendedorSelectLastOption()
        ]);
        expect(await documentosUpdatePage.getIdDocumentoInput()).to.eq('idDocumento');
        expect(await documentosUpdatePage.getNomeInput()).to.eq('nome');
        expect(await documentosUpdatePage.getImagemInput()).to.endsWith(fileNameToUpload);
        const selectedStatus = documentosUpdatePage.getStatusInput();
        if (await selectedStatus.isSelected()) {
            await documentosUpdatePage.getStatusInput().click();
            expect(await documentosUpdatePage.getStatusInput().isSelected()).to.be.false;
        } else {
            await documentosUpdatePage.getStatusInput().click();
            expect(await documentosUpdatePage.getStatusInput().isSelected()).to.be.true;
        }
        await documentosUpdatePage.save();
        expect(await documentosUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await documentosComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Documentos', async () => {
        const nbButtonsBeforeDelete = await documentosComponentsPage.countDeleteButtons();
        await documentosComponentsPage.clickOnLastDeleteButton();

        documentosDeleteDialog = new DocumentosDeleteDialog();
        expect(await documentosDeleteDialog.getDialogTitle()).to.eq('pagboletosApp.documentos.delete.question');
        await documentosDeleteDialog.clickOnConfirmButton();

        expect(await documentosComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
