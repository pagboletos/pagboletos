/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { CartaoComponentsPage, CartaoDeleteDialog, CartaoUpdatePage } from './cartao.page-object';

const expect = chai.expect;

describe('Cartao e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let cartaoUpdatePage: CartaoUpdatePage;
    let cartaoComponentsPage: CartaoComponentsPage;
    let cartaoDeleteDialog: CartaoDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Cartaos', async () => {
        await navBarPage.goToEntity('cartao');
        cartaoComponentsPage = new CartaoComponentsPage();
        await browser.wait(ec.visibilityOf(cartaoComponentsPage.title), 5000);
        expect(await cartaoComponentsPage.getTitle()).to.eq('pagboletosApp.cartao.home.title');
    });

    it('should load create Cartao page', async () => {
        await cartaoComponentsPage.clickOnCreateButton();
        cartaoUpdatePage = new CartaoUpdatePage();
        expect(await cartaoUpdatePage.getPageTitle()).to.eq('pagboletosApp.cartao.home.createOrEditLabel');
        await cartaoUpdatePage.cancel();
    });

    it('should create and save Cartaos', async () => {
        const nbButtonsBeforeCreate = await cartaoComponentsPage.countDeleteButtons();

        await cartaoComponentsPage.clickOnCreateButton();
        await promise.all([cartaoUpdatePage.setIdTokenInput('idToken'), cartaoUpdatePage.vendedorSelectLastOption()]);
        expect(await cartaoUpdatePage.getIdTokenInput()).to.eq('idToken');
        await cartaoUpdatePage.save();
        expect(await cartaoUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await cartaoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Cartao', async () => {
        const nbButtonsBeforeDelete = await cartaoComponentsPage.countDeleteButtons();
        await cartaoComponentsPage.clickOnLastDeleteButton();

        cartaoDeleteDialog = new CartaoDeleteDialog();
        expect(await cartaoDeleteDialog.getDialogTitle()).to.eq('pagboletosApp.cartao.delete.question');
        await cartaoDeleteDialog.clickOnConfirmButton();

        expect(await cartaoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
