import { by, element, ElementFinder } from 'protractor';

export class CartaoComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-cartao div table .btn-danger'));
    title = element.all(by.css('jhi-cartao div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class CartaoUpdatePage {
    pageTitle = element(by.id('jhi-cartao-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    idTokenInput = element(by.id('field_idToken'));
    vendedorSelect = element(by.id('field_vendedor'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setIdTokenInput(idToken) {
        await this.idTokenInput.sendKeys(idToken);
    }

    async getIdTokenInput() {
        return this.idTokenInput.getAttribute('value');
    }

    async vendedorSelectLastOption() {
        await this.vendedorSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async vendedorSelectOption(option) {
        await this.vendedorSelect.sendKeys(option);
    }

    getVendedorSelect(): ElementFinder {
        return this.vendedorSelect;
    }

    async getVendedorSelectedOption() {
        return this.vendedorSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class CartaoDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-cartao-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-cartao'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
