/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ParcelamentoComponentsPage, ParcelamentoDeleteDialog, ParcelamentoUpdatePage } from './parcelamento.page-object';

const expect = chai.expect;

describe('Parcelamento e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let parcelamentoUpdatePage: ParcelamentoUpdatePage;
    let parcelamentoComponentsPage: ParcelamentoComponentsPage;
    let parcelamentoDeleteDialog: ParcelamentoDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Parcelamentos', async () => {
        await navBarPage.goToEntity('parcelamento');
        parcelamentoComponentsPage = new ParcelamentoComponentsPage();
        await browser.wait(ec.visibilityOf(parcelamentoComponentsPage.title), 5000);
        expect(await parcelamentoComponentsPage.getTitle()).to.eq('pagboletosApp.parcelamento.home.title');
    });

    it('should load create Parcelamento page', async () => {
        await parcelamentoComponentsPage.clickOnCreateButton();
        parcelamentoUpdatePage = new ParcelamentoUpdatePage();
        expect(await parcelamentoUpdatePage.getPageTitle()).to.eq('pagboletosApp.parcelamento.home.createOrEditLabel');
        await parcelamentoUpdatePage.cancel();
    });

    it('should create and save Parcelamentos', async () => {
        const nbButtonsBeforeCreate = await parcelamentoComponentsPage.countDeleteButtons();

        await parcelamentoComponentsPage.clickOnCreateButton();
        await promise.all([
            parcelamentoUpdatePage.setIdParcelamentoInput('idParcelamento'),
            parcelamentoUpdatePage.setJurosClienteInput('5'),
            parcelamentoUpdatePage.setJurosPagBoletosInput('5'),
            parcelamentoUpdatePage.planoSelectLastOption()
        ]);
        expect(await parcelamentoUpdatePage.getIdParcelamentoInput()).to.eq('idParcelamento');
        expect(await parcelamentoUpdatePage.getJurosClienteInput()).to.eq('5');
        expect(await parcelamentoUpdatePage.getJurosPagBoletosInput()).to.eq('5');
        await parcelamentoUpdatePage.save();
        expect(await parcelamentoUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await parcelamentoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Parcelamento', async () => {
        const nbButtonsBeforeDelete = await parcelamentoComponentsPage.countDeleteButtons();
        await parcelamentoComponentsPage.clickOnLastDeleteButton();

        parcelamentoDeleteDialog = new ParcelamentoDeleteDialog();
        expect(await parcelamentoDeleteDialog.getDialogTitle()).to.eq('pagboletosApp.parcelamento.delete.question');
        await parcelamentoDeleteDialog.clickOnConfirmButton();

        expect(await parcelamentoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
