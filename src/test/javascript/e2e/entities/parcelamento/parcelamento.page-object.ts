import { by, element, ElementFinder } from 'protractor';

export class ParcelamentoComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-parcelamento div table .btn-danger'));
    title = element.all(by.css('jhi-parcelamento div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ParcelamentoUpdatePage {
    pageTitle = element(by.id('jhi-parcelamento-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    idParcelamentoInput = element(by.id('field_idParcelamento'));
    jurosClienteInput = element(by.id('field_jurosCliente'));
    jurosPagBoletosInput = element(by.id('field_jurosPagBoletos'));
    planoSelect = element(by.id('field_plano'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setIdParcelamentoInput(idParcelamento) {
        await this.idParcelamentoInput.sendKeys(idParcelamento);
    }

    async getIdParcelamentoInput() {
        return this.idParcelamentoInput.getAttribute('value');
    }

    async setJurosClienteInput(jurosCliente) {
        await this.jurosClienteInput.sendKeys(jurosCliente);
    }

    async getJurosClienteInput() {
        return this.jurosClienteInput.getAttribute('value');
    }

    async setJurosPagBoletosInput(jurosPagBoletos) {
        await this.jurosPagBoletosInput.sendKeys(jurosPagBoletos);
    }

    async getJurosPagBoletosInput() {
        return this.jurosPagBoletosInput.getAttribute('value');
    }

    async planoSelectLastOption() {
        await this.planoSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async planoSelectOption(option) {
        await this.planoSelect.sendKeys(option);
    }

    getPlanoSelect(): ElementFinder {
        return this.planoSelect;
    }

    async getPlanoSelectedOption() {
        return this.planoSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class ParcelamentoDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-parcelamento-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-parcelamento'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
