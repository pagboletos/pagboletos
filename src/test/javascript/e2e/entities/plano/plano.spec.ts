/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { PlanoComponentsPage, PlanoDeleteDialog, PlanoUpdatePage } from './plano.page-object';

const expect = chai.expect;

describe('Plano e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let planoUpdatePage: PlanoUpdatePage;
    let planoComponentsPage: PlanoComponentsPage;
    let planoDeleteDialog: PlanoDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Planos', async () => {
        await navBarPage.goToEntity('plano');
        planoComponentsPage = new PlanoComponentsPage();
        await browser.wait(ec.visibilityOf(planoComponentsPage.title), 5000);
        expect(await planoComponentsPage.getTitle()).to.eq('pagboletosApp.plano.home.title');
    });

    it('should load create Plano page', async () => {
        await planoComponentsPage.clickOnCreateButton();
        planoUpdatePage = new PlanoUpdatePage();
        expect(await planoUpdatePage.getPageTitle()).to.eq('pagboletosApp.plano.home.createOrEditLabel');
        await planoUpdatePage.cancel();
    });

    it('should create and save Planos', async () => {
        const nbButtonsBeforeCreate = await planoComponentsPage.countDeleteButtons();

        await planoComponentsPage.clickOnCreateButton();
        await promise.all([planoUpdatePage.setIdPlanoInput('idPlano'), planoUpdatePage.setRecebimentoInput('5')]);
        expect(await planoUpdatePage.getIdPlanoInput()).to.eq('idPlano');
        expect(await planoUpdatePage.getRecebimentoInput()).to.eq('5');
        await planoUpdatePage.save();
        expect(await planoUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await planoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Plano', async () => {
        const nbButtonsBeforeDelete = await planoComponentsPage.countDeleteButtons();
        await planoComponentsPage.clickOnLastDeleteButton();

        planoDeleteDialog = new PlanoDeleteDialog();
        expect(await planoDeleteDialog.getDialogTitle()).to.eq('pagboletosApp.plano.delete.question');
        await planoDeleteDialog.clickOnConfirmButton();

        expect(await planoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
