import { by, element, ElementFinder } from 'protractor';

export class PlanoComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-plano div table .btn-danger'));
    title = element.all(by.css('jhi-plano div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class PlanoUpdatePage {
    pageTitle = element(by.id('jhi-plano-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    idPlanoInput = element(by.id('field_idPlano'));
    recebimentoInput = element(by.id('field_recebimento'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setIdPlanoInput(idPlano) {
        await this.idPlanoInput.sendKeys(idPlano);
    }

    async getIdPlanoInput() {
        return this.idPlanoInput.getAttribute('value');
    }

    async setRecebimentoInput(recebimento) {
        await this.recebimentoInput.sendKeys(recebimento);
    }

    async getRecebimentoInput() {
        return this.recebimentoInput.getAttribute('value');
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class PlanoDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-plano-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-plano'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
