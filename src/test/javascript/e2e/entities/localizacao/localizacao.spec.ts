/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { LocalizacaoComponentsPage, LocalizacaoDeleteDialog, LocalizacaoUpdatePage } from './localizacao.page-object';

const expect = chai.expect;

describe('Localizacao e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let localizacaoUpdatePage: LocalizacaoUpdatePage;
    let localizacaoComponentsPage: LocalizacaoComponentsPage;
    let localizacaoDeleteDialog: LocalizacaoDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Localizacaos', async () => {
        await navBarPage.goToEntity('localizacao');
        localizacaoComponentsPage = new LocalizacaoComponentsPage();
        await browser.wait(ec.visibilityOf(localizacaoComponentsPage.title), 5000);
        expect(await localizacaoComponentsPage.getTitle()).to.eq('pagboletosApp.localizacao.home.title');
    });

    it('should load create Localizacao page', async () => {
        await localizacaoComponentsPage.clickOnCreateButton();
        localizacaoUpdatePage = new LocalizacaoUpdatePage();
        expect(await localizacaoUpdatePage.getPageTitle()).to.eq('pagboletosApp.localizacao.home.createOrEditLabel');
        await localizacaoUpdatePage.cancel();
    });

    it('should create and save Localizacaos', async () => {
        const nbButtonsBeforeCreate = await localizacaoComponentsPage.countDeleteButtons();

        await localizacaoComponentsPage.clickOnCreateButton();
        await promise.all([localizacaoUpdatePage.setLatitudeInput('5'), localizacaoUpdatePage.setLongitudeInput('5')]);
        expect(await localizacaoUpdatePage.getLatitudeInput()).to.eq('5');
        expect(await localizacaoUpdatePage.getLongitudeInput()).to.eq('5');
        await localizacaoUpdatePage.save();
        expect(await localizacaoUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await localizacaoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Localizacao', async () => {
        const nbButtonsBeforeDelete = await localizacaoComponentsPage.countDeleteButtons();
        await localizacaoComponentsPage.clickOnLastDeleteButton();

        localizacaoDeleteDialog = new LocalizacaoDeleteDialog();
        expect(await localizacaoDeleteDialog.getDialogTitle()).to.eq('pagboletosApp.localizacao.delete.question');
        await localizacaoDeleteDialog.clickOnConfirmButton();

        expect(await localizacaoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
