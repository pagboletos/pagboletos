/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PagboletosTestModule } from '../../../test.module';
import { ParcelamentoDetailComponent } from 'app/entities/parcelamento/parcelamento-detail.component';
import { Parcelamento } from 'app/shared/model/parcelamento.model';

describe('Component Tests', () => {
    describe('Parcelamento Management Detail Component', () => {
        let comp: ParcelamentoDetailComponent;
        let fixture: ComponentFixture<ParcelamentoDetailComponent>;
        const route = ({ data: of({ parcelamento: new Parcelamento(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [ParcelamentoDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ParcelamentoDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ParcelamentoDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.parcelamento).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
