/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PagboletosTestModule } from '../../../test.module';
import { ParcelamentoComponent } from 'app/entities/parcelamento/parcelamento.component';
import { ParcelamentoService } from 'app/entities/parcelamento/parcelamento.service';
import { Parcelamento } from 'app/shared/model/parcelamento.model';

describe('Component Tests', () => {
    describe('Parcelamento Management Component', () => {
        let comp: ParcelamentoComponent;
        let fixture: ComponentFixture<ParcelamentoComponent>;
        let service: ParcelamentoService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [ParcelamentoComponent],
                providers: []
            })
                .overrideTemplate(ParcelamentoComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ParcelamentoComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ParcelamentoService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Parcelamento(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.parcelamentos[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
