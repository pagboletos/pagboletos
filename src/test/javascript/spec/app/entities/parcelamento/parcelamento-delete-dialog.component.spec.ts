/* tslint:disable max-line-length */
import { ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PagboletosTestModule } from '../../../test.module';
import { ParcelamentoDeleteDialogComponent } from 'app/entities/parcelamento/parcelamento-delete-dialog.component';
import { ParcelamentoService } from 'app/entities/parcelamento/parcelamento.service';

describe('Component Tests', () => {
    describe('Parcelamento Management Delete Component', () => {
        let comp: ParcelamentoDeleteDialogComponent;
        let fixture: ComponentFixture<ParcelamentoDeleteDialogComponent>;
        let service: ParcelamentoService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [ParcelamentoDeleteDialogComponent]
            })
                .overrideTemplate(ParcelamentoDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ParcelamentoDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ParcelamentoService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
