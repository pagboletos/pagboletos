/* tslint:disable max-line-length */
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { PagboletosTestModule } from '../../../test.module';
import { ParcelamentoUpdateComponent } from 'app/entities/parcelamento/parcelamento-update.component';
import { ParcelamentoService } from 'app/entities/parcelamento/parcelamento.service';
import { Parcelamento } from 'app/shared/model/parcelamento.model';

describe('Component Tests', () => {
    describe('Parcelamento Management Update Component', () => {
        let comp: ParcelamentoUpdateComponent;
        let fixture: ComponentFixture<ParcelamentoUpdateComponent>;
        let service: ParcelamentoService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [ParcelamentoUpdateComponent]
            })
                .overrideTemplate(ParcelamentoUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ParcelamentoUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ParcelamentoService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new Parcelamento(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.parcelamento = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new Parcelamento();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.parcelamento = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
