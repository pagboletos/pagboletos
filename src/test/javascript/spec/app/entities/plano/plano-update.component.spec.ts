/* tslint:disable max-line-length */
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { PagboletosTestModule } from '../../../test.module';
import { PlanoUpdateComponent } from 'app/entities/plano/plano-update.component';
import { PlanoService } from 'app/entities/plano/plano.service';
import { Plano } from 'app/shared/model/plano.model';

describe('Component Tests', () => {
    describe('Plano Management Update Component', () => {
        let comp: PlanoUpdateComponent;
        let fixture: ComponentFixture<PlanoUpdateComponent>;
        let service: PlanoService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [PlanoUpdateComponent]
            })
                .overrideTemplate(PlanoUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PlanoUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PlanoService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new Plano(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.plano = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new Plano();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.plano = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
