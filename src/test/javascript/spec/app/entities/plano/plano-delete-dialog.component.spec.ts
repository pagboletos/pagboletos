/* tslint:disable max-line-length */
import { ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PagboletosTestModule } from '../../../test.module';
import { PlanoDeleteDialogComponent } from 'app/entities/plano/plano-delete-dialog.component';
import { PlanoService } from 'app/entities/plano/plano.service';

describe('Component Tests', () => {
    describe('Plano Management Delete Component', () => {
        let comp: PlanoDeleteDialogComponent;
        let fixture: ComponentFixture<PlanoDeleteDialogComponent>;
        let service: PlanoService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [PlanoDeleteDialogComponent]
            })
                .overrideTemplate(PlanoDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PlanoDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PlanoService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
