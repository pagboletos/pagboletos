/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PagboletosTestModule } from '../../../test.module';
import { PlanoComponent } from 'app/entities/plano/plano.component';
import { PlanoService } from 'app/entities/plano/plano.service';
import { Plano } from 'app/shared/model/plano.model';

describe('Component Tests', () => {
    describe('Plano Management Component', () => {
        let comp: PlanoComponent;
        let fixture: ComponentFixture<PlanoComponent>;
        let service: PlanoService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [PlanoComponent],
                providers: []
            })
                .overrideTemplate(PlanoComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PlanoComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PlanoService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Plano(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.planos[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
