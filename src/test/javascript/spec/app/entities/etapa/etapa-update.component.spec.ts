/* tslint:disable max-line-length */
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { PagboletosTestModule } from '../../../test.module';
import { EtapaUpdateComponent } from 'app/entities/etapa/etapa-update.component';
import { EtapaService } from 'app/entities/etapa/etapa.service';
import { Etapa } from 'app/shared/model/etapa.model';

describe('Component Tests', () => {
    describe('Etapa Management Update Component', () => {
        let comp: EtapaUpdateComponent;
        let fixture: ComponentFixture<EtapaUpdateComponent>;
        let service: EtapaService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [EtapaUpdateComponent]
            })
                .overrideTemplate(EtapaUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(EtapaUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EtapaService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new Etapa(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.etapa = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new Etapa();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.etapa = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
