/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PagboletosTestModule } from '../../../test.module';
import { EtapaDetailComponent } from 'app/entities/etapa/etapa-detail.component';
import { Etapa } from 'app/shared/model/etapa.model';

describe('Component Tests', () => {
    describe('Etapa Management Detail Component', () => {
        let comp: EtapaDetailComponent;
        let fixture: ComponentFixture<EtapaDetailComponent>;
        const route = ({ data: of({ etapa: new Etapa(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [EtapaDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(EtapaDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(EtapaDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.etapa).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
