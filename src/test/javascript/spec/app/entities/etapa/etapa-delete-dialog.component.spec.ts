/* tslint:disable max-line-length */
import { ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PagboletosTestModule } from '../../../test.module';
import { EtapaDeleteDialogComponent } from 'app/entities/etapa/etapa-delete-dialog.component';
import { EtapaService } from 'app/entities/etapa/etapa.service';

describe('Component Tests', () => {
    describe('Etapa Management Delete Component', () => {
        let comp: EtapaDeleteDialogComponent;
        let fixture: ComponentFixture<EtapaDeleteDialogComponent>;
        let service: EtapaService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [EtapaDeleteDialogComponent]
            })
                .overrideTemplate(EtapaDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(EtapaDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EtapaService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
