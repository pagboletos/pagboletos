/* tslint:disable max-line-length */
import { getTestBed, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, take } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { EtapaService } from 'app/entities/etapa/etapa.service';
import { Etapa, IEtapa } from 'app/shared/model/etapa.model';

describe('Service Tests', () => {
    describe('Etapa Service', () => {
        let injector: TestBed;
        let service: EtapaService;
        let httpMock: HttpTestingController;
        let elemDefault: IEtapa;
        let currentDate: moment.Moment;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(EtapaService);
            httpMock = injector.get(HttpTestingController);
            currentDate = moment();

            elemDefault = new Etapa(0, 'AAAAAAA', 'AAAAAAA', currentDate, false);
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign(
                    {
                        dataEfetuacao: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a Etapa', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0,
                        dataEfetuacao: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        dataEfetuacao: currentDate
                    },
                    returnedFromService
                );
                service
                    .create(new Etapa(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a Etapa', async () => {
                const returnedFromService = Object.assign(
                    {
                        nome: 'BBBBBB',
                        descricao: 'BBBBBB',
                        dataEfetuacao: currentDate.format(DATE_FORMAT),
                        finalizado: true
                    },
                    elemDefault
                );

                const expected = Object.assign(
                    {
                        dataEfetuacao: currentDate
                    },
                    returnedFromService
                );
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of Etapa', async () => {
                const returnedFromService = Object.assign(
                    {
                        nome: 'BBBBBB',
                        descricao: 'BBBBBB',
                        dataEfetuacao: currentDate.format(DATE_FORMAT),
                        finalizado: true
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        dataEfetuacao: currentDate
                    },
                    returnedFromService
                );
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a Etapa', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
