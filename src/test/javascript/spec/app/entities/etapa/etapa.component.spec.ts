/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PagboletosTestModule } from '../../../test.module';
import { EtapaComponent } from 'app/entities/etapa/etapa.component';
import { EtapaService } from 'app/entities/etapa/etapa.service';
import { Etapa } from 'app/shared/model/etapa.model';

describe('Component Tests', () => {
    describe('Etapa Management Component', () => {
        let comp: EtapaComponent;
        let fixture: ComponentFixture<EtapaComponent>;
        let service: EtapaService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [EtapaComponent],
                providers: []
            })
                .overrideTemplate(EtapaComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(EtapaComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EtapaService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Etapa(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.etapas[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
