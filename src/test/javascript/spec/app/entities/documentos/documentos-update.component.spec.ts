/* tslint:disable max-line-length */
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { PagboletosTestModule } from '../../../test.module';
import { DocumentosUpdateComponent } from 'app/entities/documentos/documentos-update.component';
import { DocumentosService } from 'app/entities/documentos/documentos.service';
import { Documentos } from 'app/shared/model/documentos.model';

describe('Component Tests', () => {
    describe('Documentos Management Update Component', () => {
        let comp: DocumentosUpdateComponent;
        let fixture: ComponentFixture<DocumentosUpdateComponent>;
        let service: DocumentosService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [DocumentosUpdateComponent]
            })
                .overrideTemplate(DocumentosUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(DocumentosUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DocumentosService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new Documentos(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.documentos = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new Documentos();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.documentos = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
