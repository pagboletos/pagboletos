/* tslint:disable max-line-length */
import { ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PagboletosTestModule } from '../../../test.module';
import { DocumentosDeleteDialogComponent } from 'app/entities/documentos/documentos-delete-dialog.component';
import { DocumentosService } from 'app/entities/documentos/documentos.service';

describe('Component Tests', () => {
    describe('Documentos Management Delete Component', () => {
        let comp: DocumentosDeleteDialogComponent;
        let fixture: ComponentFixture<DocumentosDeleteDialogComponent>;
        let service: DocumentosService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [DocumentosDeleteDialogComponent]
            })
                .overrideTemplate(DocumentosDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(DocumentosDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DocumentosService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
