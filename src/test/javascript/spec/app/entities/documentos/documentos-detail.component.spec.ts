/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PagboletosTestModule } from '../../../test.module';
import { DocumentosDetailComponent } from 'app/entities/documentos/documentos-detail.component';
import { Documentos } from 'app/shared/model/documentos.model';

describe('Component Tests', () => {
    describe('Documentos Management Detail Component', () => {
        let comp: DocumentosDetailComponent;
        let fixture: ComponentFixture<DocumentosDetailComponent>;
        const route = ({ data: of({ documentos: new Documentos(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [DocumentosDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(DocumentosDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(DocumentosDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.documentos).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
