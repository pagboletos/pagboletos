/* tslint:disable max-line-length */
import { getTestBed, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { DocumentosService } from 'app/entities/documentos/documentos.service';
import { CategoriaDocumento, Documentos, IDocumentos } from 'app/shared/model/documentos.model';

describe('Service Tests', () => {
    describe('Documentos Service', () => {
        let injector: TestBed;
        let service: DocumentosService;
        let httpMock: HttpTestingController;
        let elemDefault: IDocumentos;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(DocumentosService);
            httpMock = injector.get(HttpTestingController);

            elemDefault = new Documentos(0, 'AAAAAAA', 'AAAAAAA', CategoriaDocumento.ATIVIDADE, 'image/png', 'AAAAAAA', false);
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign({}, elemDefault);
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a Documentos', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .create(new Documentos(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a Documentos', async () => {
                const returnedFromService = Object.assign(
                    {
                        idDocumento: 'BBBBBB',
                        nome: 'BBBBBB',
                        categoria: 'BBBBBB',
                        imagem: 'BBBBBB',
                        status: true
                    },
                    elemDefault
                );

                const expected = Object.assign({}, returnedFromService);
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of Documentos', async () => {
                const returnedFromService = Object.assign(
                    {
                        idDocumento: 'BBBBBB',
                        nome: 'BBBBBB',
                        categoria: 'BBBBBB',
                        imagem: 'BBBBBB',
                        status: true
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a Documentos', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
