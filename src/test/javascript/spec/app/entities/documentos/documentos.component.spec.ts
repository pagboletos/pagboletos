/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PagboletosTestModule } from '../../../test.module';
import { DocumentosComponent } from 'app/entities/documentos/documentos.component';
import { DocumentosService } from 'app/entities/documentos/documentos.service';
import { Documentos } from 'app/shared/model/documentos.model';

describe('Component Tests', () => {
    describe('Documentos Management Component', () => {
        let comp: DocumentosComponent;
        let fixture: ComponentFixture<DocumentosComponent>;
        let service: DocumentosService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [DocumentosComponent],
                providers: []
            })
                .overrideTemplate(DocumentosComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(DocumentosComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DocumentosService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Documentos(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.documentos[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
