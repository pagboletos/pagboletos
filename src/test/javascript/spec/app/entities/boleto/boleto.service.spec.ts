/* tslint:disable max-line-length */
import { getTestBed, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, take } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { BoletoService } from 'app/entities/boleto/boleto.service';
import { Boleto, IBoleto } from 'app/shared/model/boleto.model';

describe('Service Tests', () => {
    describe('Boleto Service', () => {
        let injector: TestBed;
        let service: BoletoService;
        let httpMock: HttpTestingController;
        let elemDefault: IBoleto;
        let currentDate: moment.Moment;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(BoletoService);
            httpMock = injector.get(HttpTestingController);
            currentDate = moment();

            elemDefault = new Boleto(0, 'AAAAAAA', 'AAAAAAA', currentDate, 'AAAAAAA', 'AAAAAAA', currentDate);
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign(
                    {
                        vencimento: currentDate.format(DATE_FORMAT),
                        dataDePagamento: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a Boleto', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0,
                        vencimento: currentDate.format(DATE_FORMAT),
                        dataDePagamento: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        vencimento: currentDate,
                        dataDePagamento: currentDate
                    },
                    returnedFromService
                );
                service
                    .create(new Boleto(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a Boleto', async () => {
                const returnedFromService = Object.assign(
                    {
                        idBoletos: 'BBBBBB',
                        codigoDeBarras: 'BBBBBB',
                        vencimento: currentDate.format(DATE_FORMAT),
                        beneficiario: 'BBBBBB',
                        descricao: 'BBBBBB',
                        dataDePagamento: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );

                const expected = Object.assign(
                    {
                        vencimento: currentDate,
                        dataDePagamento: currentDate
                    },
                    returnedFromService
                );
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of Boleto', async () => {
                const returnedFromService = Object.assign(
                    {
                        idBoletos: 'BBBBBB',
                        codigoDeBarras: 'BBBBBB',
                        vencimento: currentDate.format(DATE_FORMAT),
                        beneficiario: 'BBBBBB',
                        descricao: 'BBBBBB',
                        dataDePagamento: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        vencimento: currentDate,
                        dataDePagamento: currentDate
                    },
                    returnedFromService
                );
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a Boleto', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
