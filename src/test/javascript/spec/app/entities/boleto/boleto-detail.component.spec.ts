/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PagboletosTestModule } from '../../../test.module';
import { BoletoDetailComponent } from 'app/entities/boleto/boleto-detail.component';
import { Boleto } from 'app/shared/model/boleto.model';

describe('Component Tests', () => {
    describe('Boleto Management Detail Component', () => {
        let comp: BoletoDetailComponent;
        let fixture: ComponentFixture<BoletoDetailComponent>;
        const route = ({ data: of({ boleto: new Boleto(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [BoletoDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(BoletoDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(BoletoDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.boleto).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
