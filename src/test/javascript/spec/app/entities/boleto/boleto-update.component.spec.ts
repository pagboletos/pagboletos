/* tslint:disable max-line-length */
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { PagboletosTestModule } from '../../../test.module';
import { BoletoUpdateComponent } from 'app/entities/boleto/boleto-update.component';
import { BoletoService } from 'app/entities/boleto/boleto.service';
import { Boleto } from 'app/shared/model/boleto.model';

describe('Component Tests', () => {
    describe('Boleto Management Update Component', () => {
        let comp: BoletoUpdateComponent;
        let fixture: ComponentFixture<BoletoUpdateComponent>;
        let service: BoletoService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [BoletoUpdateComponent]
            })
                .overrideTemplate(BoletoUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(BoletoUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BoletoService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new Boleto(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.boleto = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new Boleto();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.boleto = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
