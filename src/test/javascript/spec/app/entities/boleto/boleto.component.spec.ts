/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PagboletosTestModule } from '../../../test.module';
import { BoletoComponent } from 'app/entities/boleto/boleto.component';
import { BoletoService } from 'app/entities/boleto/boleto.service';
import { Boleto } from 'app/shared/model/boleto.model';

describe('Component Tests', () => {
    describe('Boleto Management Component', () => {
        let comp: BoletoComponent;
        let fixture: ComponentFixture<BoletoComponent>;
        let service: BoletoService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [BoletoComponent],
                providers: []
            })
                .overrideTemplate(BoletoComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(BoletoComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BoletoService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Boleto(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.boletos[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
