/* tslint:disable max-line-length */
import { ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PagboletosTestModule } from '../../../test.module';
import { BoletoDeleteDialogComponent } from 'app/entities/boleto/boleto-delete-dialog.component';
import { BoletoService } from 'app/entities/boleto/boleto.service';

describe('Component Tests', () => {
    describe('Boleto Management Delete Component', () => {
        let comp: BoletoDeleteDialogComponent;
        let fixture: ComponentFixture<BoletoDeleteDialogComponent>;
        let service: BoletoService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [BoletoDeleteDialogComponent]
            })
                .overrideTemplate(BoletoDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(BoletoDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BoletoService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
