/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PagboletosTestModule } from '../../../test.module';
import { VendedorComponent } from 'app/entities/vendedor/vendedor.component';
import { VendedorService } from 'app/entities/vendedor/vendedor.service';
import { Vendedor } from 'app/shared/model/vendedor.model';

describe('Component Tests', () => {
    describe('Vendedor Management Component', () => {
        let comp: VendedorComponent;
        let fixture: ComponentFixture<VendedorComponent>;
        let service: VendedorService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PagboletosTestModule],
                declarations: [VendedorComponent],
                providers: []
            })
                .overrideTemplate(VendedorComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(VendedorComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VendedorService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Vendedor(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.vendedors[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
