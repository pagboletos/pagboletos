package com.rvpedroso.pagboletos.service.util;

import com.rvpedroso.pagboletos.PagboletosApp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for the UserResource REST controller.
 *
 * @see UserService
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PagboletosApp.class)
@Transactional
public class CriptografiaTest {

    @Test
    @Transactional
    public void codificar() {

        String codificado = Criptografia.codificar("123");
        String decodificado =  Criptografia.decodificar(codificado);

        assertThat(decodificado).isEqualTo("123");
    }

    @Test
    @Transactional
    public void decodificar() {

        String decodificado = "544836";
        assertThat(Criptografia.decodificar("NTQ0ODM2")).isEqualTo(decodificado);

    }

    @Test
    @Transactional
    public void decodificarTest() {

        String codificado = "NTM3NTY3";

        System.out.println("Decodificar "+ Criptografia.decodificar(codificado));

    }

}
