package com.rvpedroso.pagboletos.domain.enumeration;

/**
 * The CategoriaDocumento enumeration.
 */
public enum CategoriaDocumento {
    ATIVIDADE, RESIDENCIA, IDENTIFICACAO
}
