package com.rvpedroso.pagboletos.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Boleto.
 */
@Entity
@Table(name = "boleto")
public class Boleto implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "id_boletos")
    private String idBoletos;

    @Column(name = "codigo_de_barras")
    private String codigoDeBarras;

    @Column(name = "vencimento")
    private LocalDate vencimento;

    @Column(name = "beneficiario")
    private String beneficiario;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "data_de_pagamento")
    private LocalDate dataDePagamento;

    @OneToOne
    @JoinColumn(unique = true)
    private Pagamento pagamento;

    @ManyToOne
    @JsonIgnoreProperties("boletos")
    private Vendedor vendedor;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdBoletos() {
        return idBoletos;
    }

    public Boleto idBoletos(String idBoletos) {
        this.idBoletos = idBoletos;
        return this;
    }

    public void setIdBoletos(String idBoletos) {
        this.idBoletos = idBoletos;
    }

    public String getCodigoDeBarras() {
        return codigoDeBarras;
    }

    public Boleto codigoDeBarras(String codigoDeBarras) {
        this.codigoDeBarras = codigoDeBarras;
        return this;
    }

    public void setCodigoDeBarras(String codigoDeBarras) {
        this.codigoDeBarras = codigoDeBarras;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public Boleto vencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
        return this;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public String getBeneficiario() {
        return beneficiario;
    }

    public Boleto beneficiario(String beneficiario) {
        this.beneficiario = beneficiario;
        return this;
    }

    public void setBeneficiario(String beneficiario) {
        this.beneficiario = beneficiario;
    }

    public String getDescricao() {
        return descricao;
    }

    public Boleto descricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public LocalDate getDataDePagamento() {
        return dataDePagamento;
    }

    public Boleto dataDePagamento(LocalDate dataDePagamento) {
        this.dataDePagamento = dataDePagamento;
        return this;
    }

    public void setDataDePagamento(LocalDate dataDePagamento) {
        this.dataDePagamento = dataDePagamento;
    }

    public Pagamento getPagamento() {
        return pagamento;
    }

    public Boleto pagamento(Pagamento pagamento) {
        this.pagamento = pagamento;
        return this;
    }

    public void setPagamento(Pagamento pagamento) {
        this.pagamento = pagamento;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public Boleto vendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
        return this;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Boleto boleto = (Boleto) o;
        if (boleto.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), boleto.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Boleto{" +
            "id=" + getId() +
            ", idBoletos='" + getIdBoletos() + "'" +
            ", codigoDeBarras='" + getCodigoDeBarras() + "'" +
            ", vencimento='" + getVencimento() + "'" +
            ", beneficiario='" + getBeneficiario() + "'" +
            ", descricao='" + getDescricao() + "'" +
            ", dataDePagamento='" + getDataDePagamento() + "'" +
            "}";
    }
}
