package com.rvpedroso.pagboletos.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.rvpedroso.pagboletos.domain.enumeration.CategoriaDocumento;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Documentos.
 */
@Entity
@Table(name = "documentos")
public class Documentos implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "id_documento")
    private String idDocumento;

    @Column(name = "nome")
    private String nome;

    @Enumerated(EnumType.STRING)
    @Column(name = "categoria")
    private CategoriaDocumento categoria;

    @Lob
    @Column(name = "imagem")
    private byte[] imagem;

    @Column(name = "imagem_content_type")
    private String imagemContentType;

    @Column(name = "status")
    private Boolean status;

    @ManyToOne
    @JsonIgnoreProperties("documentos")
    private Vendedor vendedor;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdDocumento() {
        return idDocumento;
    }

    public Documentos idDocumento(String idDocumento) {
        this.idDocumento = idDocumento;
        return this;
    }

    public void setIdDocumento(String idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getNome() {
        return nome;
    }

    public Documentos nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public CategoriaDocumento getCategoria() {
        return categoria;
    }

    public Documentos categoria(CategoriaDocumento categoria) {
        this.categoria = categoria;
        return this;
    }

    public void setCategoria(CategoriaDocumento categoria) {
        this.categoria = categoria;
    }

    public byte[] getImagem() {
        return imagem;
    }

    public Documentos imagem(byte[] imagem) {
        this.imagem = imagem;
        return this;
    }

    public void setImagem(byte[] imagem) {
        this.imagem = imagem;
    }

    public String getImagemContentType() {
        return imagemContentType;
    }

    public Documentos imagemContentType(String imagemContentType) {
        this.imagemContentType = imagemContentType;
        return this;
    }

    public void setImagemContentType(String imagemContentType) {
        this.imagemContentType = imagemContentType;
    }

    public Boolean isStatus() {
        return status;
    }

    public Documentos status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public Documentos vendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
        return this;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Documentos documentos = (Documentos) o;
        if (documentos.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), documentos.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Documentos{" +
            "id=" + getId() +
            ", idDocumento='" + getIdDocumento() + "'" +
            ", nome='" + getNome() + "'" +
            ", categoria='" + getCategoria() + "'" +
            ", imagem='" + getImagem() + "'" +
            ", imagemContentType='" + getImagemContentType() + "'" +
            ", status='" + isStatus() + "'" +
            "}";
    }
}
