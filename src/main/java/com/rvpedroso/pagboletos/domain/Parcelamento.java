package com.rvpedroso.pagboletos.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A Parcelamento.
 */
@Entity
@Table(name = "parcelamento")
public class Parcelamento implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "id_parcelamento")
    private String idParcelamento;

    @Column(name = "juros_cliente", precision = 10, scale = 2)
    private BigDecimal jurosCliente;

    @Column(name = "juros_pag_boletos", precision = 10, scale = 2)
    private BigDecimal jurosPagBoletos;

    @ManyToOne
    @JsonIgnoreProperties("parcelamentos")
    private Plano plano;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdParcelamento() {
        return idParcelamento;
    }

    public Parcelamento idParcelamento(String idParcelamento) {
        this.idParcelamento = idParcelamento;
        return this;
    }

    public void setIdParcelamento(String idParcelamento) {
        this.idParcelamento = idParcelamento;
    }

    public BigDecimal getJurosCliente() {
        return jurosCliente;
    }

    public Parcelamento jurosCliente(BigDecimal jurosCliente) {
        this.jurosCliente = jurosCliente;
        return this;
    }

    public void setJurosCliente(BigDecimal jurosCliente) {
        this.jurosCliente = jurosCliente;
    }

    public BigDecimal getJurosPagBoletos() {
        return jurosPagBoletos;
    }

    public Parcelamento jurosPagBoletos(BigDecimal jurosPagBoletos) {
        this.jurosPagBoletos = jurosPagBoletos;
        return this;
    }

    public void setJurosPagBoletos(BigDecimal jurosPagBoletos) {
        this.jurosPagBoletos = jurosPagBoletos;
    }

    public Plano getPlano() {
        return plano;
    }

    public Parcelamento plano(Plano plano) {
        this.plano = plano;
        return this;
    }

    public void setPlano(Plano plano) {
        this.plano = plano;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Parcelamento parcelamento = (Parcelamento) o;
        if (parcelamento.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), parcelamento.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Parcelamento{" +
            "id=" + getId() +
            ", idParcelamento='" + getIdParcelamento() + "'" +
            ", jurosCliente=" + getJurosCliente() +
            ", jurosPagBoletos=" + getJurosPagBoletos() +
            "}";
    }
}
