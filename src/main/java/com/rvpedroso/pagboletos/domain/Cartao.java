package com.rvpedroso.pagboletos.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Cartao.
 */
@Entity
@Table(name = "cartao")
public class Cartao implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "id_cartao")
    private String idCartao;

    @Column(name = "id_token")
    private String idToken;

    @Column(name = "numero_cartao")
    private String numeroCartao;

    @ManyToOne
    @JsonIgnoreProperties("cartaos")
    private Vendedor vendedor;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdToken() {
        return idToken;
    }

    public Cartao idToken(String idToken) {
        this.idToken = idToken;
        return this;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public String getIdCartao() {
        return idCartao;
    }

    public void setIdCartao(String idCartao) {
        this.idCartao = idCartao;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public Cartao vendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
        return this;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Cartao cartao = (Cartao) o;
        if (cartao.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cartao.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Cartao{" +
            "id=" + getId() +
            ", idToken='" + getIdToken() + "'" +
            "}";
    }
}
