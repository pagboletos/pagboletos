package com.rvpedroso.pagboletos.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Plano.
 */
@Entity
@Table(name = "plano")
public class Plano implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "id_plano")
    private String idPlano;

    @Column(name = "recebimento")
    private Integer recebimento;

    @OneToMany(mappedBy = "plano")
    private Set<Parcelamento> parcelamentos = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdPlano() {
        return idPlano;
    }

    public Plano idPlano(String idPlano) {
        this.idPlano = idPlano;
        return this;
    }

    public void setIdPlano(String idPlano) {
        this.idPlano = idPlano;
    }

    public Integer getRecebimento() {
        return recebimento;
    }

    public Plano recebimento(Integer recebimento) {
        this.recebimento = recebimento;
        return this;
    }

    public void setRecebimento(Integer recebimento) {
        this.recebimento = recebimento;
    }

    public Set<Parcelamento> getParcelamentos() {
        return parcelamentos;
    }

    public Plano parcelamentos(Set<Parcelamento> parcelamentos) {
        this.parcelamentos = parcelamentos;
        return this;
    }

    public Plano addParcelamento(Parcelamento parcelamento) {
        this.parcelamentos.add(parcelamento);
        parcelamento.setPlano(this);
        return this;
    }

    public Plano removeParcelamento(Parcelamento parcelamento) {
        this.parcelamentos.remove(parcelamento);
        parcelamento.setPlano(null);
        return this;
    }

    public void setParcelamentos(Set<Parcelamento> parcelamentos) {
        this.parcelamentos = parcelamentos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Plano plano = (Plano) o;
        if (plano.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plano.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Plano{" +
            "id=" + getId() +
            ", idPlano='" + getIdPlano() + "'" +
            ", recebimento=" + getRecebimento() +
            "}";
    }
}
