package com.rvpedroso.pagboletos.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Etapa.
 */
@Entity
@Table(name = "etapa")
public class Etapa implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "data_efetuacao")
    private LocalDate dataEfetuacao;

    @Column(name = "finalizado")
    private Boolean finalizado;

    @ManyToOne
    @JsonIgnoreProperties("etapas")
    private Pagamento pagamento;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Etapa nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public Etapa descricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public LocalDate getDataEfetuacao() {
        return dataEfetuacao;
    }

    public Etapa dataEfetuacao(LocalDate dataEfetuacao) {
        this.dataEfetuacao = dataEfetuacao;
        return this;
    }

    public void setDataEfetuacao(LocalDate dataEfetuacao) {
        this.dataEfetuacao = dataEfetuacao;
    }

    public Boolean isFinalizado() {
        return finalizado;
    }

    public Etapa finalizado(Boolean finalizado) {
        this.finalizado = finalizado;
        return this;
    }

    public void setFinalizado(Boolean finalizado) {
        this.finalizado = finalizado;
    }

    public Pagamento getPagamento() {
        return pagamento;
    }

    public Etapa pagamento(Pagamento pagamento) {
        this.pagamento = pagamento;
        return this;
    }

    public void setPagamento(Pagamento pagamento) {
        this.pagamento = pagamento;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Etapa etapa = (Etapa) o;
        if (etapa.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), etapa.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Etapa{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", descricao='" + getDescricao() + "'" +
            ", dataEfetuacao='" + getDataEfetuacao() + "'" +
            ", finalizado='" + isFinalizado() + "'" +
            "}";
    }
}
