package com.rvpedroso.pagboletos.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Pagamento.
 */
@Entity
@Table(name = "pagamento")
public class Pagamento implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "id_transacao_cartao")
    private String idTransacaoCartao;

    @Column(name = "id_transacao_boleto")
    private String idTransacaoBoleto;

    @OneToMany(mappedBy = "pagamento")
    private Set<Etapa> etapas = new HashSet<>();
    @OneToOne(mappedBy = "pagamento")
    @JsonIgnore
    private Boleto vendedor;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdTransacaoCartao() {
        return idTransacaoCartao;
    }

    public Pagamento idTransacaoCartao(String idTransacaoCartao) {
        this.idTransacaoCartao = idTransacaoCartao;
        return this;
    }

    public void setIdTransacaoCartao(String idTransacaoCartao) {
        this.idTransacaoCartao = idTransacaoCartao;
    }

    public String getIdTransacaoBoleto() {
        return idTransacaoBoleto;
    }

    public Pagamento idTransacaoBoleto(String idTransacaoBoleto) {
        this.idTransacaoBoleto = idTransacaoBoleto;
        return this;
    }

    public void setIdTransacaoBoleto(String idTransacaoBoleto) {
        this.idTransacaoBoleto = idTransacaoBoleto;
    }

    public Set<Etapa> getEtapas() {
        return etapas;
    }

    public Pagamento etapas(Set<Etapa> etapas) {
        this.etapas = etapas;
        return this;
    }

    public Pagamento addEtapa(Etapa etapa) {
        this.etapas.add(etapa);
        etapa.setPagamento(this);
        return this;
    }

    public Pagamento removeEtapa(Etapa etapa) {
        this.etapas.remove(etapa);
        etapa.setPagamento(null);
        return this;
    }

    public void setEtapas(Set<Etapa> etapas) {
        this.etapas = etapas;
    }

    public Boleto getVendedor() {
        return vendedor;
    }

    public Pagamento vendedor(Boleto boleto) {
        this.vendedor = boleto;
        return this;
    }

    public void setVendedor(Boleto boleto) {
        this.vendedor = boleto;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pagamento pagamento = (Pagamento) o;
        if (pagamento.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pagamento.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Pagamento{" +
            "id=" + getId() +
            ", idTransacaoCartao='" + getIdTransacaoCartao() + "'" +
            ", idTransacaoBoleto='" + getIdTransacaoBoleto() + "'" +
            "}";
    }
}
