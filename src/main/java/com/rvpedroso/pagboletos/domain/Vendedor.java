package com.rvpedroso.pagboletos.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Vendedor.
 */
@Entity
@Table(name = "vendedor")
public class Vendedor implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "id_vendedor", nullable = false)
    private String idVendedor;

    @Lob
    @Column(name = "selfie")
    private byte[] selfie;

    @Column(name = "selfie_content_type")
    private String selfieContentType;

    @Column(name = "identidade")
    private Boolean identidade;

    @Column(name = "endereco")
    private Boolean endereco;

    @OneToOne
    @JoinColumn(unique = true)
    private Localizacao localizacao;

    @OneToMany(mappedBy = "vendedor")
    private Set<Documentos> documentos = new HashSet<>();
    @OneToMany(mappedBy = "vendedor")
    private Set<Cartao> cartaos = new HashSet<>();
    @OneToMany(mappedBy = "vendedor")
    private Set<Boleto> boletos = new HashSet<>();

    @OneToOne(mappedBy = "vendedor")
    @JsonIgnore
    private User user;

    public Vendedor() {
    }

    public Vendedor(@NotNull String idVendedor, User user) {
        this.idVendedor = idVendedor;
        this.user = user;
    }

    public Vendedor(@NotNull String idVendedor) {
        this.idVendedor = idVendedor;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdVendedor() {
        return idVendedor;
    }

    public Vendedor idVendedor(String idVendedor) {
        this.idVendedor = idVendedor;
        return this;
    }

    public void setIdVendedor(String idVendedor) {
        this.idVendedor = idVendedor;
    }

    public byte[] getSelfie() {
        return selfie;
    }

    public Vendedor selfie(byte[] selfie) {
        this.selfie = selfie;
        return this;
    }

    public void setSelfie(byte[] selfie) {
        this.selfie = selfie;
    }

    public String getSelfieContentType() {
        return selfieContentType;
    }

    public Vendedor selfieContentType(String selfieContentType) {
        this.selfieContentType = selfieContentType;
        return this;
    }

    public void setSelfieContentType(String selfieContentType) {
        this.selfieContentType = selfieContentType;
    }

    public Localizacao getLocalizacao() {
        return localizacao;
    }

    public Vendedor localizacao(Localizacao localizacao) {
        this.localizacao = localizacao;
        return this;
    }

    public void setLocalizacao(Localizacao localizacao) {
        this.localizacao = localizacao;
    }

    public Set<Documentos> getDocumentos() {
        return documentos;
    }

    public Vendedor documentos(Set<Documentos> documentos) {
        this.documentos = documentos;
        return this;
    }

    public Vendedor addDocumento(Documentos documentos) {
        this.documentos.add(documentos);
        documentos.setVendedor(this);
        return this;
    }

    public Vendedor removeDocumento(Documentos documentos) {
        this.documentos.remove(documentos);
        documentos.setVendedor(null);
        return this;
    }

    public void setDocumentos(Set<Documentos> documentos) {
        this.documentos = documentos;
    }

    public Set<Cartao> getCartaos() {
        return cartaos;
    }

    public Vendedor cartaos(Set<Cartao> cartaos) {
        this.cartaos = cartaos;
        return this;
    }

    public Vendedor addCartao(Cartao cartao) {
        this.cartaos.add(cartao);
        cartao.setVendedor(this);
        return this;
    }

    public Vendedor removeCartao(Cartao cartao) {
        this.cartaos.remove(cartao);
        cartao.setVendedor(null);
        return this;
    }

    public void setCartaos(Set<Cartao> cartaos) {
        this.cartaos = cartaos;
    }

    public Set<Boleto> getBoletos() {
        return boletos;
    }

    public Vendedor boletos(Set<Boleto> boletos) {
        this.boletos = boletos;
        return this;
    }

    public Vendedor addBoleto(Boleto boleto) {
        this.boletos.add(boleto);
        boleto.setVendedor(this);
        return this;
    }

    public Vendedor removeBoleto(Boleto boleto) {
        this.boletos.remove(boleto);
        boleto.setVendedor(null);
        return this;
    }

    public void setBoletos(Set<Boleto> boletos) {
        this.boletos = boletos;
    }


    public User getUser() {
        return user;
    }

    public Vendedor user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getIdentidade() {
        return identidade;
    }

    public void setIdentidade(Boolean identidade) {
        this.identidade = identidade;
    }

    public Boolean getEndereco() {
        return endereco;
    }

    public void setEndereco(Boolean endereco) {
        this.endereco = endereco;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Vendedor vendedor = (Vendedor) o;
        if (vendedor.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), vendedor.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Vendedor{" +
            "id=" + getId() +
            ", idVendedor='" + getIdVendedor() + "'" +
            ", selfie='" + getSelfie() + "'" +
            ", selfieContentType='" + getSelfieContentType() + "'" +
            "}";
    }
}
