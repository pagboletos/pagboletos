/**
 * Data Access Objects used by WebSocket services.
 */
package com.rvpedroso.pagboletos.web.websocket.dto;
