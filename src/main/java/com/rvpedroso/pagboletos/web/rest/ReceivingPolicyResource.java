package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.zoop.ReceivingPolicyZoop;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class ReceivingPolicyResource {

    private static final String ENTITY_NAME = "ReceivingPolicy";
    private final ReceivingPolicyZoop receivingPolicyZoop;

    public ReceivingPolicyResource(ReceivingPolicyZoop receivingPolicyZoop) {
        this.receivingPolicyZoop = receivingPolicyZoop;
    }

    /**
     * Recuperar política de recebimento por seller
     */
    @GetMapping("/receiving_policy/seller/{sellerId}")
    public ResponseEntity<Map<String, Object>> get(@Valid @RequestParam(name = "sellerId", defaultValue = "") String sellerId) {
        Map<String, Object> receivingPolicy;

        receivingPolicy = this.receivingPolicyZoop.get(sellerId);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sellerId))
            .body(receivingPolicy);
    }

    /**
     * Alterar política de recebimento por seller
     */
    @PutMapping("/bank_account/{sellerId}")
    public ResponseEntity<Map<String, Object>> update(@Valid @RequestParam(name = "sellerId", defaultValue = "") String sellerId
        , @RequestBody Map<String, Object> body) {
        Map<String, Object> receivingPolicy;

        receivingPolicy = this.receivingPolicyZoop.update(sellerId, body);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sellerId))
            .body(receivingPolicy);
    }


}
