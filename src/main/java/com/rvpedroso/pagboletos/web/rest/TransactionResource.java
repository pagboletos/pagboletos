package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.zoop.TransactionZoop;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import io.micrometer.core.annotation.Timed;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class TransactionResource {

    private static final String ENTITY_NAME = "Transaction";

    private final TransactionZoop transactionZoop;

    public TransactionResource(TransactionZoop transactionZoop) {
        this.transactionZoop = transactionZoop;
    }

    /**
     * Listar transaçoes do marketplace
     */
    @GetMapping("/transaction")
    @Timed
    public ResponseEntity<Map<String, Object>> list(Pageable pageable) {
        Map<String, Object> bankAccount;

        bankAccount = this.transactionZoop.list();
        return ResponseEntity.ok()
            .body(bankAccount);
    }

    /**
     * Criar transação Cartão Não Presente
     */
    @PostMapping("/transaction")
    public ResponseEntity<Map<String, Object>> create(@Valid @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> bankAccount;

        bankAccount = this.transactionZoop.create(body);

        return ResponseEntity.created(new URI("/api/transaction/" + bankAccount.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, bankAccount.get("id").toString()))
            .body(body);
    }

    /**
     * Listar documentos de um vendedor
     */
    @GetMapping("/transaction/seller/{sellerId}")
    public ResponseEntity<Map<String, Object>> listTransactionSeller(@Valid @RequestParam(name = "sellerId", defaultValue = "") String sellerId) {
        Map<String, Object> bankAccount;

        bankAccount = this.transactionZoop.listTransactionSeller(sellerId);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sellerId))
            .body(bankAccount);
    }


    /**
     * Recuperar detalhes de transação pelo identificador
     */
    @GetMapping("/transaction/{id}")
    public ResponseEntity<Map<String, Object>> get(@Valid @PathVariable String id) {
        Map<String, Object> bankAccount;

        bankAccount = this.transactionZoop.get(id);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(bankAccount);
    }

    /**
     * Alterar detalhes de transação pelo identificador
     */
    @PutMapping("/transaction/{id}")
    public ResponseEntity<Map<String, Object>> update(@Valid @PathVariable String id, @RequestBody Map<String, Object> body) {
        Map<String, Object> bankAccount;

        bankAccount = this.transactionZoop.update(id, body);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(bankAccount);
    }

    /**
     * Alterar detalhes de transação pelo identificador
     */
    @PutMapping("/transaction/void/{id}")
    public ResponseEntity<Map<String, Object>> reverseTransaction(@Valid @PathVariable String id, @RequestBody Map<String, Object> body) {
        Map<String, Object> bankAccount;

        bankAccount = this.transactionZoop.reverseTransaction(id, body);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(bankAccount);
    }

    /**
     * Capturar transação cartão não presente
     */
    @PostMapping("/transaction/capture/{id}")
    public ResponseEntity<Map<String, Object>> capture(@Valid @PathVariable String id,
                                                       @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> bankAccount;

        bankAccount = this.transactionZoop.capture(id, body);

        return ResponseEntity.created(new URI("/api/transaction/" + bankAccount.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, bankAccount.get("id").toString()))
            .body(body);
    }
}
