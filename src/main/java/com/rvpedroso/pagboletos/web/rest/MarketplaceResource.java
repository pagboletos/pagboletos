package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.MarketplaceService;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class MarketplaceResource {

    private final MarketplaceService marketplaceService;
    private static final String ENTITY_NAME = "Marketplace";

    public MarketplaceResource(MarketplaceService marketplaceService) {
        this.marketplaceService = marketplaceService;
    }

    /**
     * Recuperar detalhes do marketplace
     */
    @GetMapping("/marketplace")
    public ResponseEntity<Map<String, Object>> get() {
        Map<String, Object> marketplace;

        marketplace = marketplaceService.get();

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, marketplace.get("id").toString()))
            .body(marketplace);
    }

}
