package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.zoop.PermissionZoop;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import io.micrometer.core.annotation.Timed;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class PermissionResource {

    private static final String ENTITY_NAME = "BankAccount";
    private final PermissionZoop permissionZoop;

    public PermissionResource(PermissionZoop permissionZoop) {
        this.permissionZoop = permissionZoop;
    }

    /**
     * Permissão do usuário Busca todas as permissões que usuário possui pelo
     * user_id
     */
    @GetMapping("/permission/{id}")
    public ResponseEntity<Map<String, Object>> getPermissionUser(@Valid @PathVariable String id) {
        Map<String, Object> bankAccount;

        bankAccount = permissionZoop.getPermissionUser(id);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(bankAccount);
    }


    /**
     * Criar permissão para usuário Cria uma nova permissão para o usuário,
     * podendo vincular à marketplace, seller ou grupo
     */
    @PostMapping("/permission")
    public ResponseEntity<Map<String, Object>> createPermission(@Valid @RequestParam(name = "id", defaultValue = "") String id,
                                                                @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> permission;

        permission = permissionZoop.createPermission(id, body);

        return ResponseEntity.created(new URI("/api/permission/" + id))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, id))
            .body(body);
    }

    /**
     * Permissão do usuário por permission_id Permissões que usuário possui pelo
     * permission_id
     */
    @GetMapping("/permission/{permissionId}/seller/{userId}")
    public ResponseEntity<Map<String, Object>> getPermissionUser(@Valid @PathVariable String permissionId,
                                                                 @RequestParam(name = "userId", defaultValue = "") String userId) {
        Map<String, Object> permission;

        permission = permissionZoop.getPermissionId(userId, permissionId);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, permissionId))
            .body(permission);
    }


    /**
     * Deleta uma permissão Remove uma permissões do usuário pelo permission_id
     */
    @DeleteMapping("/permission/{permissionId}/seller/{userId}")
    @Timed
    public ResponseEntity<Void> deletePermission(@Valid @PathVariable String permissionId,
                                                 @RequestParam(name = "userId", defaultValue = "") String userId) {
        permissionZoop.deletePermission(userId, permissionId);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, permissionId)).build();
    }

}
