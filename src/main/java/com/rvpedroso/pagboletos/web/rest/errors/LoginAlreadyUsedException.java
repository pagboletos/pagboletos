package com.rvpedroso.pagboletos.web.rest.errors;

public class LoginAlreadyUsedException extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public LoginAlreadyUsedException() {
        super(ErrorConstants.LOGIN_ALREADY_USED_TYPE, "Usuário já possui cadastro!", "userManagement", "userexists");
    }
}
