package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.BalanceService;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class BalanceResource {

    private final BalanceService balanceService;
    private static final String ENTITY_NAME = "Balance";

    public BalanceResource(BalanceService balanceService) {
        this.balanceService = balanceService;
    }

    /**
     * Recuperar saldo de conta por seller Recupera o saldo corrente e saldo
     * total de conta
     */
    @GetMapping("/balance/{balanceId}")
    public ResponseEntity<Map<String, Object>> get(@Valid @PathVariable String balanceId) {
        Map<String, Object> balance;

        balance = balanceService.get(balanceId);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, balance.get("id").toString()))
            .body(balance);
    }

    /**
     * Lista contas por seller
     */
    @GetMapping("/balance/sellers/{sellerId}")
    public ResponseEntity<Map<String, Object>> listBalanceSeller(@Valid @RequestParam(name = "sellerId", defaultValue = "") String sellerId) {
        Map<String, Object> balance;

        balance = balanceService.listBalanceSeller(sellerId);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, balance.get("id").toString()))
            .body(balance);
    }


    /**
     * Lista contas por buyer
     */
    @GetMapping("/balance/buyer/{buyerId}")
    public ResponseEntity<Map<String, Object>> listBalanceBuyer(@Valid @PathVariable String buyerId) {
        Map<String, Object> balance;

        balance = balanceService.listBalanceBuyer(buyerId);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, balance.get("id").toString()))
            .body(balance);
    }


    /**
     * Listar histórico de lançamentos pelo identificador da conta
     */
    @GetMapping("/balance/{balanceId}/history")
    public ResponseEntity<Map<String, Object>> listHistory(@Valid @PathVariable String balanceId) {
        Map<String, Object> balance;

        balance = balanceService.listHistory(balanceId);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, balance.get("id").toString()))
            .body(balance);
    }

    /**
     * Listar histórico de lançamentos de conta por seller Listagem de histórico
     * da conta principal do seller
     */
    @GetMapping("/balance/seller/history/{sellerId}")
    public ResponseEntity<Map<String, Object>> listHistorySeller(@Valid @RequestParam(name = "sellerId", defaultValue = "") String sellerId) {
        Map<String, Object> balance;

        balance = balanceService.listHistorySeller(sellerId);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, balance.get("id").toString()))
            .body(balance);
    }

    /**
     * Listar histórico de lançamentos de conta por buyer Listagem de histórico
     * da conta principal do buyer
     */

    @GetMapping("/balance/seller/Buyer/{buyerId}")
    public ResponseEntity<Map<String, Object>> listHistoryBuyer(@Valid @PathVariable String buyerId) {
        Map<String, Object> balance;

        balance = balanceService.listHistoryBuyer(buyerId);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, balance.get("id").toString()))
            .body(balance);
    }

}
