package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.zoop.TokenZoopZoop;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class TokenZoopResource {

    private static final String ENTITY_NAME = "BankAccount";

    private final TokenZoopZoop tokenZoopZoop;

    public TokenZoopResource(TokenZoopZoop tokenZoopZoop) {
        this.tokenZoopZoop = tokenZoopZoop;
    }

    /**
     * Criar novo token de cartão
     */
    @PostMapping("/token/card")
    public ResponseEntity<Map<String, Object>> createToken(@Valid @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> token;

        token = this.tokenZoopZoop.createToken(body);

        return ResponseEntity.created(new URI("/api/token/" + token.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, token.get("id").toString()))
            .body(body);
    }

    /**
     * Criar novo token de conta bancária
     */
    @PostMapping("/token/bank")
    public ResponseEntity<Map<String, Object>> createTokenBankAccounts(@Valid @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> token;

        token = this.tokenZoopZoop.createTokenBankAccounts(body);

        return ResponseEntity.created(new URI("/api/token/" + token.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, token.get("id").toString()))
            .body(body);
    }

    /**
     * Recuperar detalhes de token de cartão/conta bancária
     *
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/tokens/token_id
     */
    @GetMapping("/token/{id}")
    public ResponseEntity<Map<String, Object>> get(@Valid @PathVariable String id) {
        Map<String, Object> bankAccount;

        bankAccount = this.tokenZoopZoop.get(id);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(bankAccount);
    }
}
