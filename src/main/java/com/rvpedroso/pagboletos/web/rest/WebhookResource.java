package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.zoop.WebhookZoop;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import io.micrometer.core.annotation.Timed;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class WebhookResource {

    private static final String ENTITY_NAME = "Webhook";
    private final WebhookZoop webhookZoop;

    public WebhookResource(WebhookZoop webhookZoop) {
        this.webhookZoop = webhookZoop;
    }

    /**
     * Cria webhook por marketplace
     */
    @PostMapping("/webhook")
    public ResponseEntity<Map<String, Object>> create(@Valid @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> bankAccount;

        bankAccount = this.webhookZoop.create(body);

        return ResponseEntity.created(new URI("/api/webhook/" + bankAccount.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, bankAccount.get("id").toString()))
            .body(body);
    }

    /**
     * Listar webhooks por marketplace
     *
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/webhooks
     */
    public Map<String, Object> list() {
        return null;
    }

    @GetMapping("/webhook")
    @Timed
    public ResponseEntity<Map<String, Object>> list(Pageable pageable) {
        Map<String, Object> bankAccount;

        bankAccount = this.webhookZoop.list();
        return ResponseEntity.ok()
            .body(bankAccount);
    }

    /**
     * Recuperar detalhes de webhook
     */
    @GetMapping("/webhook/{id}")
    public ResponseEntity<Map<String, Object>> get(@Valid @PathVariable String id) {
        Map<String, Object> webhook;

        webhook = this.webhookZoop.get(id);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(webhook);
    }

    /**
     * Remover webhook
     */
    @DeleteMapping("/webhook/{id}")
    @Timed
    public ResponseEntity<Void> delete(@PathVariable String id) {
        this.webhookZoop.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
