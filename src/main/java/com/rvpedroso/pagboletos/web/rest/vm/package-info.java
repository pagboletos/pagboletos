/**
 * View Models used by Spring MVC REST controllers.
 */
package com.rvpedroso.pagboletos.web.rest.vm;
