package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.zoop.TransfersZoop;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import io.micrometer.core.annotation.Timed;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class TransfersResource {

    private static final String ENTITY_NAME = "Transfers";
    private final TransfersZoop transfersZoop;

    public TransfersResource(TransfersZoop transfersZoop) {
        this.transfersZoop = transfersZoop;
    }

    /**
     * Listar transferências por seller
     */
    @GetMapping("/transfers/seller/{sellerId}")
    public ResponseEntity<Map<String, Object>> listTransfersSeller(@Valid @RequestParam(name = "sellerId", defaultValue = "") String sellerId) {
        Map<String, Object> bankAccount;

        bankAccount = this.transfersZoop.listTransfersSeller(sellerId);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sellerId))
            .body(bankAccount);
    }

    /**
     * Criar transferência para bancária
     */
    @PostMapping("/transfers/{id}")
    public ResponseEntity<Map<String, Object>> createTransfersBank(@Valid @PathVariable String id, @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> transfers;

        transfers = this.transfersZoop.createTransfersBank(id, body);

        return ResponseEntity.created(new URI("/api/transfers/" + transfers.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, transfers.get("id").toString()))
            .body(body);
    }

    /**
     * Listar transferência por marketplace
     */
    @GetMapping("/transfers/marketplace")
    @Timed
    public ResponseEntity<Map<String, Object>> list(Pageable pageable) {
        Map<String, Object> bankAccount;

        bankAccount = this.transfersZoop.listTransfersMarketplaces();
        return ResponseEntity.ok()
            .body(bankAccount);
    }

    /**
     * Recuperar detalhes de transferência
     */
    @GetMapping("/transfers/{id}")
    public ResponseEntity<Map<String, Object>> get(@Valid @PathVariable String id) {
        Map<String, Object> bankAccount;

        bankAccount = this.transfersZoop.get(id);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(bankAccount);
    }

    /**
     * Listar transações associadas a transferência
     */
    @GetMapping("/transfers/transactions/{id}")
    public ResponseEntity<Map<String, Object>> list(@Valid @PathVariable String id) {
        Map<String, Object> bankAccount;

        bankAccount = this.transfersZoop.list(id);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(bankAccount);
    }


    /**
     * Criar transferência P2P
     */
    public Map<String, Object> transfersP2P(String ownerId, String receiverId,
                                            Map<String, Object> body) {
        return null;
    }

    @PostMapping("/transfers/owner/{ownerId}/receiver/{receiverId}")
    public ResponseEntity<Map<String, Object>> associate(@Valid @PathVariable String ownerId, @PathVariable String receiverId,
                                                         @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> transfers;

        transfers = this.transfersZoop.transfersP2P(ownerId, receiverId, body);

        return ResponseEntity.created(new URI("/api/transfers/" + transfers.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, transfers.get("id").toString()))
            .body(body);
    }


}
