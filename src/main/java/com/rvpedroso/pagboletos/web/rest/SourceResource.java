package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.zoop.SourceZoop;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import io.micrometer.core.annotation.Timed;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class SourceResource {

    private static final String ENTITY_NAME = "BankAccount";
    private final SourceZoop sourceZoop;

    public SourceResource(SourceZoop sourceZoop) {
        this.sourceZoop = sourceZoop;
    }

    /**
     * Criar source para utilizacão transação Um objeto source permite você
     * aceitar uma variedade de formas de pagamento
     */
    @PostMapping("/source")
    public ResponseEntity<Map<String, Object>> create(@Valid @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> source;

        source = this.sourceZoop.create(body);

        return ResponseEntity.created(new URI("/api/source/" + source.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, source.get("id").toString()))
            .body(body);
    }

    /**
     * Recuperar detalhes de source tão pelo identificador
     */
    @GetMapping("/source/{id}")
    public ResponseEntity<Map<String, Object>> get(@Valid @PathVariable String id) {
        Map<String, Object> source;

        source = this.sourceZoop.get(id);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(source);
    }

    /**
     * Remover source pelo identificador
     */
    @DeleteMapping("/source/{id}")
    @Timed
    public ResponseEntity<Void> delete(@PathVariable String id) {
        this.sourceZoop.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
