package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.zoop.ReceiptZoop;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class ReceiptResource {

    private static final String ENTITY_NAME = "Receipt";

    private final ReceiptZoop receiptZoop;

    public ReceiptResource(ReceiptZoop receiptZoop) {
        this.receiptZoop = receiptZoop;
    }

    /**
     * Enviar recibo por email
     */
    @PostMapping("/receipt/emails")
    public ResponseEntity<Map<String, Object>> sendEmail(@Valid @RequestParam(name = "id") String id) throws URISyntaxException {
        Map<String, Object> receipt;

        receipt = this.receiptZoop.sendEmail(id);

        return ResponseEntity.created(new URI("/api/receipt/" + receipt.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, receipt.get("id").toString()))
            .body(receipt);
    }

    /**
     * Enviar recibo por SMS
     */
    @PostMapping("/receipt/sms")
    public ResponseEntity<Map<String, Object>> sendSMS(@Valid @RequestParam(name = "id") String id) throws URISyntaxException {
        Map<String, Object> receipt;

        receipt = this.receiptZoop.sendSMS(id);

        return ResponseEntity.created(new URI("/api/receipt/" + receipt.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, receipt.get("id").toString()))
            .body(receipt);
    }

    /**
     * Enviar recibo por sms.email
     */
    @PostMapping("/receipt/sms_email")
    public ResponseEntity<Map<String, Object>> sendSMSEmail(@Valid @RequestParam(name = "id") String id) throws URISyntaxException {
        Map<String, Object> receipt;

        receipt = this.receiptZoop.sendSMSEmail(id);

        return ResponseEntity.created(new URI("/api/receipt/" + receipt.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, receipt.get("id").toString()))
            .body(receipt);
    }

    /**
     * Recuperar detalhes do recibo
     */
    @GetMapping("/receipt/{id}")
    public ResponseEntity<Map<String, Object>> get(@Valid @PathVariable String id) {
        Map<String, Object> receipt;

        receipt = this.receiptZoop.get(id);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(receipt);
    }

    /**
     * Alterar detalhes do recibo
     */
    @PutMapping("/receipt/{id}")
    public ResponseEntity<Map<String, Object>> updateIndividuals(@Valid @PathVariable String id, @RequestBody Map<String, Object> body) {
        Map<String, Object> receipt;

        receipt = this.receiptZoop.update(id, body);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(receipt);
    }

    /**
     * Renderizar template de recibo HTML
     */
    @GetMapping("/receipt/render/{id}")
    public ResponseEntity<Map<String, Object>> render(@Valid @PathVariable String id) {
        Map<String, Object> receipt;

        receipt = this.receiptZoop.render(id);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(receipt);
    }

}
