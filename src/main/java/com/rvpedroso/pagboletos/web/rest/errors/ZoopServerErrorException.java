package com.rvpedroso.pagboletos.web.rest.errors;

import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.StatusType;

import java.net.URI;

/**
 * Simple exception with a message, that returns an Internal Server Error code.
 */
public class ZoopServerErrorException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    public ZoopServerErrorException(ValidationException e) {
        super(URI.create(e.getErrors().getError().getCategory()), e.getResponseStatus(), new StatusType() {
            @Override
            public int getStatusCode() {
                return e.getResponseCode();
            }

            @Override
            public String getReasonPhrase() {
                return e.getErrors().getError().getType();
            }
        });
    }
}
