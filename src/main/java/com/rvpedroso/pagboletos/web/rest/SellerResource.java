package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.SellerService;
import com.rvpedroso.pagboletos.web.rest.model.Paginate;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class SellerResource {

    private final Logger log = LoggerFactory.getLogger(SellerResource.class);
    private static final String ENTITY_NAME = "Sellers";

    private final SellerService sellerService;

    public SellerResource(SellerService sellerService) {
        this.sellerService = sellerService;
    }

    /**
     * Criar novo vendedor do tipo indivíduo - Pessoa Física
     *
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers/individuals
     */
    @PostMapping("/seller/individuals")
    public ResponseEntity<Map<String, Object>> createIndividuals(@Valid @RequestBody Map<String, Object> body) throws URISyntaxException {
        log.debug("Criando novo vendedor do tipo indivíduo : {}", body);
        Map<String, Object> seller;

        seller = sellerService.createIndividuals(body);

        return ResponseEntity.created(new URI("/api/users/" + seller.get("id")))
            .headers(HeaderUtil.createAlert("seller.created", seller.get("id").toString()))
            .body(body);
    }

    /**
     * Criar novo vendedor do tipo indivíduo pelo parceiro - Pessoa Física
     *
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers/individuals
     */
    @PostMapping("/seller/individuals/parceiro")
    public ResponseEntity<Map<String, Object>> createIndividualsParceiro(@Valid @RequestBody Map<String, Object> body) throws URISyntaxException {
        log.debug("Criando novo vendedor do tipo indivíduo : {}", body);
        Map<String, Object> seller;

        seller = sellerService.createIndividualsParceiro(body);

        return ResponseEntity.created(new URI("/api/users/" + seller.get("id")))
            .headers(HeaderUtil.createAlert("seller.created", seller.get("id").toString()))
            .body(body);
    }


    /**
     * Alterar um vendedor do tipo indivíduo - Pessoa Física
     *
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers/individuals/id
     */

    @PutMapping("/seller/{id}")
    public ResponseEntity<Map<String, Object>> updateIndividuals(@Valid @PathVariable String id,
                                                                 @RequestBody Map<String, Object> body) {
        log.debug("Alterando um vendedor do tipo indivíduo : {}", body);
        Map<String, Object> seller;

        seller = sellerService.updateIndividuals(id, body);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, seller.get("id").toString()))
            .body(seller);

    }

    /**
     * Criar novo vendedor do tipo indivíduo - Pessoa Jurídica
     *
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers/businesses
     */
    @PostMapping("/seller/businesses")
    public ResponseEntity<Map<String, Object>> createBusinesses(@Valid @RequestBody Map<String, Object> body) throws URISyntaxException {
        log.debug("Criando novo vendedor do tipo indivíduo  : {}", body);
        Map<String, Object> seller;

        seller = sellerService.createBusinesses(body);

        return ResponseEntity.created(new URI("/api/users/" + seller.get("id")))
            .headers(HeaderUtil.createAlert("userManagement.created", seller.get("id").toString()))
            .body(body);
    }


    /**
     * Alterar um vendedor do tipo indivíduo - Pessoa Jurídica
     *
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers/businesses/id
     */
    @PutMapping("/seller/businesses/{id}")
    public ResponseEntity<Map<String, Object>> updateBusinesses(@Valid @PathVariable String id,
                                                                @RequestBody Map<String, Object> body) {
        log.debug("Alterando um vendedor do tipo indivíduo : {}", body);
        Map<String, Object> seller;

        seller = sellerService.updateBusinesses(id, body);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, seller.get("id").toString()))
            .body(seller);

    }

    /**
     * Recupera detalhes de vendedor pelo id
     *
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers/{id}
     */
    @GetMapping("/seller/{id}")
    public ResponseEntity<Map<String, Object>> getId(@Valid @PathVariable String id) {
        log.debug("Recuperando detalhes de vendedor pelo id : {}", id);
        Map<String, Object> seller;

        seller = sellerService.get(id);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, seller.get("id").toString()))
            .body(seller);

    }

    /**
     * Listando vendedores
     *
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers
     */
    @GetMapping("/seller")
    public ResponseEntity<Object> list(Paginate pageable) {
        log.debug("Listando vendedores");
        Map<String, Object> seller;

        seller = sellerService.list(pageable);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, seller.toString()))
            .body(seller.get("items"));
    }

    /**
     * Pesquisando vendedor
     *
     * @param cpf {@code Setup} {"taxpayer_id": "cpf","ein": "CNPJ"
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers/search
     */
    @GetMapping("/seller/search/individuals/{cpf}")
    public ResponseEntity<Map<String, Object>> search(@Valid @PathVariable String cpf) {
        log.debug("Pesquisando vendedor pelo CPF : {}", cpf);
        Map<String, Object> seller;

        seller = sellerService.searchIndividuals(cpf);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, seller.get("id").toString()))
            .body(seller);

    }

    /**
     * Pesquisando vendedor
     *
     * @param cnpj {@code Setup}
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers/search
     */
    @PostMapping("/seller/search/businesses/{cnpj}")
    public ResponseEntity<Map<String, Object>> searchIndividuals(@Valid @PathVariable String cnpj) {
        log.debug("Pesquisando vendedor pelo CNPJ : {}", cnpj);
        Map<String, Object> seller;

        seller = sellerService.searchIndividuals(cnpj);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, seller.get("id").toString()))
            .body(seller);

    }


    /**
     * Criar novo vendedor do tipo indivíduo pelo parceiro - Pessoa Física
     *
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers/individuals
     */
    @PostMapping("/seller/check")
    public ResponseEntity verificarLoginExistente(@Valid @RequestBody Map<String, Object> body) {
        log.debug("Verifica vendedor do tipo indivíduo : {}", body);
        sellerService.verificarLoginExistente(body);
        return new ResponseEntity(HttpStatus.OK);

    }

}
