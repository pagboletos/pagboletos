package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.BuyerService;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import io.micrometer.core.annotation.Timed;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class BuyerResource {

    private BuyerService buyerService;
    private static final String ENTITY_NAME = "Buyer";

    public BuyerResource(BuyerService buyerService) {
        this.buyerService = buyerService;
    }

    /**
     * Criar comprador
     */
    @PostMapping("/buyer")
    public ResponseEntity<Map<String, Object>> create(@Valid @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> buyer;

        buyer = buyerService.create(body);

        return ResponseEntity.created(new URI("/api/buyer/" + buyer.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, buyer.get("id").toString()))
            .body(body);
    }

    /**
     * Alterar detalhes de comprador
     */
    @PutMapping("/buyer/{idBuyer}")
    public ResponseEntity<Map<String, Object>> update(@Valid @PathVariable String idBuyer, @RequestBody Map<String, Object> body) {
        Map<String, Object> bankAccount;

        bankAccount = buyerService.update(idBuyer, body);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, idBuyer))
            .body(bankAccount);
    }

    /**
     * Recuperar detalhes de comprador
     */
    @GetMapping("/buyer/{idBuyer}")
    public ResponseEntity<Map<String, Object>> get(@Valid @PathVariable String idBuyer) {
        Map<String, Object> bankAccount;

        bankAccount = buyerService.get(idBuyer);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, idBuyer))
            .body(bankAccount);
    }

    /**
     * Deleta um de vendedor pelo id
     */
    @DeleteMapping("/buyer/{idBuyer}")
    @Timed
    public ResponseEntity<Void> delete(@PathVariable String idBuyer) {
        buyerService.delete(idBuyer);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, idBuyer)).build();
    }

    /**
     * Listar comprador por marketplace
     */
    @GetMapping("/buyer")
    @Timed
    public ResponseEntity<Map<String, Object>> list(Pageable pageable) {
        Map<String, Object> bankAccount;

        bankAccount = buyerService.list();
        return ResponseEntity.ok()
            .body(bankAccount);
    }

    /**
     * Buscar comprador por CPF/CNPJ
     *
     * @param body {@code Setup} {"taxpayer_id": "cpf","ein": "CNPJ"
     */

    @PostMapping("/buyer/search")
    public ResponseEntity<Map<String, Object>> search(@Valid @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> buyer;

        buyer = buyerService.search(body);

        return ResponseEntity.created(new URI("/api/buyer/" + buyer.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, buyer.get("id").toString()))
            .body(body);
    }

}
