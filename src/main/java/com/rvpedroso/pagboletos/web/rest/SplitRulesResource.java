package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.zoop.SplitRulesZoop;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import io.micrometer.core.annotation.Timed;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class SplitRulesResource {

    private static final String ENTITY_NAME = "SplitRules";
    private final SplitRulesZoop splitRulesZoop;

    public SplitRulesResource(SplitRulesZoop splitRulesZoop) {
        this.splitRulesZoop = splitRulesZoop;
    }

    /**
     * Recuperar detalhes de regra de divisão por transação
     */
    @GetMapping("/split_rules/{id}")
    public ResponseEntity<Map<String, Object>> get(@Valid @PathVariable String id) {
        Map<String, Object> splitRules;

        splitRules = this.splitRulesZoop.get(id);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(splitRules);
    }

    /**
     * Cria regra de divisão por transação
     */
    @GetMapping("/split_rules/transactions/{id}")
    public ResponseEntity<Map<String, Object>> createcreate(@Valid @PathVariable String id,
                                                            @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> splitRules;

        splitRules = this.splitRulesZoop.create(body, id);

        return ResponseEntity.created(new URI("/api/split_rules/" + splitRules.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, splitRules.get("id").toString()))
            .body(body);
    }

    /**
     * Recupera detalhes de regra de divisão por transação
     */
    @GetMapping("/split_rules/{idSplit}/transactions/{idTransactions}")
    public ResponseEntity<Map<String, Object>> details(@Valid @PathVariable String idSplit, @PathVariable String idTransactions) {
        Map<String, Object> splitRules;

        splitRules = this.splitRulesZoop.details(idTransactions, idSplit);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, idSplit))
            .body(splitRules);
    }

    /**
     * Alterar regra de divisão por transação
     */
    @PutMapping("/split_rules/{idSplit}/transactions/{idTransactions}")
    public ResponseEntity<Map<String, Object>> update(@Valid @PathVariable String idSplit, @PathVariable String idTransactions
        , @RequestBody Map<String, Object> body) {
        Map<String, Object> splitRules;

        splitRules = this.splitRulesZoop.update(body, idTransactions, idSplit);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, idSplit))
            .body(splitRules);
    }

    /**
     * Remover regra de divisão por transação
     */
    @DeleteMapping("/split_rules/{idSplit}/transactions/{idTransactions}")
    @Timed
    public ResponseEntity<Void> delete(@Valid @PathVariable String idSplit, @PathVariable String idTransactions) {
        this.splitRulesZoop.delete(idTransactions, idSplit);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, idSplit)).build();
    }
}
