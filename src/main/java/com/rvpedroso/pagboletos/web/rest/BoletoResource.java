package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.BoletoService;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class BoletoResource {

    private final BoletoService boletoService;
    private static final String ENTITY_NAME = "Boleto";

    public BoletoResource(BoletoService boletoService) {
        this.boletoService = boletoService;
    }

    /**
     * Enviar cobrança de boleto por email
     */
    @PostMapping("/boleto/{id}/emails")
    public ResponseEntity<Map<String, Object>> sendBillingEmail(@Valid @PathVariable String id) throws URISyntaxException {
        Map<String, Object> boleto;

        boleto = boletoService.sendBillingEmail(id);

        return ResponseEntity.created(new URI("/api/users/" + boleto.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, id))
            .body(boleto);
    }

    /**
     * Recuperar detalhes de boleto
     */
    @GetMapping("/boleto/{id}")
    public ResponseEntity<Map<String, Object>> get(@Valid @PathVariable String id) {
        Map<String, Object> bankAccount;

        bankAccount = boletoService.get(id);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(bankAccount);
    }

}
