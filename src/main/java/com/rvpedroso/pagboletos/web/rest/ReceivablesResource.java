package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.zoop.ReceivablesZoop;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class ReceivablesResource {

    private static final String ENTITY_NAME = "Receivables";
    private final ReceivablesZoop receivablesZoop;

    public ReceivablesResource(ReceivablesZoop receivablesZoop) {
        this.receivablesZoop = receivablesZoop;
    }

    /**
     * Recuperar detalhes de recebível
     */
    @GetMapping("/receivables/{id}")
    public ResponseEntity<Map<String, Object>> get(@Valid @PathVariable String id) {
        Map<String, Object> receivables;

        receivables = this.receivablesZoop.get(id);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(receivables);
    }

    /**
     * Listar recebíveis por transação Listar todas as parcelas de recebimento
     * por transação
     */
    @GetMapping("/receivables/transactions/{id}")
    public ResponseEntity<Map<String, Object>> getTransactions(@Valid @PathVariable String id) {
        Map<String, Object> receivables;

        receivables = this.receivablesZoop.getTransactions(id);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(receivables);
    }

    /**
     * Listar recebíveis por seller
     * Listar todas as parcelas de recebimento por seller
     */
    @GetMapping("/receivables/seller/{sellerId}")
    public ResponseEntity<Map<String, Object>> listSeller(@Valid @RequestParam(name = "sellerId", defaultValue = "") String sellerId) {
        Map<String, Object> receivables;

        receivables = this.receivablesZoop.getSeller(sellerId);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sellerId))
            .body(receivables);
    }

}
