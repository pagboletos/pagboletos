package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.DocumentsService;
import com.rvpedroso.pagboletos.web.rest.model.Paginate;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import io.micrometer.core.annotation.Timed;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class DocumentsResource {

    private final DocumentsService documentsService;
    private static final String ENTITY_NAME = "documentos";

    public DocumentsResource(DocumentsService documentsService) {
        this.documentsService = documentsService;
    }

    /**
     * Criar documento de cadastro de vendedor
     */
    @PostMapping("/documents/seller/{id}/{tipo}")
    public ResponseEntity<Map<String, Object>> create(@Valid @PathVariable String id, @PathVariable String tipo,
                                                      @RequestParam("file") MultipartFile files) throws URISyntaxException {
        Map<String, Object> result = documentsService.create(id, files, tipo);
        return ResponseEntity.created(new URI("/api/vendedors/" + result.get("id").toString()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.get("id").toString()))
            .body(result);
    }

    /**
     * Atualiza os dados de um documento de um vendedoro.
     */
    @PutMapping("/documents/{id}")
    public ResponseEntity<Map<String, Object>> update(@Valid @PathVariable String id, @RequestBody Map<String, Object> body) {
        Map<String, Object> bankAccount;

        bankAccount = documentsService.update(id, body);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(bankAccount);
    }

    /**
     * Listar documentos de um vendedor
     */
    @GetMapping("/documents/seller/{id}")
    @Timed
    public ResponseEntity<Map<String, Object>> list(@Valid @PathVariable String id, Paginate paginate) {
        Map<String, Object> bankAccount;

        bankAccount = documentsService.list(id, paginate);
        return ResponseEntity.ok()
            .body(bankAccount);
    }
}
