package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.zoop.SubscriptionsZoop;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import io.micrometer.core.annotation.Timed;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class SubscriptionsResource {

    private static final String ENTITY_NAME = "Subscriptions";
    private final SubscriptionsZoop subscriptionsZoop;

    public SubscriptionsResource(SubscriptionsZoop subscriptionsZoop) {
        this.subscriptionsZoop = subscriptionsZoop;
    }

    /**
     * Recuperar todas as assinatura de um marketplace
     */
    @GetMapping("/subscriptions")
    @Timed
    public ResponseEntity<Map<String, Object>> list(Pageable pageable) {
        Map<String, Object> subscriptions;

        subscriptions = this.subscriptionsZoop.list();
        return ResponseEntity.ok()
            .body(subscriptions);
    }

    /**
     * Criar uma assinatura entre um comprador e um plano
     */
    @PostMapping("/subscriptions")
    public ResponseEntity<Map<String, Object>> create(@Valid @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> subscriptions;

        subscriptions = this.subscriptionsZoop.create(body);

        return ResponseEntity.created(new URI("/api/subscriptions/" + subscriptions.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, subscriptions.get("id").toString()))
            .body(body);
    }

    /**
     * Alterar os detalhes de uma assinatura pelo identificador
     */
    @PutMapping("/subscriptions/{id}")
    public ResponseEntity<Map<String, Object>> update(@Valid @PathVariable String id, @RequestBody Map<String, Object> body) {
        Map<String, Object> subscriptions;

        subscriptions = this.subscriptionsZoop.update(id, body);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(subscriptions);
    }

    /**
     * Alterar os detalhes de uma assinatura pelo identificador
     */
    @DeleteMapping("/subscriptions/{id}")
    @Timed
    public ResponseEntity<Void> delete(@PathVariable String id) {
        this.subscriptionsZoop.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * Suspender uma assinatura pelo identificador
     */
    @PostMapping("/subscriptions/suspend/{id}")
    public ResponseEntity<Map<String, Object>> suspendSignature(@Valid @PathVariable String id, @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> subscriptions;

        subscriptions = this.subscriptionsZoop.suspendSignature(id, body);

        return ResponseEntity.created(new URI("/api/subscriptions/" + subscriptions.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, subscriptions.get("id").toString()))
            .body(body);
    }

    /**
     * Suspender uma assinatura pelo identificador
     */
    @PostMapping("/subscriptions/reactivate/{id}")
    public ResponseEntity<Map<String, Object>> reactivateSignature(@Valid @PathVariable String id, @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> subscriptions;

        subscriptions = this.subscriptionsZoop.reactivateSignature(id, body);

        return ResponseEntity.created(new URI("/api/subscriptions/" + subscriptions.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, subscriptions.get("id").toString()))
            .body(body);
    }
}
