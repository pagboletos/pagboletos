package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.EventsService;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class EventsResource {

    private final EventsService eventsService;
    private static final String ENTITY_NAME = "Events";

    public EventsResource(EventsService eventsService) {
        this.eventsService = eventsService;
    }

    /**
     * Recuperar detalhes de evento pelo identificador
     */
    @GetMapping("/events/{id}")
    public ResponseEntity<Map<String, Object>> get(@Valid @PathVariable String id) {
        Map<String, Object> events;

        events = eventsService.get(id);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(events);
    }

    /**
     * Listar eventos por marketplace
     */
    @GetMapping("/events")
    public ResponseEntity<Map<String, Object>> list() {
        Map<String, Object> events;

        events = eventsService.list();

        return ResponseEntity.ok()
            .body(events);
    }
}
