package com.rvpedroso.pagboletos.web.rest.model;

/**
 * Usuário: william
 * Data: 16/04/19
 * Horário: 12
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest.model
 **/
public class Paginate {

    public Integer limit;
    public String sort;
    public Integer offset;
    public String dateRange;
    public Boolean collapse;
    public String sortField;

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public String getDateRange() {
        return dateRange;
    }

    public void setDateRange(String dateRange) {
        this.dateRange = dateRange;
    }

    public Boolean getCollapse() {
        return collapse;
    }

    public void setCollapse(Boolean collapse) {
        this.collapse = collapse;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    @Override
    public String toString() {

        String retorno = "";
        if (limit != null) retorno += "limit=" + limit;
        if (sort != null) retorno += "&sort=" + sort;
        if (offset != null) retorno += "&offset=" + offset;
        if (dateRange != null) retorno += "&date_range=" + dateRange;
        if (sortField != null) retorno += "&sort_field=" + sortField;

        return retorno;

    }
}
