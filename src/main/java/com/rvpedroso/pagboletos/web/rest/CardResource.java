package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.CardService;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import io.micrometer.core.annotation.Timed;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class CardResource {

    private static final String ENTITY_NAME = "Card";
    private final CardService cardService;

    public CardResource(CardService cardService) {
        this.cardService = cardService;
    }

    /**
     * Associar cartão com comprador Associar token de cartão com comprador
     */
    @PostMapping("/card")
    public ResponseEntity<Map<String, Object>> associate(@Valid @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> card;

        card = cardService.associate(body);

        return ResponseEntity.created(new URI("/api/card/" + card.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, card.get("id").toString()))
            .body(body);
    }

    /**
     * Recuperar detalhes de cartão pelo identificador
     */
    @GetMapping("/card/{cardId}")
    public ResponseEntity<Map<String, Object>> get(@Valid @PathVariable String cardId) {
        Map<String, Object> card;

        card = cardService.get(cardId);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cardId))
            .body(card);
    }

    /**
     * Remover cartão pelo identificador
     */
    @DeleteMapping("/card/{id}")
    @Timed
    public ResponseEntity<Void> delete(@PathVariable String id) {
        cardService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * Atualizar detalhes de cartão pelo identificador
     */
    @PutMapping("/card/{id}")
    public ResponseEntity<Map<String, Object>> update(@Valid @PathVariable String id, @RequestBody Map<String, Object> body) {
        Map<String, Object> card;

        card = cardService.update(body, id);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(card);
    }
}
