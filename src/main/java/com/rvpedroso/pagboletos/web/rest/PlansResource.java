package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.PlansService;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import io.micrometer.core.annotation.Timed;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class PlansResource {

    private static final String ENTITY_NAME = "Plans";
    private final PlansService plansService;

    public PlansResource(PlansService plansService) {
        this.plansService = plansService;
    }


    /**
     * Criar plano de vendas Planos com taxa e políticas para credito de
     * recebíveis
     */
    @PostMapping("/plans")
    public ResponseEntity<Map<String, Object>> create(@Valid @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> plans;

        plans = plansService.create(body);

        return ResponseEntity.created(new URI("/api/plans/" + plans.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, plans.get("id").toString()))
            .body(body);
    }

    /**
     * Listar plano de vendas por marketplace
     */
    @GetMapping("/plans")
    @Timed
    public ResponseEntity<Map<String, Object>> list(Pageable pageable) {
        Map<String, Object> plans;

        plans = plansService.list();
        return ResponseEntity.ok()
            .body(plans);
    }

    /**
     * Recuperar detalhes de plano de venda
     */
    @GetMapping("/plans/{id}")
    public ResponseEntity<Map<String, Object>> get(@Valid @PathVariable String id) {
        Map<String, Object> plans;

        plans = plansService.get(id);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(plans);
    }

    /**
     * Remover plano de venda
     */
    @DeleteMapping("/plans/{id}")
    @Timed
    public ResponseEntity<Void> delete(@PathVariable String id) {
        plansService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * Criar assinatura de plano de venda
     */
    @PostMapping("/plans/subscriptions")
    public ResponseEntity<Map<String, Object>> createSubscriptions(@Valid @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> plans;

        plans = plansService.createSubscriptions(body);

        return ResponseEntity.created(new URI("/api/plans/" + plans.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, plans.get("id").toString()))
            .body(body);
    }

    /**
     * Listar assinaturas de plano de venda
     */
    @GetMapping("/plans/subscriptions")
    @Timed
    public ResponseEntity<Map<String, Object>> listSubscriptions(Pageable pageable) {
        Map<String, Object> plans;

        plans = plansService.listSubscriptions();
        return ResponseEntity.ok()
            .body(plans);
    }

    /**
     * Recuperar detalhes de assinatura de plano de venda
     */
    @GetMapping("/plans/subscriptions/{id}")
    public ResponseEntity<Map<String, Object>> getSubscription(@Valid @PathVariable String id) {
        Map<String, Object> plans;

        plans = plansService.getSubscription(id);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(plans);
    }

    /**
     * Remover plano de venda
     */
    @DeleteMapping("/plans/subscriptions/{id}")
    @Timed
    public ResponseEntity<Void> deleteSubscriptions(@PathVariable String id) {
        plansService.deleteSubscriptions(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * Recuperar detalhes de assinatura de plano de venda
     */
    @GetMapping("/plans/seller/{sellerId}")
    public ResponseEntity<Map<String, Object>> getSubscriptionSeller(@Valid @RequestParam(name = "sellerId", defaultValue = "") String sellerId) {
        Map<String, Object> plans;

        plans = plansService.getSubscriptionSeller(sellerId);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sellerId))
            .body(plans);
    }

    /**
     * Alterar plano pelo identificador Alterar detalhes de um plano existente
     * pelo identificador
     */
    @PutMapping("/plans/{id}")
    public ResponseEntity<Map<String, Object>> update(@Valid @PathVariable String id, @RequestBody Map<String, Object> body) {
        Map<String, Object> plans;

        plans = plansService.update(id, body);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(plans);
    }

    /**
     * Deletar um plano pelo identificador
     */
    @DeleteMapping("/plans/identificador/{id}")
    @Timed
    public ResponseEntity<Void> deletePlans(@PathVariable String id) {
        plansService.deletePlans(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

}
