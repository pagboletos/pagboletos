package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.BankAccountService;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import io.micrometer.core.annotation.Timed;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class BankAccountResource {

    private final BankAccountService bankAccountService;
    private static final String ENTITY_NAME = "BankAccount";

    public BankAccountResource(BankAccountService bankAccountService) {
        this.bankAccountService = bankAccountService;
    }

    /**
     * Recuperar detalhes de conta bancária por identificador
     */
    @GetMapping("/bank_account/{bankAccountiId}")
    public ResponseEntity<Map<String, Object>> get(@Valid @PathVariable String bankAccountiId) {
        Map<String, Object> bankAccount;

        bankAccount = bankAccountService.get(bankAccountiId);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bankAccountiId))
            .body(bankAccount);
    }

    /**
     * Alterar detalhes de conta bancária
     */
    @PutMapping("/bank_account/{id}")
    public ResponseEntity<Map<String, Object>> updateIndividuals(@Valid @PathVariable String id, @RequestBody Map<String, Object> body) {
        Map<String, Object> bankAccount;

        bankAccount = bankAccountService.updateIndividuals(id, body);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, id))
            .body(bankAccount);
    }

    /**
     * Deleta conta bancária
     */
    @DeleteMapping("/bank_account/{id}")
    @Timed
    public ResponseEntity<Void> delete(@PathVariable String id) {
        bankAccountService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * Listas as contas bancárias
     */
    @GetMapping("/bank_account")
    @Timed
    public ResponseEntity<Map<String, Object>> list(Pageable pageable) {
        Map<String, Object> bankAccount;

        bankAccount = bankAccountService.list();
        return ResponseEntity.ok()
            .body(bankAccount);
    }

    /**
     * Associar conta bancaria com customer Associar token de cartão com
     * comprador
     */
    @PostMapping("/bank_account/token")
    public ResponseEntity<Map<String, Object>> associate(@Valid @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> bankAccount;

        bankAccount = bankAccountService.associate(body);

        return ResponseEntity.created(new URI("/api/users/" + bankAccount.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, bankAccount.get("id").toString()))
            .body(body);
    }

    /**
     * Listar contas bancárias por seller
     */
    @GetMapping("/bank_account/seller/{sellerId}")
    public ResponseEntity<Map<String, Object>> listSeller(@Valid @RequestParam(name = "sellerId", defaultValue = "") String sellerId) {
        Map<String, Object> bankAccount;

        bankAccount = bankAccountService.listSeller(sellerId);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sellerId))
            .body(bankAccount);
    }
}
