package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.MCCsService;
import io.micrometer.core.annotation.Timed;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class MCCsResource {

    private MCCsService mcCsService;

    public MCCsResource(MCCsService mcCsService) {
        this.mcCsService = mcCsService;
    }

    /**
     * Listar MCCs (Merchant Category Codes)
     */
    @GetMapping("/mccs")
    @Timed
    public ResponseEntity<Map<String, Object>> list(Pageable pageable) {
        Map<String, Object> bankAccount;

        bankAccount = mcCsService.list();
        return ResponseEntity.ok()
            .body(bankAccount);
    }


}
