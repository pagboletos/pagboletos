package com.rvpedroso.pagboletos.web.rest;

import com.rvpedroso.pagboletos.service.zoop.InvoicesZoop;
import com.rvpedroso.pagboletos.web.rest.util.HeaderUtil;
import io.micrometer.core.annotation.Timed;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.web.rest
 **/
@RestController
@RequestMapping("/api")
public class InvoicesResource {

    private static final String ENTITY_NAME = "Invoices";
    private final InvoicesZoop invoicesZoop;

    public InvoicesResource(InvoicesZoop invoicesZoop) {
        this.invoicesZoop = invoicesZoop;
    }

    /**
     * Recuperar todas as faturas de um marketplace
     */
    @GetMapping("/invoices")
    @Timed
    public ResponseEntity<Map<String, Object>> list(Pageable pageable) {
        Map<String, Object> invoices;

        invoices = invoicesZoop.list();
        return ResponseEntity.ok()
            .body(invoices);
    }

    /**
     * Criar uma fatura avulsa
     */
    @PostMapping("/invoices")
    public ResponseEntity<Map<String, Object>> create(@Valid @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> invoices;

        invoices = invoicesZoop.create(body);

        return ResponseEntity.created(new URI("/api/invoices/" + invoices.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, invoices.get("id").toString()))
            .body(body);
    }

    /**
     * Recuperar os detalhes de uma fatura pelo identificador - Você pode
     * recuperar os detalhes de uma determinada fatura através do identificador
     */
    @GetMapping("/invoices/{invoiceId}")
    public ResponseEntity<Map<String, Object>> get(@Valid @PathVariable String invoiceId) {
        Map<String, Object> invoices;

        invoices = invoicesZoop.get(invoiceId);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, invoiceId))
            .body(invoices);
    }

    /**
     * Alterar detalhes de uma fatura pelo identificador
     */
    @PutMapping("/invoices/{invoiceId}")
    public ResponseEntity<Map<String, Object>> update(@Valid @PathVariable String invoiceId, @RequestBody Map<String, Object> body) {
        Map<String, Object> invoices;

        invoices = invoicesZoop.update(invoiceId, body);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, invoiceId))
            .body(invoices);
    }

    /**
     * Remover uma fatura pelo identificador
     */
    @DeleteMapping("/invoices/{invoiceId}")
    @Timed
    public ResponseEntity<Void> delete(@PathVariable String invoiceId) {
        invoicesZoop.delete(invoiceId);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, invoiceId)).build();
    }

    /**
     * Aprovar fatura pendente - Aprovar manualmente uma fatura pendente, sem
     * que seja feita a cobrança ao cobrador
     */
    @PostMapping("/invoices/approve/{invoiceId}")
    public ResponseEntity<Map<String, Object>> approveInvoicePending(@Valid @PathVariable String invoiceId,
                                                                     @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> invoices;

        invoices = invoicesZoop.approveInvoicePending(invoiceId, body);

        return ResponseEntity.created(new URI("/api/invoices/" + invoices.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, invoices.get("id").toString()))
            .body(body);
    }

    /**
     * Estornar e reembolsar fatura - Estornar uma fatura paga, sendo feito o
     * processamento do estorno da forma de cobrança associada ao pagamento da
     * fatura
     */
    @PostMapping("/invoices/void/{invoiceId}")
    public ResponseEntity<Map<String, Object>> reverseInvoicePending(@Valid @PathVariable String invoiceId,
                                                                     @RequestBody Map<String, Object> body) throws URISyntaxException {
        Map<String, Object> invoices;

        invoices = invoicesZoop.reverseInvoicePending(invoiceId, body);

        return ResponseEntity.created(new URI("/api/invoices/" + invoices.get("id")))
            .headers(HeaderUtil.createAlert(ENTITY_NAME, invoices.get("id").toString()))
            .body(body);
    }

    /**
     * Recuperar faturas associadas a um vendedor pelo identificador - Recuperar
     * os detalhes de todas as faturas associadas a um determinado vendedor pelo
     * identificador
     */
    @GetMapping("/invoices/seller/{sellerId}")
    public ResponseEntity<Map<String, Object>> getinvoicesSeller(@Valid @RequestParam(name = "sellerId", defaultValue = "") String sellerId) {
        Map<String, Object> invoices;

        invoices = invoicesZoop.getinvoicesSeller(sellerId);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sellerId))
            .body(invoices);
    }
}
