package com.rvpedroso.pagboletos.zoop.auth;

import java.net.HttpURLConnection;

/**
 *
 * @author william-fernandes
 */
public interface Authentication {

    /**
     *
     * @param connection
     */
    void authenticate(HttpURLConnection connection);
}
