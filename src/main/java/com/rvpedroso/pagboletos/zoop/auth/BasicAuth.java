package com.rvpedroso.pagboletos.zoop.auth;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author william-fernandes
 */
public class BasicAuth implements Authentication {

    private final String key;

    /**
     *
     * @param key
     */
    public BasicAuth(String key) {
        this.key = key;
    }

    /**
     *
     * @return
     */
    public String getKey() {
        return key;
    }

    /**
     *
     * @param connection
     */
    @Override
    public void authenticate(HttpURLConnection connection) {
        String auth = key + ":" + "";
        byte[] message;
        String authorization = null;
        try {
            message = auth.getBytes("UTF-8");
            authorization = DatatypeConverter.printBase64Binary(message);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BasicAuth.class.getName()).log(Level.SEVERE, null, ex);
        }

        connection.addRequestProperty("Authorization", "Basic " + authorization);
    }
}
