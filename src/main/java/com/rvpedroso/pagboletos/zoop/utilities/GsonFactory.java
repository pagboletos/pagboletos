package com.rvpedroso.pagboletos.zoop.utilities;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rvpedroso.pagboletos.zoop.api.request.APIDateRequest;
import com.rvpedroso.pagboletos.zoop.models.APIDate;

import java.util.Date;

/**
 *
 * @author william-fernandes
 */
public class GsonFactory {

    /**
     *
     * @return
     */
    public static Gson gson() {
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .registerTypeAdapter(Date.class, new BirthDateRequestSerializer())
                .registerTypeAdapter(APIDateRequest.class, new ApiDateSerializer())
                .registerTypeAdapter(APIDate.class, new ApiDateDeserializer())
                .create();
    }
}
