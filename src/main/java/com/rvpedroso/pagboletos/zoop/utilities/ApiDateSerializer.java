package com.rvpedroso.pagboletos.zoop.utilities;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.rvpedroso.pagboletos.zoop.api.request.APIDateRequest;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;

/**
 *
 * @author william-fernandes
 */
class ApiDateSerializer implements JsonSerializer<APIDateRequest> {

    /**
     *
     * @param src
     * @param typeOfSrc
     * @param context
     * @return
     */
    @Override
    public JsonElement serialize(APIDateRequest src, Type typeOfSrc, JsonSerializationContext context) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return new JsonPrimitive(format.format(src.getDate()));
    }
}
