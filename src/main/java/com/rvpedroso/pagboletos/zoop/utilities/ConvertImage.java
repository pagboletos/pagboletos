package com.rvpedroso.pagboletos.zoop.utilities;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

/**
 * Usuário: william
 * Data: 08/04/19
 * Horário: 20
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.zoop.utilities
 **/
public class ConvertImage {


    public static File convert(MultipartFile file) throws IOException {
        File convFile = new File(Objects.requireNonNull(file.getOriginalFilename()));
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }
}
