package com.rvpedroso.pagboletos.zoop.api.request;

import com.google.gson.JsonSyntaxException;
import com.rvpedroso.pagboletos.zoop.PagBoletos;
import com.rvpedroso.pagboletos.zoop.api.response.Response;
import com.rvpedroso.pagboletos.zoop.auth.Authentication;
import com.rvpedroso.pagboletos.zoop.exception.*;
import com.rvpedroso.pagboletos.zoop.models.Setup;
import com.rvpedroso.pagboletos.zoop.utilities.ssl.SSLSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author william-fernandes
 */
public class RequestMaker extends PagBoletos {

    private final String PagBoletosEnvironment;
    private final Authentication authentication;
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestMaker.class);
    private final RequestTools tools;
    private final Response response;

    /**
     * Esse construtor configura o ambiente do PagBoletos e a autenticação       *
     * recebido do parâmetro.
     *
     * @param setup {@code Setup} the setup object.
     */
    public RequestMaker(Setup setup) {

        this.PagBoletosEnvironment = setup.getEnvironment();
        this.authentication = setup.getAuthentication();
        this.tools = new RequestTools();
        this.response = new Response();
    }

    /**
     * This method is used to build the request, it set the environment (Sandbox
     * or Production) and the headers, create the {@code connection} object to
     * establish connection with PagBoletos APIResources, authenticate the
     * connection, load the request properties received from parameter,
     * serialize the request object and send it to the API. Finally, this method
     * receive the response JSON and deserialize it into the respective model
     * object, sending the response code and response body to
     * {@code responseBodyTreatment} method to treat the response.
     *
     * @param requestProps {@code RequestProperties} the object containing the
     *                     properties of request, its like request method, endpoint, object, type,
     *                     content type and if it accepts another JSON version.
     * @return {@code Map<String, Object>}
     */
    public Map<String, Object> doRequest(final RequestProperties requestProps) {

        try {

            URL url = new URL(PagBoletosEnvironment + requestProps.endpoint);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", getUserAgent());
            connection.setRequestProperty("Content-type", requestProps.getContentType().getMimeType());

            if (requestProps.hasAccept()) {
                connection.setRequestProperty("Accept", requestProps.accept);
            }

            connection.setRequestMethod(requestProps.method);

            // This validation disable the TLS 1.0
            if (connection instanceof HttpsURLConnection) {
                ((HttpsURLConnection) connection).setSSLSocketFactory(new SSLSupport());
            }

            if (this.authentication != null) {
                authentication.authenticate(connection);
            }

            LOGGER.debug("---> {} {}", requestProps.method, connection.getURL().toString());
            logHeaders(connection.getRequestProperties().entrySet());

            if (requestProps.body != null) {
                connection.setDoOutput(true);
                String body = tools.getBody(requestProps.body, requestProps.contentType);

                LOGGER.debug("{}", body);

                try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
                    try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr, "UTF-8"))) {
                        writer.write(body);
                    }
                    wr.flush();
                }
            }

            LOGGER.debug("---> END HTTP");

            int responseCode = connection.getResponseCode();

            LOGGER.debug("<--- {} {}", responseCode, connection.getResponseMessage());
            logHeaders(connection.getHeaderFields().entrySet());

            StringBuilder responseBody = new StringBuilder();

            responseBody = responseBodyTreatment(responseBody, responseCode, connection);

            LOGGER.debug("{}", responseBody.toString());
            LOGGER.debug("<-- END HTTP ({}-byte body)", connection.getContentLength());

            // Return the parsed response from JSON to Map.
            return this.response.jsonToMap(responseBody.toString());

        } catch (IOException | KeyManagementException | NoSuchAlgorithmException e) {
            throw new PagBoletosAPIException("Error occurred connecting to PagBoletos API: " + e.getMessage(), e);
        }
    }

    /**
     * @param requestProps
     * @return
     */
    public List<Map<String, Object>> getList(final RequestProperties requestProps) {

        try {

            URL url = new URL(PagBoletosEnvironment + requestProps.endpoint);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", getUserAgent());
            connection.setRequestProperty("Content-type", requestProps.getContentType().getMimeType());

            if (requestProps.hasAccept()) {
                connection.setRequestProperty("Accept", requestProps.accept);
            }

            connection.setRequestMethod(requestProps.method);

            // This validation disable the TLS 1.0
            if (connection instanceof HttpsURLConnection) {
                ((HttpsURLConnection) connection).setSSLSocketFactory(new SSLSupport());
            }

            if (this.authentication != null) {
                authentication.authenticate(connection);
            }

            LOGGER.debug("---> {} {}", requestProps.method, connection.getURL().toString());
            logHeaders(connection.getRequestProperties().entrySet());

            if (requestProps.body != null) {
                connection.setDoOutput(true);
                String body = tools.getBody(requestProps.body, requestProps.contentType);

                LOGGER.debug("{}", body);

                try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
                    try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr, "UTF-8"))) {
                        writer.write(body);
                    }
                    wr.flush();
                }
            }

            LOGGER.debug("---> END HTTP");

            int responseCode = connection.getResponseCode();

            LOGGER.debug("<--- {} {}", responseCode, connection.getResponseMessage());
            logHeaders(connection.getHeaderFields().entrySet());

            StringBuilder responseBody = new StringBuilder();

            responseBody = responseBodyTreatment(responseBody, responseCode, connection);

            LOGGER.debug("{}", responseBody.toString());
            LOGGER.debug("<-- END HTTP ({}-byte body)", connection.getContentLength());

            // Return the parsed response from JSON to Map.
            return this.response.jsonToList(responseBody.toString());

        } catch (IOException | KeyManagementException | NoSuchAlgorithmException e) {
            throw new PagBoletosAPIException("Error occurred connecting to PagBoletos API: " + e.getMessage(), e);
        }
    }

    /**
     * This method is used to check the response code and apply the correct
     * treatment to the response body. Basically, if the response code is
     * between 200 and 299, this method returns a response body charged with the
     * right input stream. If the response code is 401, this method throws a
     * {@code UnauthorizedException}. If the code is between 400 and 499 (except
     * 401), this method create and charge an {@code Error} object with a
     * deserialize JSON returned from PagBoletos API to build a
     * {@code ValidationException} and throw it. And finally, if the response
     * code is 500 or higher, this method throws a {@code UnexpectedException}.
     *
     * @param responseBody {@code StringBuilder} the response body returned from
     *                     API.
     * @param responseCode {@code int} the response code returned from API.
     * @param connection   {@code HttpURLConnection} the object containing the
     *                     connection with PagBoletos API.
     * @return {@code StringBuilder} response body.
     */
    private StringBuilder responseBodyTreatment(StringBuilder responseBody, int responseCode, HttpURLConnection connection) {

        StringBuilder responseBodyTemp = responseBody;

        try {

            if (responseCode >= 200 && responseCode < 299) {
                responseBodyTemp = tools.readBody(connection.getInputStream());
            }

            if (responseCode == 401) {
                throw new UnauthorizedException();
            }

            if (responseCode >= 400 && responseCode < 499) {
                responseBodyTemp = tools.readBody(connection.getErrorStream());
                LOGGER.debug("API ERROR {}", responseBodyTemp.toString());

                ErrorZoop error = new ErrorZoop();
                String mensagem = null;
                try {

                    error = tools.getGsonInstance().fromJson(responseBodyTemp.toString(), ErrorZoop.class);
                    mensagem = errorZoop(error.getError().getCategory());

                } catch (JsonSyntaxException e) {
                    LOGGER.debug("There was not possible cast the JSON to object");
                }

                throw new ValidationException(responseCode, mensagem, error);
            }

            if (responseCode >= 500) {
                throw new UnexpectedException();
            }

        } catch (IOException e) {
            throw new PagBoletosAPIException("Error occurred connecting to PagBoletos API: " + e.getMessage(), e);
        }

        return responseBodyTemp;
    }


    private String errorZoop(String categoria) {

        String retorno = "";
        switch (categoria) {
            case "resource_not_found":
                retorno = "Não Encontrado";
                break;
            case "server_api_error":
                retorno = "Ocorreu um erro de processamento na PagBoletos, entre em contato com contato@pagboletos.com.br";
                break;
            case "duplicate_taxpayer_id":
                retorno = "O cliente com este já existe";
                break;
            case "service_request_timeout":
                retorno = "O processo do cartão de crédito está temporariamente indisponível";
                break;
            case "endpoint_not_found":
                retorno = "A URL solicitada não foi encontrada no servidor";
                break;
            case "authentication_failed":
                retorno = "As credenciais do usuário fornecidas falharam na validação do serviço solicitado.";
                break;
            case "expired_security_key":
                retorno = "A chave da API fornecida expirou ou foi excluída.";
                break;
            case "invalid_key_for_api_call":
                retorno = "Essa chamada de API não pode ser feita com uma chave de API publicável.";
                break;
            case "transaction_amount_error":
                retorno = "O valor mínimo é de US $ 0,50 (ou equivalente na moeda do país)";
                break;
            case "transfer_amount_error":
                retorno = "O valor mínimo de transferência é de US $ 1,00 (ou equivalente na moeda do país).";
                break;
            case "missing_required_param":
                retorno = "Parâmetro (s) requerido (s) ausente (s). Por favor, verifique os parâmetros da solicitação.";
                break;
            case "unsupported_payment_type":
                retorno = "Pedido inválido: Tipo de pagamento não suportado.";
                break;
            case "invalid_payment_information":
                retorno = "Informações de pagamento inválidas";
                break;
            case "invalid_parameter":
                retorno = "Parâmetros inválidos";
                break;
            case "file_size_too_large":
                retorno = "Tamanho do arquivo é muito grande";
                break;
            case "insufficient_escrow_funds_error":
                retorno = "A transferência solicitada excede os fundos liquidados restantes em depósito.";
                break;
            case "capture_transaction_error":
                retorno = "A solicitação de captura falhou. A transação não pôde ser capturada.";
                break;
            case "no_action_taken":
                retorno = "Nenhuma ação tomada. Não é possível fazer o backup da transação anterior";
                break;
            case "seller_authorization_refused":
                retorno = "Recusado: Você não completou seu cadastro!";
                break;
            case "void_transaction_error":
                retorno = "A transação não pode ser nula.";
                break;
            case "invalid_expiry_month":
                retorno = "Cartão expirado";
                break;
            case "invalid_expiry_year":
                retorno = "Ano expirado";
                break;
            case "card_customer_not_associated":
                retorno = "Nenhum cartão ativo.";
                break;
            case "insufficient_funds_error":
                retorno = "O crédito requerido excede os fundos liquidados restantes.";
                break;
            case "expired_card_error":
                retorno = "O cartão de crédito expirou.";
                break;
            case "invalid_card_number":
                retorno = "O número do cartão não está válido";
                break;
            case "invalid_pin_code":
                retorno = "CVC inválido.";
                break;
            case "authorization_refused":
                retorno = "Autorização Recusada";
                break;
            default:
                retorno = "Ocorrou um erro";
                break;
        }
        return retorno;
    }


    /**
     * This method is used to populate an {@code Map.Entry} with passed keys and
     * values to charge the debug logger.
     *
     * @param entries {@code Map.Entry<String, List<String>>}
     */
    private void logHeaders(Set<Map.Entry<String, List<String>>> entries) {
        entries.stream().filter((header) -> (header.getKey() != null)).forEachOrdered((header) -> {
            LOGGER.debug("{}: {}", header.getKey(), header.getValue());
        });
    }
}
