package com.rvpedroso.pagboletos.zoop.api.request;

import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 *
 * @author william-fernandes
 */
public class RequestProperties {

    /**
     *
     */
    protected String method;

    /**
     *
     */
    protected String endpoint;

    /**
     *
     */
    protected Map<String, Object> body;

    /**
     *
     */
    protected Class type;

    /**
     *
     */
    protected ContentType contentType;

    /**
     *
     */
    protected String accept;

    /**
     *
     * @return
     */
    public String getMethod() {
        return method;
    }

    /**
     *
     * @return
     */
    public String getEndpoint() {
        return endpoint;
    }

    /**
     *
     * @return
     */
    public Object getObject() {
        return body;
    }

    /**
     *
     * @param <T>
     * @return
     */
    public <T> Class<T> getType() {
        return type;
    }

    /**
     *
     * @return
     */
    public ContentType getContentType() {
        return contentType;
    }

    /**
     *
     * @return
     */
    public String getAccept() {
        return accept;
    }

    /**
     * This method is used to verify if the {@code Accept} header was settled
     * into properties.
     *
     * @return {@code boolean}
     */
    public boolean hasAccept() {

        return this.accept != null;
    }

}
