package com.rvpedroso.pagboletos.zoop.api;

import com.rvpedroso.pagboletos.zoop.models.*;

/**
 *
 * @author william-fernandes
 */
public class APIResources {

    private static final ApiKeys API_KEY_INSTANCE = new ApiKeys();
    private static final Balance BALANCE_INSTANCE = new Balance();
    private static final BankAccount BANK_ACCOUNT_INSTANCE = new BankAccount();
    private static final Boleto BOLETO_INSTANCE = new Boleto();
    private static final Buyer BUYER_INSTANCE = new Buyer();
    private static final Cards CARDS_INSTANCE = new Cards();
    private static final Documents DOCUMENTS_INSTANCE = new Documents();
    private static final Events EVENTS_INSTANCE = new Events();
    private static final Invoices INVOICES_INSTANCE = new Invoices();
    private static final Marketplace MARKETPLACE_INSTANCE = new Marketplace();
    private static final MCCs MCCS_INSTANCE = new MCCs();
    private static final Permission PERMISSION_INSTANCE = new Permission();
    private static final Plans PLANS_INSTANCE = new Plans();
    private static final Receipt RECEIPT_INSTANCE = new Receipt();
    private static final Receivables RECEIVABLES_INSTANCE = new Receivables();
    private static final ReceivingPolicy RECEIVING_POLICY_INSTANCE = new ReceivingPolicy();
    private static final Seller SELLER_INSTANCE = new Seller();
    private static final Source SOURCE_INSTANCE = new Source();
    private static final SplitRules SPLIT_RULES_INSTANCE = new SplitRules();
    private static final Subscriptions SUBSCRIPTIONS_INSTANCE = new Subscriptions();
    private static final Token TOKEN_INSTANCE = new Token();
    private static final Transaction TRANSACTION_INSTANCE = new Transaction();
    private static final Transfers TRANSFERS_INSTANCE = new Transfers();
    private static final User USER_INSTANCE = new User();
    private static final Webhook WEBHOOKS_INSTANCE = new Webhook();

    public static Seller sellers() {
        return SELLER_INSTANCE;
    }

    public static User customers() {
        return USER_INSTANCE;
    }

    public static ApiKeys apiKeys() {
        return API_KEY_INSTANCE;
    }

    public static Balance balance() {
        return BALANCE_INSTANCE;
    }

    public static BankAccount bankAccount() {
        return BANK_ACCOUNT_INSTANCE;
    }

    public static Boleto boleto() {
        return BOLETO_INSTANCE;
    }

    public static Buyer byer() {
        return BUYER_INSTANCE;
    }

    public static Cards cards() {
        return CARDS_INSTANCE;
    }

    public static Documents documents() {
        return DOCUMENTS_INSTANCE;
    }

    public static Events events() {
        return EVENTS_INSTANCE;
    }

    public static Invoices invoices() {
        return INVOICES_INSTANCE;
    }

    public static MCCs mccs() {
        return MCCS_INSTANCE;
    }

    public static Marketplace marketplace() {
        return MARKETPLACE_INSTANCE;
    }

    public static Permission permission() {
        return PERMISSION_INSTANCE;
    }

    public static Plans plans() {
        return PLANS_INSTANCE;
    }

    public static Receipt receipe() {
        return RECEIPT_INSTANCE;
    }

    public static Receivables receivables() {
        return RECEIVABLES_INSTANCE;
    }

    public static ReceivingPolicy receivingPolicy() {
        return RECEIVING_POLICY_INSTANCE;
    }

    public static Source source() {
        return SOURCE_INSTANCE;
    }

    public static SplitRules splitRules() {
        return SPLIT_RULES_INSTANCE;
    }

    public static Subscriptions subscriptions() {
        return SUBSCRIPTIONS_INSTANCE;
    }

    public static Token token() {
        return TOKEN_INSTANCE;
    }

    public static Transaction transaction() {
        return TRANSACTION_INSTANCE;
    }

    public static Transfers transfers() {
        return TRANSFERS_INSTANCE;
    }

    public static User user() {
        return USER_INSTANCE;
    }

    public static Webhook webhook() {
        return WEBHOOKS_INSTANCE;
    }

}
