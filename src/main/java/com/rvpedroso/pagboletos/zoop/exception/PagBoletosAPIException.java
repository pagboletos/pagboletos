package com.rvpedroso.pagboletos.zoop.exception;

/**
 *
 * @author william-fernandes
 */
public class PagBoletosAPIException extends RuntimeException {

    /**
     * Default class constructor.
     */
    public PagBoletosAPIException() {
    }

    /**
     * Constructor to receive the exception message.
     *
     * @param message {@code String} exception message
     */
    public PagBoletosAPIException(final String message) {
        super(message);
    }

    /**
     * Constructor to receive the exception cause.
     *
     * @param cause {@code Throwable} exception cause.
     */
    public PagBoletosAPIException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructor to receive the exception message and cause.
     *
     * @param message {@code String} exception message
     *
     * @param cause {@code Throwable} exception cause
     */
    public PagBoletosAPIException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor to receive the message and the cause of an exception and to
     * able the suppression and the writable stack trace.
     *
     * @param message {@code String} exception message
     *
     * @param cause {@code Throwable} exception cause
     *
     * @param enableSuppression {@code boolean} exception suppression
     *
     * @param writableStackTrace {@code boolean} exception stack trace
     */
    public PagBoletosAPIException(final String message, final Throwable cause,
            final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
