package com.rvpedroso.pagboletos.zoop.exception;

/**
 * Usuário: william
 * Data: 11/04/19
 * Horário: 21
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.zoop.exception
 **/
public class Error {
    public String status;
    public Integer statusCode;
    public String type;
    public String category;
    public String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
