package com.rvpedroso.pagboletos.zoop.exception;

import com.rvpedroso.pagboletos.web.rest.errors.ErrorConstants;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

/**
 * Simple exception with a message, that returns an Internal Server Error code.
 */
public class PagBoletosErrorException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    public PagBoletosErrorException(PagBoletosAPIException e) {
        super(ErrorConstants.DEFAULT_TYPE, e.getMessage(), Status.INTERNAL_SERVER_ERROR);
    }
}
