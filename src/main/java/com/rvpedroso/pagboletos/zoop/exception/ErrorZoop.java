package com.rvpedroso.pagboletos.zoop.exception;

/**
 * Usuário: william
 * Data: 11/04/19
 * Horário: 21
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.zoop.exception
 **/
public class ErrorZoop {

    public Error error;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }
}
