package com.rvpedroso.pagboletos.zoop.exception;

/**
 *
 * @author william-fernandes
 */
public class ValidationException extends PagBoletosAPIException {

    private final int responseCode;
    private final String responseStatus;
    private final ErrorZoop errors;

    /**
     * This constructor is used to set the code, status and errors from api
     * response, when it trows a ValidationException.
     *
     * @param responseCode {@code int} api response code
     *
     * @param responseStatus {@code String} api response status
     *
     * @param errors {@code Errors} api errors
     */
    public ValidationException(final int responseCode, final String responseStatus, final ErrorZoop errors) {
        this.responseCode = responseCode;
        this.responseStatus = responseStatus;
        this.errors = errors;
    }

    /**
     *
     * @return
     */
    public int getResponseCode() {
        return responseCode;
    }

    /**
     *
     * @return
     */
    public String getResponseStatus() {
        return responseStatus;
    }

    /**
     *
     * @return
     */
    public ErrorZoop getErrors() {
        return errors;
    }
}
