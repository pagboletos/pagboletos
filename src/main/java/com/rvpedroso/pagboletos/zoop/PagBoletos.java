package com.rvpedroso.pagboletos.zoop;

import com.rvpedroso.pagboletos.zoop.api.APIResources;
import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;

import java.io.InputStream;
import java.util.Properties;

/**
 * @author william-fernandes
 */
public class PagBoletos {

    /**
     * Chava Zoop
     */
    public static final String API_KEY = "zpk_prod_gvT7Hlyra9kIXchQdtRcreHF";

    /**
     * Chave Zoop criptografada
     */
    public static final String API_KEY_BASIC = "Basic enBrX3Byb2RfZ3ZUN0hseXJhOWtJWGNoUWR0UmNyZUhGOg==";

    /**
     *
     */
    public static final String BASE_V1 = "https://api.zoop.ws/v1";


    /**
     *
     */
    public static final String BASE_V1_BETA = "https://api-beta.zoop.ws/v1";

    /**
     *
     */
    public static final String BASE_V2 = "https://api.zoop.ws/v2";

    /**
     *
     */
    public static final String PRODUCTION_V1 = BASE_V1.concat("/marketplaces/271fc31ff48d4f8793168a9bb688f0c9");

    /**
     *
     */
    public static final String PRODUCTION_BETA_V1 = BASE_V1_BETA.concat("/marketplaces/271fc31ff48d4f8793168a9bb688f0c9");


    /**
     *
     */
    public static final String PRODUCTION_V2 = BASE_V2.concat("/marketplaces/271fc31ff48d4f8793168a9bb688f0c9");

    /**
     *
     */
    public static final String AUTH_URL = BASE_V1.concat("/users/signin");

    private static String USER_AGENT;

    static {
        try {
            InputStream inputStream = RequestMaker.class.getResourceAsStream("/PagBoletosJavaSDK.properties");
            Properties properties = new Properties();
            properties.load(inputStream);

            USER_AGENT = properties.getProperty("userAgent");
        } catch (Exception e) {
            USER_AGENT = "PagBoletosJavaSDK/UnknownVersion (+https://github.com/PagBoletos/PagBoletos-sdk-java/)";
        }
    }

    /**
     * This method returns the {@code USER_AGENT} value.
     *
     * @return {@code String}
     */
    protected String getUserAgent() {
        return USER_AGENT;
    }

    /**
     * Classe API
     */
    public static class API extends APIResources {
    }
}
