package com.rvpedroso.pagboletos.zoop.models.error;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author william-fernandes
 */
public class Errors {

    private final List<Error> errors = new ArrayList<>();

    /**
     *
     * @return
     */
    public List<Error> getErrors() {
        return errors;
    }

    /**
     * This method adds a PagBoletos error object to the errors list.
     *
     * @param error {@code br.com.rvpedroso.pagboletos.sdk.java.models.error.Error} object
     */
    public void setError(ErrorBuilder error) {
        this.errors.add(error);
    }
}
