package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 *
 * @author william-fernandes
 */
public class Receivables {

    private static final String RECEIVABLES = "receivables";
    private static final String TRANSACTIONS = "transactions";
    private static final String SELLERS = "sellers";

    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private RequestMaker requestMaker;

    /**
     * Recuperar detalhes de recebível
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /receivables/receivable_id
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param receivablesId
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> get(String receivablesId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s", RECEIVABLES, receivablesId))
                .type(Receivables.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Listar recebíveis por transação Listar todas as parcelas de recebimento
     * por transação
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /transactions/transaction_id/receivables
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param receivablesId
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> getTransactions(String receivablesId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s/%s", TRANSACTIONS, receivablesId, RECEIVABLES))
                .type(Receivables.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }
    
    /**
     * Listar recebíveis por seller
     * Listar todas as parcelas de recebimento por seller
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /sellers/seller_id/receivables
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param sellerId
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> getSeller(String sellerId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s/%s", SELLERS, sellerId, RECEIVABLES))
                .type(Receivables.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

}
