package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 *
 * @author william-fernandes
 */
public class Cards {

    private static final String CARDS = "/cards";

    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private RequestMaker requestMaker;

    /**
     * Associar cartão com comprador Associar token de cartão com comprador
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id /cards
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param body
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> associate(Map<String, Object> body, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(CARDS)
                .body(body)
                .type(Cards.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }
    
    
    /**
     * Recuperar detalhes de cartão pelo identificador
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /cards/card_id
     * 
     * @param cardId {@code String} user_id do usuário PagBoletos
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> get(String cardId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("%s/%s", CARDS, cardId))
                .type(Cards.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }
    
    /**
     * Remover cartão pelo identificador
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /cards/card_id
     *
     * @param id {@code String}
     * @param setup {@code Setup}
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> delete(String id, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("DELETE")
                .endpoint(String.format("%s/%s", CARDS, id))
                .type(Cards.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }
    
    /**
     * Atualizar detalhes de cartão pelo identificador
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /cards/card_id
     * 
     * @param id {@code String}
     * @param body
     * @param setup {@code Setup}
     * 
     * @return {@code Map<String, Object}
     */
    public Map<String, Object> update(Map<String, Object> body, Setup setup, String id) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("PUT")
                .endpoint(String.format("%s/%s", CARDS, id))
                .body(body)
                .type(Cards.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);  
    }


}
