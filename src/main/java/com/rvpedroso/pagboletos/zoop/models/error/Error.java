package com.rvpedroso.pagboletos.zoop.models.error;

/**
 *
 * @author william-fernandes
 */
class Error {

    private String code;
    private String path;
    private String description;

    public String getCode() {
        return code;
    }

    public String getPath() {
        return path;
    }

    public String getDescription() {
        return description;
    }

    /**
     * This method receive the PagBoletos error identifier code from ErrorBuilder
     * class.
     *
     * @param code {@code String} PagBoletos error code
     */
    protected void setCode(String code) {
        this.code = code;
    }

    /**
     * This method receive the PagBoletos error path from ErrorBuilder class.
     *
     * @param path {@code String} PagBoletos error path
     */
    protected void setPath(String path) {
        this.path = path;
    }

    /**
     * This method receive the PagBoletos error description from ErrorBuilder
     * class.
     *
     * @param description {@code String} PagBoletos error description
     */
    protected void setDescription(String description) {
        this.description = description;
    }
}
