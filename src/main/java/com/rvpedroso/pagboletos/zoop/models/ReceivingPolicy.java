package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 *
 * @author william-fernandes
 */
public class ReceivingPolicy {

    private static final String RECEIVING_POLICY = "receiving_policy";
    private static final String SELLERS = "sellers";

    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private RequestMaker requestMaker;

    /**
     * Recuperar política de recebimento por seller
     *
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /sellers/seller_id/receiving_policy
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param sellerId
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> get(String sellerId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s/%s", SELLERS, sellerId, RECEIVING_POLICY))
                .type(ReceivingPolicy.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Alterar política de recebimento por seller
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /sellers/seller_id/receiving_policy
     *
     * @param body {@code Setup} {"customer": "string","token": "string"
     * @param setup {@code Setup} objeto de configuração.
     * @param sellerId
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> update(String sellerId, Setup setup, Map<String, Object> body) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s/%s/%s", SELLERS, sellerId, RECEIVING_POLICY))
                .body(body)
                .type(ReceivingPolicy.class)
                .contentType(CONTENT_TYPE)
                .build();

        return requestMaker.doRequest(props);
    }

}
