package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.auth.Authentication;

import static com.rvpedroso.pagboletos.zoop.PagBoletos.*;

/**
 * @author william-fernandes
 */
public class Setup {

    private String environment;

    // Default connect timeout (in milliseconds) with default value.
    private static int CONNECT_TIMEOUT = 60 * 1000;

    // Read timeout (in milliseconds) with default value.
    private static int READ_TIMEOUT = 80 * 1000;
    private Authentication authentication;

    /**
     * This method is used to set the authentication that will be used to
     * authorize the request. The authentication may be an BasicAuth
     *
     * @param authentication {@code Authentication} to connect with PagBoletos
     *                       APIResources.
     * @return {@code this} (Setup)
     */
    public Setup setAuthentication(final Authentication authentication) {
        this.authentication = authentication;

        return this;
    }

    /**
     * This method is used to set the PagBoletos API environment where the requests
     * will be sent. The only PagBoletos environments that are possible request are
     * {@code SANDBOX} or {@code PRODUCTION}.
     *
     * @param environment {@code String} the PagBoletos API environment.
     * @return {@code Setup}
     */
    public Setup setEnvironment(final Environment environment) {

        switch (environment) {

            case BASE_V1:
                this.environment = BASE_V1;
                break;

            case API_KEY_BASIC:
                this.environment = API_KEY_BASIC;
                break;

            case BASE_V2:
                this.environment = BASE_V2;
                break;

            case PRODUCTION_V1:
                this.environment = PRODUCTION_V1;
                break;

            case PRODUCTION_BETA_V1:
                this.environment = PRODUCTION_BETA_V1;
                break;

            case PRODUCTION_V2:
                this.environment = PRODUCTION_V2;
                break;

            case AUTH_URL:
                this.environment = AUTH_URL;
                break;

            default:
                this.environment = "";
                break;
        }

        return this;
    }

    /**
     * This method is used to set the {@code Player} endpoint for <strong>mock
     * unit tests</strong>.
     *
     * @param endpoint {@code String} the endpoint for mock tests.
     * @return {@code this} (Setup)
     */
    public Setup setPlayerEndpoint(final String endpoint) {
        this.environment = endpoint;

        return this;
    }

    /**
     * Use this method will change the connect timeout default value. It will be
     * used to request the PagBoletos APIResources.
     *
     * @param connectTimeout {@code int} timeout in milliseconds.
     * @return {@code this} (Setup)
     */
    public Setup setConnectTimeout(final int connectTimeout) {
        CONNECT_TIMEOUT = connectTimeout;

        return this;
    }

    /**
     * Use this method will change the read timeout default value. It will be
     * used to request the PagBoletos APIResources.
     *
     * @param readTimeout {@code int} timeout in millisecond.
     * @return {@code this} (Setup)
     */
    public Setup setReadTimeout(final int readTimeout) {
        READ_TIMEOUT = readTimeout;

        return this;
    }

    /**
     * This method is used to get the value of {@code authentication} attribute.
     *
     * @return {@code Authentication}
     */
    public Authentication getAuthentication() {
        return this.authentication;
    }

    /**
     * This method is used to get the value of {@code environment} attribute.
     *
     * @return {@code String}
     */
    public String getEnvironment() {
        return this.environment;
    }

    /**
     * This method is used to get the value of {@code CONNECT_TIMEOUT}
     * attribute.
     *
     * @return {@code int}
     */
    public static int getConnectTimeout() {
        return CONNECT_TIMEOUT;
    }

    /**
     * This method is used to get the value of {@code READ_TIMEOUT} attribute.
     *
     * @return {@code int}
     */
    public static int getReadTimeout() {
        return READ_TIMEOUT;
    }

    /**
     * These enums are used to difference all PagBoletos API environments.
     */
    public enum Environment {

        /**
         *
         */
        BASE_V1,


        /**
         *
         */
        API_KEY_BASIC,

        /**
         *
         */
        BASE_V2,

        /**
         *
         */
        PRODUCTION_V1,

        /**
         *
         */
        PRODUCTION_BETA_V1,


        /**
         *
         */
        PRODUCTION_V2,

        /**
         *
         */
        AUTH_URL
    }
}
