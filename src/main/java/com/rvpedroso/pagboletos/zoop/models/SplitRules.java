package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 *
 * @author william-fernandes
 */
public class SplitRules {

    private static final String SPLIT_RULES = "split_rules";
    private static final String TRANSACTION = "transactions";

    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private RequestMaker requestMaker;

    /**
     * Recuperar detalhes de regra de divisão por transação
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /transactions/transaction_id/split_rules
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param id
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> get(String id, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s/%s", TRANSACTION, id, SPLIT_RULES))
                .type(SplitRules.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Criar regra de divisão por transação
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /transactions/transaction_id/split_rules
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param transactionId
     * @param body
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> create(Map<String, Object> body, 
            String transactionId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s/%s/%s", TRANSACTION, transactionId, SPLIT_RULES))
                .body(body)
                .type(SplitRules.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Recupera detalhes de regra de divisão por transação
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /transactions/transaction_id/split_rules/id
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param transactionId
     * @param id
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> details(String transactionId, String id, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s/%s/%s",
                        TRANSACTION, transactionId, SPLIT_RULES, id))
                .type(SplitRules.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Alterar regra de divisão por transação
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /transactions/transaction_id/split_rules/id
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param transactionId
     * @param id
     * @param body
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> update(Map<String, Object> body,
            String transactionId, String id, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s/%s/%s", TRANSACTION, transactionId,
                        SPLIT_RULES, id))
                .body(body)
                .type(SplitRules.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Remover regra de divisão por transação
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /transactions/transaction_id/split_rules/id
     *
     * @param transactionId {@code String}
     * @param id {@code String}
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> delete(String transactionId,String id, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("DELETE")
                .endpoint(String.format("/%s/%s/%s/%s", TRANSACTION, 
                        transactionId,SPLIT_RULES, id))
                .type(SplitRules.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

}
