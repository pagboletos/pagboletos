package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 *
 * @author william-fernandes
 */
public class Balance {

    private static final String BALENCES = "balances";
    private static final String SELLERS = "sellers";
    private static final String HISTORY = "history";
    private static final String ALL = "all";
    private static final String BUYERS = "buyers";

    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private RequestMaker requestMaker;

    /**
     * Recuperar saldo de conta por seller Recupera o saldo corrente e saldo
     * total de conta
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/
     * {marketplace_id}/sellers/{seller_id}/balances
     *
     * @param sellerId {@code String}
     * @param setup {@code Setup}
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> get(String sellerId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s/%s", SELLERS, sellerId, BALENCES))
                .type(Balance.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Lista contas por seller
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/
     * marketplace_id/sellers/{seller_id}/balances/all
     *
     * @param sellerId {@code String}
     * @param setup {@code Setup}.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> listBalanceSeller(String sellerId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s/%s/%s", SELLERS, sellerId, BALENCES, ALL))
                .type(Balance.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Lista contas por buyer
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/
     * marketplace_id/buyers/buyer_id/balances
     *
     * @param buyerId {@code String}
     * @param setup {@code Setup}
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> listBalanceBuyer(String buyerId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s/%s", BUYERS, buyerId, BALENCES))
                .type(Balance.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Listar histórico de lançamentos pelo identificador da conta
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces
     * /marketplace_id/balances/balance_id/history
     *
     * @param balanceId {@code String}
     * @param setup {@code Setup}
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> listHistory(String balanceId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s/%s", BALENCES, balanceId, HISTORY))
                .type(Balance.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Listar histórico de lançamentos de conta por seller Listagem de histórico
     * da conta principal do seller
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces
     * /marketplace_id/sellers/seller_id/balances/history
     *
     * @param sellerId {@code String}
     * @param setup {@code Setup}
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> listHistorySeller(String sellerId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s/%s/%s", SELLERS, sellerId,
                        BALENCES, HISTORY))
                .type(Balance.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Listar histórico de lançamentos de conta por buyer Listagem de histórico
     * da conta principal do buyer
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces
     * /marketplace_id/buyers/buyer_id/balances/history
     *
     * @param buyerId {@code String}
     * @param setup {@code Setup}
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> listHistoryBuyer(String buyerId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s/%s/%s", BUYERS, buyerId,
                        BALENCES, HISTORY))
                .type(Balance.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

}
