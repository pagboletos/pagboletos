package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 *
 * @author william-fernandes
 */
public class Invoices {

    private static final String INVOICES = "invoices";
    private static final String APPROVE = "approve";
    private static final String VOID = "void";
    private static final String SELLERS = "sellers";

    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private RequestMaker requestMaker;

    /**
     * Recuperar todas as faturas de um marketplace
     *
     *
     * @link https://api.zoop.ws/v2/marketplaces/marketplace_id/invoices
     *
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> list(Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s", INVOICES))
                .type(Invoices.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Criar uma fatura avulsa
     *
     *
     *
     * @link https://api.zoop.ws/v2/marketplaces/marketplace_id/invoices
     *
     * @param body
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> create(Map<String, Object> body,
            Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s", INVOICES))
                .body(body)
                .type(Invoices.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Recuperar os detalhes de uma fatura pelo identificador - Você pode
     * recuperar os detalhes de uma determinada fatura através do identificador
     *
     *
     * @link https://api.zoop.ws/v2/marketplaces/marketplace_id
     * /invoices/invoice_id
     *
     * @param invoiceId
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> get(String invoiceId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s", INVOICES, invoiceId))
                .type(Invoices.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Alterar detalhes de uma fatura pelo identificador
     *
     *
     * @link https://api.zoop.ws/v2/marketplaces/marketplace_id
     * /invoices/invoice_id
     *
     * @param invoiceId
     * @param body
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> update(String invoiceId, Map<String, Object> body,
            Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("PUT")
                .endpoint(String.format("/%s/%s", INVOICES, invoiceId))
                .body(body)
                .type(Invoices.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Remover uma fatura pelo identificador
     *
     *
     * @link https://api.zoop.ws/v2/marketplaces/marketplace_id
     * /invoices/invoice_id
     *
     * @param invoiceId {@code String}
     * @param setup {@code Setup}
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> delete(String invoiceId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("DELETE")
                .endpoint(String.format("%s/%s", INVOICES, invoiceId))
                .type(Invoices.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Aprovar fatura pendente - Aprovar manualmente uma fatura pendente, sem
     * que seja feita a cobrança ao cobrador
     *
     *
     *
     * @link https://api.zoop.ws/v2/marketplaces/marketplace_id
     * /invoices/invoice_id/approve
     *
     * @param invoiceId
     * @param body
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> approveInvoicePending(String invoiceId,
            Map<String, Object> body,
            Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s/%s/%s", INVOICES, invoiceId, APPROVE))
                .body(body)
                .type(Invoices.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Estornar e reembolsar fatura - Estornar uma fatura paga, sendo feito o
     * processamento do estorno da forma de cobrança associada ao pagamento da
     * fatura
     *
     *
     *
     * @link https://api.zoop.ws/v2/marketplaces/marketplace_id
     * /invoices/invoice_id/void
     *
     * @param invoiceId
     * @param body
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> reverseInvoicePending(String invoiceId,
            Map<String, Object> body,
            Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s/%s/%s", INVOICES, invoiceId, VOID))
                .body(body)
                .type(Invoices.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Recuperar faturas associadas a um vendedor pelo identificador - Recuperar
     * os detalhes de todas as faturas associadas a um determinado vendedor pelo
     * identificador
     *
     *
     * @link https://api.zoop.ws/v2/marketplaces/marketplace_id
     * /sellers/customer_id/invoices
     *
     * @param customerId
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> getinvoicesSeller(String customerId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s/%s", SELLERS, customerId, INVOICES))
                .type(Invoices.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

}
