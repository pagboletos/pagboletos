package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 * @author william-fernandes
 */
public class Receipt {

    private static final String RECEIPTS = "receipts";
    private static final String EMAILS = "emails";
    private static final String TEXTS = "texts";
    private static final String SEND = "send";

    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private RequestMaker requestMaker;

    /**
     * Enviar recibo por email
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param id
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /receipts/receipt_id/emails
     */
    public Map<String, Object> sendEmail(String id, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
            .method("POST")
            .endpoint(String.format("/%s/%s/%s", RECEIPTS, id, EMAILS))
            .type(Receipt.class)
            .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Enviar recibo por SMS
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param id
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /receipts/receipt_id/texts
     */
    public Map<String, Object> sendSMS(String id, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
            .method("POST")
            .endpoint(String.format("/%s/%s/%s", RECEIPTS, id, TEXTS))
            .type(Receipt.class)
            .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Enviar recibo por sms.email
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param id
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /receipts/receipt_id/send
     */
    public Map<String, Object> sendSMSEmail(String id, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
            .method("POST")
            .endpoint(String.format("/%s/%s/%s", RECEIPTS, id, SEND))
            .type(Receipt.class)
            .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Recuperar detalhes do recibo
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param id
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /receipts/receipt_id
     */
    public Map<String, Object> get(String id, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
            .method("GET")
            .endpoint(String.format("/%s/%s", RECEIPTS, id))
            .type(Receipt.class)
            .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Alterar detalhes do recibo
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param id
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /receipts/receipt_id
     */
    public Map<String, Object> update(String id, Map<String, Object> body, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
            .method("PUT")
            .endpoint(String.format("/%s/%s", RECEIPTS, id))
            .body(body)
            .type(Seller.class)
            .contentType(CONTENT_TYPE);
        return this.requestMaker.doRequest(props);
    }

    /**
     * Renderizar template de recibo HTML
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param id
     * @return {@code Map<String, Object>}.
     * @link Ithttps://api.zoop.ws/v1/receipts/marketplace_id/receipt_id
     */
    public Map<String, Object> render(String id, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
            .method("GET")
            .endpoint(String.format("/%s", id))
            .type(Receipt.class)
            .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

}
