package com.rvpedroso.pagboletos.zoop.models;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rvpedroso.pagboletos.web.rest.errors.InternalServerErrorException;
import com.rvpedroso.pagboletos.web.rest.model.Paginate;
import com.rvpedroso.pagboletos.zoop.PagBoletos;
import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import com.rvpedroso.pagboletos.zoop.exception.PagBoletosAPIException;
import com.rvpedroso.pagboletos.zoop.utilities.ConvertImage;
import org.apache.http.HttpEntity;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author william-fernandes
 */
public class Documents {

    public static final String ENDPOINT = "sellers";
    public static final String DOCUMENTS = "documents";
    public static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    public static final ContentType CONTENT_TYPE_MULTIPART_FORM_DATA = ContentType.MULTIPART_FORM_DATA;
    private RequestMaker requestMaker;

    /**
     * Criar documento de cadastro de vendedor
     *
     * @param sellerId
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/{marketplace_id}/sellers/
     * {seller_id}/documents
     */
    public Map<String, Object> create(String sellerId, MultipartFile files, String tipo) {
        Map<String, Object> map = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> responseMap = null;

        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            FileBody bin = new FileBody(ConvertImage.convert(files));

            HttpEntity reqEntity = MultipartEntityBuilder.create()
                .addPart("file", bin)
                .build();

            HttpUriRequest request = RequestBuilder
                .post(String.format("%s/%s/%s/documents?category=%s",
                    PagBoletos.PRODUCTION_BETA_V1,
                    Documents.ENDPOINT,
                    sellerId,
                    tipo))
                .setHeader("Authorization", PagBoletos.API_KEY_BASIC)
                .setEntity(reqEntity)
                .build();

            ResponseHandler<String> responseHandler = response -> {
                int status = response.getStatusLine().getStatusCode();
                map.put("status", status);
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new PagBoletosAPIException("Unexpected response status: " + response.getStatusLine());
                }
            };
            String responseBody = httpclient.execute(request, responseHandler);
            responseMap = mapper.readValue(responseBody, new TypeReference<Map<String, Object>>() {
            });
        } catch (PagBoletosAPIException e) {
            throw new InternalServerErrorException(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return responseMap;
    }

    /**
     * Listar documentos de um vendedor
     *
     * @param sellerId
     * @param setup    {@code Setup} objeto de configuração.
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/{marketplace_id}/sellers/{seller_id}
     * /documents
     */
    public Map<String, Object> list(String sellerId, Setup setup, Paginate paginate) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
            .method("GET")
            .endpoint(String.format("/%s/%s/%s?%s", ENDPOINT, sellerId, DOCUMENTS, paginate.toString()))
            .type(Documents.class)
            .contentType(CONTENT_TYPE)
            .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Atualiza os dados de um documento de um vendedor
     *
     * @param idDocuments
     * @param body
     * @param setup       {@code Setup} objeto de configuração.
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/{marketplace_id}/sellers/documents/{id}
     */
    public Map<String, Object> update(String idDocuments, Map<String, Object> body,
                                      Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
            .method("PUT")
            .endpoint(String.format("/%s/%s/%s", ENDPOINT, DOCUMENTS, idDocuments))
            .body(body)
            .type(Documents.class)
            .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

}
