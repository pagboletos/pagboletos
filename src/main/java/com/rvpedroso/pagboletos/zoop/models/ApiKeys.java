package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 *
 * @author william-fernandes
 */
public class ApiKeys {

    private static final String API_KEYS = "api_keys";

    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private RequestMaker requestMaker;

    /**
     * Criar nova API Key por marketplace
     *
     * @param body
     * @param setup
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> create(Map<String, Object> body, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s", API_KEYS))
                .body(body)
                .type(ApiKeys.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Listar chaves de API por marketplace
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/api_keys
     *
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> list(Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s", API_KEYS))
                .type(ApiKeys.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Recuperar detalhes de um API Key
     *
     *
     * @link
     * https://api.zoop.ws/v1/marketplaces/marketplace_id/api_keys/apikey_id
     *
     * @param apikeyId {@code String} user_id do usuário PagBoletos
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> get(String apikeyId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("%s/%s", API_KEYS, apikeyId))
                .type(ApiKeys.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Remover cartão pelo identificador
     *
     *
     * @link
     * https://api.zoop.ws/v1/marketplaces/marketplace_id/api_keys/apikey_id
     *
     * @param apikeyId {@code String}
     * @param setup {@code Setup}
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> delete(String apikeyId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("DELETE")
                .endpoint(String.format("%s/%s", API_KEYS, apikeyId))
                .type(ApiKeys.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

}
