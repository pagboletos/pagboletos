package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 *
 * @author william-fernandes
 */
public class Transaction {

    private static final String TRANSACTIONS = "transactions";
    private static final String SELLERS = "sellers";
    private static final String VOID = "void";
    private static final String CAPTURE = "capture";

    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private RequestMaker requestMaker;

    /**
     * Listar transaçoes do marketplace
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id /transactions
     *
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> list(Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s", TRANSACTIONS))
                .type(Transaction.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Criar transação Cartão Não Presente
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id /transactions
     *
     * @param body
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> create(Map<String, Object> body,
            Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s", TRANSACTIONS))
                .body(body)
                .type(Transaction.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Listar documentos de um vendedor
     *
     *
     * @return 
     * @link ttps://api.zoop.ws/v1/marketplaces/marketplace_id
     * /sellers/seller_id/transactions}
     *
     * @param sellerId
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> listTransactionSeller(String sellerId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s", SELLERS, sellerId, TRANSACTIONS))
                .type(Transaction.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Recuperar detalhes de transação pelo identificador
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /transactions/transaction_id
     *
     * @param id
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> get(String id, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s", TRANSACTIONS, id))
                .type(Transaction.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Alterar detalhes de transação pelo identificador
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /transactions/transaction_id
     *
     * @param idTransaction
     * @param body
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> update(String idTransaction, Map<String, Object> body,
            Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("PUT")
                .endpoint(String.format("/%s/%s", TRANSACTIONS, idTransaction))
                .body(body)
                .type(Transaction.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Estornar transação cartão não presente
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /transactions/transaction_id/void
     *
     * @param idTransaction
     * @param body
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> reverseTransaction(String idTransaction, Map<String, Object> body,
            Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s/%s/%s", TRANSACTIONS, idTransaction, VOID))
                .body(body)
                .type(Transaction.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Capturar transação cartão não presente
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /transactions/transaction_id/capture
     *
     * @param idTransaction
     * @param body
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> capture(String idTransaction, Map<String, Object> body,
            Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s/%s/%s", TRANSACTIONS, idTransaction, CAPTURE))
                .body(body)
                .type(Transaction.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

}
