package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 *
 * @author william-fernandes
 */
public class BankAccount {

    private static final String BANK_ACCOUNTS = "bank_accounts";
    private static final String SELLERS = "sellers";

    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private RequestMaker requestMaker;

    /**
     * Recuperar detalhes de conta bancária por identificador
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /bank_accounts/bank_account_id
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param bankAccountiId
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> get(String bankAccountiId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s", BANK_ACCOUNTS, bankAccountiId))
                .type(BankAccount.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Alterar detalhes de conta bancária
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /bank_accounts/bank_account_id
     *
     * @param bankAccountiId
     * @param setup {@code Setup} objeto de configuração.
     * @param body
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> updateIndividuals(String bankAccountiId,
            Map<String, Object> body, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("PUT")
                .endpoint(String.format("/%s/%s/%s", BANK_ACCOUNTS, bankAccountiId))
                .body(body)
                .type(BankAccount.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Deleta conta bancária
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /bank_accounts/bank_account_id
     *
     * @param bankAccountiId {@code String}
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> delete(String bankAccountiId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("DELETE")
                .endpoint(String.format("%s/%s", BANK_ACCOUNTS, bankAccountiId))
                .type(BankAccount.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Listar contas bancárias por marketplace
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/bank_accounts
     *
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> list(Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s", BANK_ACCOUNTS))
                .type(BankAccount.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Associar conta bancaria com customer Associar token de cartão com
     * comprador
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/bank_accounts
     *
     * @param body {@code Setup} {"customer": "string","token": "string"
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> associate(Setup setup, Map<String, Object> body) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s", BANK_ACCOUNTS))
                .body(body)
                .type(BankAccount.class)
                .contentType(CONTENT_TYPE)
                .build();

        return requestMaker.doRequest(props);
    }

    /**
     * Listar contas bancárias por seller
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /sellers/seller_id/bank_accounts
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param sellerId
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> listSeller(String sellerId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s/%s", SELLERS, sellerId, BANK_ACCOUNTS))
                .type(BankAccount.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

}
