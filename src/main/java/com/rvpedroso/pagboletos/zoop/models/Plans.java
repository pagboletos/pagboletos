package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 *
 * @author william-fernandes
 */
public class Plans {

    private static final String PLANS = "plans";
    private static final String SUBSCRIPTIONS = "subscriptions";
    private static final String SELLERS = "sellers";

    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private RequestMaker requestMaker;

    /**
     * Criar plano de vendas Planos com taxa e políticas para credito de
     * recebíveis
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/plans
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param body
     *
     * @return {@code Map<String, Object>}.    @DeleteMapping("/plans/{id}")
    @Timed
    public ResponseEntity<Void> delete(@PathVariable String id) {
        plansService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

     */
    public Map<String, Object> create(Map<String, Object> body, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s", PLANS))
                .body(body)
                .type(Plans.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Listar plano de vendas por marketplace
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/plans
     *
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> list(Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s", PLANS))
                .type(Plans.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Recuperar detalhes de plano de venda
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id /plans/plan_id
     *
     * @param planId {@code String} user_id do usuário PagBoletos
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> get(String planId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s", PLANS, planId))
                .type(Plans.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Remover plano de venda
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id /plans/plan_id
     *
     * @param id {@code String} user_id do usuário PagBoletos
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> delete(String id, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("DELETE")
                .endpoint(String.format("/%s/%s", PLANS, id))
                .type(Plans.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Criar assinatura de plano de venda
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/subscriptions
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param body
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> createSubscriptions(Map<String, Object> body, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s", SUBSCRIPTIONS))
                .body(body)
                .type(Plans.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Listar assinaturas de plano de venda
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/subscriptions
     *
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> listSubscriptions(Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s", SUBSCRIPTIONS))
                .type(Plans.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Recuperar detalhes de assinatura de plano de venda
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /subscriptions/subscription_id
     *
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param subscriptionId {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> getSubscription(String subscriptionId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s", SUBSCRIPTIONS, subscriptionId))
                .type(Plans.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Remover plano de venda
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id /plans/plan_id
     *
     * @param id {@code String}
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> deleteSubscriptions(String id, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("DELETE")
                .endpoint(String.format("/%s/%s", SUBSCRIPTIONS, id))
                .type(Plans.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Recuperar detalhes de assinatura de plano de venda
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /sellers/seller_id/subscriptions
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param sellerId {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> getSubscriptionSeller(String sellerId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s/%s", SELLERS, sellerId, SUBSCRIPTIONS))
                .type(Plans.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Alterar plano pelo identificador Alterar detalhes de um plano existente
     * pelo identificador
     *
     *
     * @link https://api.zoop.ws/v2/marketplaces/marketplace_id/plans/plan_id
     *
     * @param id {@code String}
     * @param body
     * @param setup {@code Setup}
     *
     * @return {@code Map<String, Object}
     */
    public Map<String, Object> update(String id, Map<String, Object> body, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("PUT")
                .endpoint(String.format("%s/%s", PLANS, id))
                .body(body)
                .type(Plans.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Deletar um plano pelo identificador
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id /plans/plan_id
     *
     * @param id {@code String}
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> deletePlans(String id, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("DELETE")
                .endpoint(String.format("/%s/%s", PLANS, id))
                .type(Plans.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

}
