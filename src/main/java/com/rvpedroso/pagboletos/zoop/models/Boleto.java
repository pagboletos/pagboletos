package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 *
 * @author william-fernandes
 */
public class Boleto {

    private static final String BOLETOS = "boletos";
    private static final String EMAILS = "emails";

    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private RequestMaker requestMaker;

    /**
     * Enviar cobrança de boleto por email
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /boletos/boleto_id/emails
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param id
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> sendBillingEmail(String id, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s/%s/%s", BOLETOS, id, EMAILS))
                .type(Boleto.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Recuperar detalhes de boleto
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /boletos/boleto_id
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param id
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> get(String id, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s", BOLETOS, id))
                .type(Boleto.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

}
