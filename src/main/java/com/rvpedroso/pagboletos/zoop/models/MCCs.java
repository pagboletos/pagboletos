package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 * @version 1.0.0
 * @author william-fernandes
 */
public class MCCs {

    private static final String ENDPOINT = "/merchant_category_codes";
    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private static RequestMaker requestMaker;

    /**
     * Listar MCCs (Merchant Category Codes)
     *
     *
     * @link https://api.zoop.ws/users/{user_id}/permissions
     *
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> list(Setup setup) {
        MCCs.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(ENDPOINT)
                .type(MCCs.class)
                .contentType(CONTENT_TYPE)
                .build();

        return requestMaker.doRequest(props);
    }

}
