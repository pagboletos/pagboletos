package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 *
 * @author william-fernandes
 */
public class Events {

    private static final String EVENTS = "events";

    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private RequestMaker requestMaker;

    /**
     * Recuperar detalhes de evento pelo identificador
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/events/event_id
     *
     * @param eventId {@code String} user_id do usuário PagBoletos
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> get(String eventId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("%s/%s", EVENTS, eventId))
                .type(Events.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Listar eventos por marketplace
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/events
     *
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> list(Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s", EVENTS))
                .type(Events.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }
}
