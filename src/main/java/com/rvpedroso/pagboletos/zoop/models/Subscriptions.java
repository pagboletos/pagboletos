package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 *
 * @author william-fernandes
 */
public class Subscriptions {

    private static final String SUSPEND = "suspend";
    private static final String SUBSCRIPTIONS = "subscriptions";
    private static final String REACTIVATE = "reactivate";

    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private RequestMaker requestMaker;

    /**
     * Recuperar todas as assinatura de um marketplace
     *
     *
     * @link https://api.zoop.ws/v2/marketplaces/marketplace_id/subscriptions
     *
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> list(Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s", SUBSCRIPTIONS))
                .type(Subscriptions.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Criar uma assinatura entre um comprador e um plano
     *
     *
     * @link https://api.zoop.ws/v2/marketplaces/marketplace_id/subscriptions
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param body
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> create(Map<String, Object> body, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s", SUBSCRIPTIONS))
                .body(body)
                .type(Subscriptions.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Recuperar detalhes de assinatura de plano de venda
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /subscriptions/subscription_id
     *
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param subscriptionId {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> getSubscription(String subscriptionId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s", SUBSCRIPTIONS, subscriptionId))
                .type(Subscriptions.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Alterar os detalhes de uma assinatura pelo identificador
     *
     *
     * @link https://api.zoop.ws/v2/marketplaces/marketplace_id
     * /subscriptions/subscription_id
     *
     * @param subscriptionId {@code String}
     * @param body
     * @param setup {@code Setup}
     *
     * @return {@code Map<String, Object}
     */
    public Map<String, Object> update(String subscriptionId, Map<String, Object> body, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("PUT")
                .endpoint(String.format("%s/%s", SUBSCRIPTIONS, subscriptionId))
                .body(body)
                .type(Subscriptions.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Alterar os detalhes de uma assinatura pelo identificador
     *
     *
     * @link https://api.zoop.ws/v2/marketplaces/marketplace_id
     * /subscriptions/subscription_id
     *
     * @param subscriptionId {@code String}
     * @param setup {@code Setup}
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> delete(String subscriptionId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("DELETE")
                .endpoint(String.format("%s/%s", SUBSCRIPTIONS, subscriptionId))
                .type(Subscriptions.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Suspender uma assinatura pelo identificador
     *
     *
     *
     * @link https://api.zoop.ws/v2/marketplaces/marketplace_id
     * /subscriptions/subscription_id/suspend
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param subscriptionId
     * @param body
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> suspendSignature(String subscriptionId,
            Map<String, Object> body, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("%s/%s/%s", SUBSCRIPTIONS, subscriptionId, SUSPEND))
                .body(body)
                .type(Subscriptions.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Suspender uma assinatura pelo identificador
     *
     *
     *
     * @link https://api.zoop.ws/v2/marketplaces/marketplace_id
     * /subscriptions/subscription_id/reactivate
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param subscriptionId
     * @param body
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> reactivateSignature(String subscriptionId,
            Map<String, Object> body, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("%s/%s/%s", SUBSCRIPTIONS, subscriptionId, REACTIVATE))
                .body(body)
                .type(Subscriptions.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }
}
