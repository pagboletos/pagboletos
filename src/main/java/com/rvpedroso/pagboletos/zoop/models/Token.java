package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 *
 * @author william-fernandes
 */
public class Token {

    private static final String CARDS = "cards";
    private static final String TOKENS = "tokens";
    private static final String BANK = "bank_accounts";

    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private RequestMaker requestMaker;

    /**
     * Criar novo token de cartão
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/cards/tokens
     *
     * @param body
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object}
     */
    public Map<String, Object> createToken(Map<String, Object> body, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s/%s", CARDS, TOKENS))
                .body(body)
                .type(Token.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Criar novo token de conta bancária
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/bank_accounts/tokens
     *
     * @param body
     * @param setup {@code Setup}
     *
     * @return {@code Map<String, Object}
     */
    public Map<String, Object> createTokenBankAccounts(Map<String, Object> body,
            Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s/%s", BANK, TOKENS))
                .body(body)
                .type(Token.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }
    
    /**
     * Recuperar detalhes de token de cartão/conta bancária
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/tokens/token_id
     *
     * @param idToken {@code String}
     * @param setup {@code Setup}
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> get(String idToken, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("%s/%s", TOKENS, idToken))
                .type(Token.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }
}
