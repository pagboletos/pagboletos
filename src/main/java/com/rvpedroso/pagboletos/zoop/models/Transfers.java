package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 *
 * @author william-fernandes
 */
public class Transfers {

    private static final String TRANSFERS = "transfers";
    private static final String SELLERS = "sellers";
    private static final String BANK_ACCOUNTS = "bank_accounts";
    private static final String TRANSACTIONS = "transactions";
    private static final String TO = "to";

    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private RequestMaker requestMaker;

    /**
     * Listar transferências por seller
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /sellers/seller_id/transfers
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param idSeller
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> listTransfersSeller(String idSeller, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s/%s", SELLERS, idSeller, TRANSFERS))
                .type(Transfers.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Criar transferência para bancária
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /bank_accounts/bank_account_id/transfers
     *
     * @param bankAccountId identificador da conta bancária
     * @param body
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> createTransfersBank(String bankAccountId,
            Map<String, Object> body,
            Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s/%s/%s", BANK_ACCOUNTS, bankAccountId, TRANSFERS))
                .body(body)
                .type(Transfers.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Listar transferência por marketplace
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/transfers
     *
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> listTransfersMarketplaces(Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s", TRANSFERS))
                .type(Transfers.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Recuperar detalhes de transferência
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /transfers/transfer_id
     *
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param transferId
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> get(String transferId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s", TRANSFERS, transferId))
                .type(Transfers.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Cancelar transferência agendada anteriormente à data prevista para efetivação
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/transfers/{transfer_id}
     *
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param transferId
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> cancelarAgendada(String transferId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("DELETE")
                .endpoint(String.format("/%s/%s", TRANSFERS, transferId))
                .type(Transfers.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Listar transações associadas a transferência
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id
     * /transfers/transfer_id/transactions
     *
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param transferId
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> list(String transferId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s/%s/%s", TRANSFERS, transferId, TRANSACTIONS))
                .type(Transfers.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Criar transferência P2P
     *
     *
     * @link https://api.zoop.ws/v2/marketplaces/marketplace_id
     * /transfers/ownerId/to/receiver
     *
     * @param ownerId identificador do customer pagador
     * @param receiverId identificador do customer recebedor
     * @param body
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> transfersP2P(String ownerId, String receiverId,
            Map<String, Object> body,
            Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s/%s/%s/%s", TRANSFERS, ownerId, TO, receiverId))
                .body(body)
                .type(Transfers.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

}
