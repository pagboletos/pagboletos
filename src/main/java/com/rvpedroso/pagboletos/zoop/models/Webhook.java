package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;

import java.util.Map;

/**
 *
 * @author william-fernandes
 */
public class Webhook {

    private static final String WEBHOOKS = "webhooks";

    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private RequestMaker requestMaker;

    /**
     * Cria webhook por marketplace
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/webhooks
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param body
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> create(Map<String, Object> body, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("POST")
                .endpoint(String.format("/%s", WEBHOOKS))
                .body(body)
                .type(Webhook.class)
                .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Listar webhooks por marketplace
     *
     *
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/webhooks
     *
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> list(Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("/%s", WEBHOOKS))
                .type(Webhook.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Recuperar detalhes de webhook
     *
     *
     * @link
     * https://api.zoop.ws/v1/marketplaces/marketplace_id/webhooks/webhook_id
     *
     * @param webhookId {@code String} user_id do usuário PagBoletos
     * @param setup {@code Setup} objeto de configuração.
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> get(String webhookId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("GET")
                .endpoint(String.format("%s/%s", WEBHOOKS, webhookId))
                .type(Webhook.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Remover webhook
     *
     *
     * @link
     * https://api.zoop.ws/v1/marketplaces/marketplace_id/webhooks/webhook_id
     *
     * @param webhookId {@code String}
     * @param setup {@code Setup}
     *
     * @return {@code Map<String, Object>}.
     */
    public Map<String, Object> delete(String webhookId, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
                .method("DELETE")
                .endpoint(String.format("%s/%s", WEBHOOKS, webhookId))
                .type(Webhook.class)
                .contentType(CONTENT_TYPE)
                .build();

        return this.requestMaker.doRequest(props);
    }

}
