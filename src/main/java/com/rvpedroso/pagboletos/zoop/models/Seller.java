package com.rvpedroso.pagboletos.zoop.models;

import com.rvpedroso.pagboletos.web.rest.model.Paginate;
import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.api.request.RequestProperties;
import com.rvpedroso.pagboletos.zoop.api.request.RequestPropertiesBuilder;
import org.apache.http.entity.ContentType;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

/**
 * @author william-fernandes
 * @version 1.0.0
 */
public class Seller {

    private static final String ENDPOINT = "sellers";
    private static final String INDIVIDUALS = "individuals";
    private static final String BUSINESSES = "businesses";

    private static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    private RequestMaker requestMaker;

    /**
     * Criar novo vendedor do tipo indivíduo - Pessoa Física
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param body
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers/individuals
     */
    public Map<String, Object> createIndividuals(Map<String, Object> body, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
            .method("POST")
            .endpoint(String.format("/%s/%s", ENDPOINT, INDIVIDUALS))
            .body(body)
            .type(Seller.class)
            .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Alterar um vendedor do tipo indivíduo - Pessoa Física
     *
     * @param idSeller
     * @param setup    {@code Setup} objeto de configuração.
     * @param body
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers/individuals/id
     */
    public Map<String, Object> updateIndividuals(String idSeller, Map<String, Object> body, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
            .method("PUT")
            .endpoint(String.format("/%s/%s/%s", ENDPOINT, INDIVIDUALS, idSeller))
            .body(body)
            .type(Seller.class)
            .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Criar novo vendedor do tipo indivíduo - Pessoa Jurídica
     *
     * @param setup {@code Setup} objeto de configuração.
     * @param body
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers/businesses
     */
    public Map<String, Object> createBusinesses(Map<String, Object> body, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
            .method("POST")
            .endpoint(String.format("/%s/%s", ENDPOINT, BUSINESSES))
            .body(body)
            .type(Seller.class)
            .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Alterar um vendedor do tipo indivíduo - Pessoa Jurídica
     *
     * @param idSeller
     * @param setup    {@code Setup} objeto de configuração.
     * @param body
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers/businesses/id
     */
    public Map<String, Object> updateBusinesses(String idSeller, Map<String, Object> body, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
            .method("PUT")
            .endpoint(String.format("/%s/%s/%s", ENDPOINT, BUSINESSES, idSeller))
            .body(body)
            .type(Seller.class)
            .contentType(CONTENT_TYPE);

        return this.requestMaker.doRequest(props);
    }

    /**
     * Recupera detalhes de vendedor pelo id
     *
     * @param id    {@code String} user_id do usuário PagBoletos
     * @param setup {@code Setup} objeto de configuração.
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers/{id}
     */
    public Map<String, Object> get(String id, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
            .method("GET")
            .endpoint(String.format("/%s/%s", ENDPOINT, id))
            .type(Seller.class)
            .contentType(CONTENT_TYPE)
            .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Deleta um de vendedor pelo id
     *
     * @param id    {@code String} user_id do usuário PagBoletos
     * @param setup {@code Setup} objeto de configuração.
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers/{id}
     */
    public Map<String, Object> delete(String id, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
            .method("DELETE")
            .endpoint(String.format("/%s/%s", ENDPOINT, id))
            .type(Seller.class)
            .contentType(CONTENT_TYPE)
            .build();

        return this.requestMaker.doRequest(props);
    }

    /**
     * Listando vendedores
     *
     * @param setup {@code Setup} objeto de configuração.
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers
     */
    public Map<String, Object> list(Setup setup, Paginate pageable) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
            .method("GET")
            .endpoint(ENDPOINT)
            .endpoint(String.format("/%s?%s", ENDPOINT, pageable.toString()))
            .type(Seller.class)
            .contentType(CONTENT_TYPE)
            .build();

        return requestMaker.doRequest(props);
    }

    /**
     * Listando vendedores pelo CPF
     *
     * @param setup {@code Setup} objeto de configuração.
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers/search
     */
    public Map<String, Object> searchIndividuals(String cpf, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
            .method("GET")
            .endpoint(String.format("/%s/%s%s", ENDPOINT, "search?taxpayer_id=", cpf))
            .type(Seller.class)
            .contentType(CONTENT_TYPE)
            .build();
        return requestMaker.doRequest(props);
    }

    /**
     * Listando vendedores pelo CNPJ
     *
     * @param setup {@code Setup} objeto de configuração.
     * @return {@code Map<String, Object>}.
     * @link https://api.zoop.ws/v1/marketplaces/marketplace_id/sellers/search
     */
    public Map<String, Object> searchBusinesses(String cnpj, Setup setup) {
        this.requestMaker = new RequestMaker(setup);
        RequestProperties props = new RequestPropertiesBuilder()
            .method("GET")
            .endpoint(String.format("/%s/%s%s", ENDPOINT, "search?ein=", cnpj))
            .type(Seller.class)
            .contentType(CONTENT_TYPE)
            .build();
        return requestMaker.doRequest(props);
    }

}
