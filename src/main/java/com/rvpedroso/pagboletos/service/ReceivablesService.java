package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.ReceivablesZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class ReceivablesService {

    private final ReceivablesZoop receivablesZoop;
    private final UserService userService;

    public ReceivablesService(ReceivablesZoop receivablesZoop, UserService userService) {
        this.receivablesZoop = receivablesZoop;
        this.userService = userService;
    }

    /**
     * Recuperar detalhes de recebível
     */
    public Map<String, Object> get(String receivablesId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.receivablesZoop.get(receivablesId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Listar recebíveis por transação Listar todas as parcelas de recebimento
     * por transação
     */
    public Map<String, Object> getTransactions(String receivablesId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.receivablesZoop.getTransactions(receivablesId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Listar recebíveis por seller
     * Listar todas as parcelas de recebimento por seller
     */
    public Map<String, Object> getSeller(String sellerId) {
        Map<String, Object> balanceRetorno;
        String id = userService.getIdSeller(sellerId);

        try {
            balanceRetorno = this.receivablesZoop.getSeller(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

}
