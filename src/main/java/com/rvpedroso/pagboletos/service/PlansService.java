package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.PlansZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class PlansService {

    private final PlansZoop plansZoop;
    private final UserService userService;

    public PlansService(PlansZoop plansZoop, UserService userService) {
        this.plansZoop = plansZoop;
        this.userService = userService;
    }

    /**
     * Criar plano de vendas Planos com taxa e políticas para credito de
     * recebíveis
     */
    public Map<String, Object> create(Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.plansZoop.create(body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Listar plano de vendas por marketplace
     */
    public Map<String, Object> list() {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.plansZoop.list();
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Recuperar detalhes de plano de venda
     */
    public Map<String, Object> get(String planId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.plansZoop.get(planId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Remover plano de venda
     */
    public Map<String, Object> delete(String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.plansZoop.delete(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Criar assinatura de plano de venda
     */
    public Map<String, Object> createSubscriptions(Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.plansZoop.createSubscriptions(body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Listar assinaturas de plano de venda
     */
    public Map<String, Object> listSubscriptions() {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.plansZoop.listSubscriptions();
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Recuperar detalhes de assinatura de plano de venda
     */
    public Map<String, Object> getSubscription(String subscriptionId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.plansZoop.getSubscription(subscriptionId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Remover plano de venda
     */
    public Map<String, Object> deleteSubscriptions(String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.plansZoop.deleteSubscriptions(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Recuperar detalhes de assinatura de plano de venda
     */
    public Map<String, Object> getSubscriptionSeller(String sellerId) {
        Map<String, Object> balanceRetorno;
        String id = userService.getIdSeller(sellerId);

        try {
            balanceRetorno = this.plansZoop.getSubscriptionSeller(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Alterar plano pelo identificador Alterar detalhes de um plano existente
     * pelo identificador
     */
    public Map<String, Object> update(String id, Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.plansZoop.update(id, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Deletar um plano pelo identificador
     */
    public Map<String, Object> deletePlans(String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.plansZoop.deletePlans(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

}
