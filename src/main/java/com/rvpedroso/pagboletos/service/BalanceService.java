package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.BalanceZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class BalanceService {

    private final BalanceZoop balanceZoop;
    private final UserService userService;

    public BalanceService(BalanceZoop balanceZoop, UserService userService) {
        this.balanceZoop = balanceZoop;
        this.userService = userService;
    }

    /**
     * Recuperar saldo de conta por seller Recupera o saldo corrente e saldo
     * total de conta
     */
    public Map<String, Object> get(String sellerId) {
        Map<String, Object> balanceRetorno;
        String id = userService.getIdSeller(sellerId);

        try {
            balanceRetorno = this.balanceZoop.get(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Lista contas por seller
     */
    public Map<String, Object> listBalanceSeller(String sellerId) {
        Map<String, Object> balanceRetorno;
        String id = userService.getIdSeller(sellerId);

        try {
            balanceRetorno = this.balanceZoop.listBalanceSeller(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Lista contas por buyer
     */
    public Map<String, Object> listBalanceBuyer(String buyerId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.balanceZoop.listBalanceBuyer(buyerId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Listar histórico de lançamentos pelo identificador da conta
     */
    public Map<String, Object> listHistory(String balanceId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.balanceZoop.listHistory(balanceId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Listar histórico de lançamentos de conta por seller Listagem de histórico
     * da conta principal do seller
     */
    public Map<String, Object> listHistorySeller(String sellerId) {
        Map<String, Object> balanceRetorno;
        String id = userService.getIdSeller(sellerId);

        try {
            balanceRetorno = this.balanceZoop.listHistorySeller(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Listar histórico de lançamentos de conta por buyer Listagem de histórico
     * da conta principal do buyer
     */
    public Map<String, Object> listHistoryBuyer(String buyerId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.balanceZoop.listHistoryBuyer(buyerId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

}
