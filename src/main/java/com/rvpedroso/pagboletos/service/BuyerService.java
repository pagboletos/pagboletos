package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.BuyerZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class BuyerService {


    private final BuyerZoop buyerZoop;

    public BuyerService(BuyerZoop buyerZoop) {
        this.buyerZoop = buyerZoop;
    }

    /**
     * Criar comprador
     */
    public Map<String, Object> create(Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.buyerZoop.create(body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Alterar detalhes de comprador
     */
    public Map<String, Object> update(String idBuyer, Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.buyerZoop.update(idBuyer, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Recuperar detalhes de comprador
     */
    public Map<String, Object> get(String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.buyerZoop.get(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Deleta um de vendedor pelo id
     */
    public Map<String, Object> delete(String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.buyerZoop.delete(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Listar comprador por marketplace
     */
    public Map<String, Object> list() {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.buyerZoop.list();
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Buscar comprador por CPF/CNPJ
     */
    public Map<String, Object> search(Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.buyerZoop.search(body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

}
