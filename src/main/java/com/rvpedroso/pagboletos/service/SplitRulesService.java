package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.SplitRulesZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class SplitRulesService {

    private final SplitRulesZoop splitRulesZoop;

    public SplitRulesService(SplitRulesZoop splitRulesZoop) {
        this.splitRulesZoop = splitRulesZoop;
    }

    /**
     * Recuperar detalhes de regra de divisão por transação
     */
    public Map<String, Object> get(String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.splitRulesZoop.get(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Recuperar detalhes de regra de divisão por transação
     */
    public Map<String, Object> create(Map<String, Object> body, String transactionId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.splitRulesZoop.create(body, transactionId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Recupera detalhes de regra de divisão por transação
     */
    public Map<String, Object> details(String transactionId, String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.splitRulesZoop.details(transactionId, id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Alterar regra de divisão por transação
     */
    public Map<String, Object> update(Map<String, Object> body, String transactionId, String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.splitRulesZoop.update(body, transactionId, id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Remover regra de divisão por transação
     */
    public Map<String, Object> delete(String transactionId, String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.splitRulesZoop.delete(transactionId, id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

}
