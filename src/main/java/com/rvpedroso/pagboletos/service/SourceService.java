package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.SourceZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class SourceService {


    private final SourceZoop sourceZoop;

    public SourceService(SourceZoop sourceZoop) {
        this.sourceZoop = sourceZoop;
    }

    /**
     * Criar source para utilizacão transação Um objeto source permite você
     * aceitar uma variedade de formas de pagamento
     */
    public Map<String, Object> create(Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.sourceZoop.create(body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Recuperar detalhes de source tão pelo identificador
     */
    public Map<String, Object> get(String sourceId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.sourceZoop.get(sourceId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Remover source pelo identificador
     */
    public Map<String, Object> delete(String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.sourceZoop.delete(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }
}
