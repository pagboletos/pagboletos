package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.pagboletos.DocumetosPagBoletos;
import com.rvpedroso.pagboletos.service.zoop.DocumentsZoop;
import com.rvpedroso.pagboletos.web.rest.errors.InternalServerErrorException;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.web.rest.model.Paginate;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/

@Service
public class DocumentsService {


    private final DocumentsZoop documentsZoop;
    private final DocumetosPagBoletos documetosPagBoletos;
    private final UserService userService;

    public DocumentsService(DocumentsZoop documentsZoop, DocumetosPagBoletos documetosPagBoletos, UserService userService) {
        this.documentsZoop = documentsZoop;
        this.documetosPagBoletos = documetosPagBoletos;
        this.userService = userService;
    }

    /**
     * Criar documento de cadastro de vendedor
     */
    public Map<String, Object> create(String sellerId, MultipartFile files, String tipo) {
        Map<String, Object> vendedorRetorno = null;
        String id = userService.getIdSeller(sellerId);

        try {
            vendedorRetorno = this.documentsZoop.create(id, files, tipo);
            this.documetosPagBoletos.create(vendedorRetorno);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        } catch (Exception ex) {
            throw new InternalServerErrorException(ex.getMessage());
        }

        return vendedorRetorno;
    }

    /**
     * Listar documentos de um vendedor
     */
    public Map<String, Object> list(String sellerId, Paginate paginate) {
        Map<String, Object> vendedorRetorno;
        String id = userService.getIdSeller(sellerId);

        try {
            vendedorRetorno = this.documentsZoop.list(id, paginate);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        } catch (Exception ex) {
            throw new InternalServerErrorException(ex.getMessage());
        }

        return vendedorRetorno;
    }

    /**
     * Atualiza os dados de um documento de um vendedor
     */
    public Map<String, Object> update(String idDocuments, Map<String, Object> body) {
        Map<String, Object> vendedorRetorno = null;

        try {
            vendedorRetorno = this.documentsZoop.update(idDocuments, body);
            this.documetosPagBoletos.update(vendedorRetorno);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        } catch (Exception ex) {
            throw new InternalServerErrorException(ex.getMessage());
        }

        return vendedorRetorno;
    }

}
