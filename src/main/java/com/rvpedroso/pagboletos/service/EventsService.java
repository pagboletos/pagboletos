package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.EventsZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class EventsService {

    private final EventsZoop eventsZoop;

    public EventsService(EventsZoop eventsZoop) {
        this.eventsZoop = eventsZoop;
    }

    /**
     * Recuperar detalhes de evento pelo identificador
     */
    public Map<String, Object> get(String eventIdp) {
        Map<String, Object> vendedorRetorno = null;

        try {
            vendedorRetorno = this.eventsZoop.get(eventIdp);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return vendedorRetorno;
    }

    /**
     * Listar eventos por marketplace
     */
    public Map<String, Object> list() {
        Map<String, Object> vendedorRetorno = null;

        try {
            vendedorRetorno = this.eventsZoop.list();
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return vendedorRetorno;
    }
}
