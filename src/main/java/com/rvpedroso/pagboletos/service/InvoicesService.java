package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.InvoicesZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class InvoicesService {

    private final InvoicesZoop invoicesZoop;

    public InvoicesService(InvoicesZoop invoicesZoop) {
        this.invoicesZoop = invoicesZoop;
    }

    /**
     * Recuperar todas as faturas de um marketplace
     */
    public Map<String, Object> list() {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.invoicesZoop.list();
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Criar uma fatura avulsa
     */
    public Map<String, Object> create(Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.invoicesZoop.create(body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Recuperar os detalhes de uma fatura pelo identificador - Você pode
     * recuperar os detalhes de uma determinada fatura através do identificador
     */
    public Map<String, Object> get(String invoiceId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.invoicesZoop.get(invoiceId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Alterar detalhes de uma fatura pelo identificador
     */
    public Map<String, Object> update(String invoiceId, Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.invoicesZoop.update(invoiceId, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Remover uma fatura pelo identificador
     */
    public Map<String, Object> delete(String invoiceId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.invoicesZoop.delete(invoiceId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Aprovar fatura pendente - Aprovar manualmente uma fatura pendente, sem
     * que seja feita a cobrança ao cobrador
     */
    public Map<String, Object> approveInvoicePending(String invoiceId, Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.invoicesZoop.approveInvoicePending(invoiceId, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Estornar e reembolsar fatura - Estornar uma fatura paga, sendo feito o
     * processamento do estorno da forma de cobrança associada ao pagamento da
     * fatura
     */
    public Map<String, Object> reverseInvoicePending(String invoiceId, Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.invoicesZoop.reverseInvoicePending(invoiceId, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Recuperar faturas associadas a um vendedor pelo identificador - Recuperar
     * os detalhes de todas as faturas associadas a um determinado vendedor pelo
     * identificador
     */
    public Map<String, Object> getinvoicesSeller(String customerId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.invoicesZoop.getinvoicesSeller(customerId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

}
