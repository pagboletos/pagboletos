package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.CardZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class CardService {

    private final CardZoop cardZoop;

    public CardService(CardZoop cardZoop) {
        this.cardZoop = cardZoop;
    }

    /**
     * Associar cartão com comprador Associar token de cartão com comprador
     */
    public Map<String, Object> associate(Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.cardZoop.associate(body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }


    /**
     * Recuperar detalhes de cartão pelo identificador
     */
    public Map<String, Object> get(String cardId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.cardZoop.get(cardId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Remover cartão pelo identificador
     */
    public Map<String, Object> delete(String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.cardZoop.delete(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Atualizar detalhes de cartão pelo identificador
     */
    public Map<String, Object> update(Map<String, Object> body, String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.cardZoop.update(body, id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }


}
