package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.ReceivingPolicyZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class ReceivingPolicyService {

    private final UserService userService;
    private final ReceivingPolicyZoop receivingPolicyZoop;

    public ReceivingPolicyService(UserService userService, ReceivingPolicyZoop receivingPolicyZoop) {
        this.userService = userService;
        this.receivingPolicyZoop = receivingPolicyZoop;
    }

    /**
     * Recuperar política de recebimento por seller
     */
    public Map<String, Object> get(String sellerId) {
        Map<String, Object> balanceRetorno;
        String id = userService.getIdSeller(sellerId);

        try {
            balanceRetorno = this.receivingPolicyZoop.get(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Alterar política de recebimento por seller
     */
    public Map<String, Object> update(String sellerId, Map<String, Object> body) {
        Map<String, Object> balanceRetorno;
        String id = userService.getIdSeller(sellerId);

        try {
            balanceRetorno = this.receivingPolicyZoop.update(id, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

}
