package com.rvpedroso.pagboletos.service.pagboletos;

import com.rvpedroso.pagboletos.repository.DocumentosRepository;
import com.rvpedroso.pagboletos.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: william
 * Data: 03/04/19
 * Horário: 13
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service.pagboletos
 **/
@Service
public class DocumetosPagBoletos {

    private final UserService userService;

    private final DocumentosRepository documentosRepository;

    private final Logger log = LoggerFactory.getLogger(DocumetosPagBoletos.class);

    public DocumetosPagBoletos(UserService userService, DocumentosRepository documentosRepository) {
        this.userService = userService;
        this.documentosRepository = documentosRepository;
    }

    public void create(Map<String, Object> documento) {

    }

    public void update(Map<String, Object> documento) {

    }

}
