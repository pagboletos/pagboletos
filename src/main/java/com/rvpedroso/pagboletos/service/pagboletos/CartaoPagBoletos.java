package com.rvpedroso.pagboletos.service.pagboletos;

import com.rvpedroso.pagboletos.repository.CartaoRepository;
import com.rvpedroso.pagboletos.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Usuário: william
 * Data: 03/04/19
 * Horário: 13
 * Projeto: pagcartaos
 * Pacote: com.rvpedroso.pagcartaos.service.pagcartaos
 **/
@Service
public class CartaoPagBoletos {

    private final UserService userService;

    private final CartaoRepository cartaoRepository;

    private final Logger log = LoggerFactory.getLogger(CartaoPagBoletos.class);

    public CartaoPagBoletos(UserService userService, CartaoRepository cartaoRepository) {
        this.userService = userService;
        this.cartaoRepository = cartaoRepository;
    }

}
