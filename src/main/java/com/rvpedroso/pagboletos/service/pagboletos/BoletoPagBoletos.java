package com.rvpedroso.pagboletos.service.pagboletos;

import com.rvpedroso.pagboletos.repository.BoletoRepository;
import com.rvpedroso.pagboletos.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Usuário: william
 * Data: 03/04/19
 * Horário: 13
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service.pagboletos
 **/
@Service
public class BoletoPagBoletos {

    private final UserService userService;

    private final BoletoRepository boletoRepository;

    private final Logger log = LoggerFactory.getLogger(BoletoPagBoletos.class);

    public BoletoPagBoletos(UserService userService, BoletoRepository boletoRepository) {
        this.userService = userService;
        this.boletoRepository = boletoRepository;
    }

}
