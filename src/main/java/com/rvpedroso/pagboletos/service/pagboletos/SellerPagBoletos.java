package com.rvpedroso.pagboletos.service.pagboletos;

import com.rvpedroso.pagboletos.domain.Authority;
import com.rvpedroso.pagboletos.domain.User;
import com.rvpedroso.pagboletos.domain.Vendedor;
import com.rvpedroso.pagboletos.repository.AuthorityRepository;
import com.rvpedroso.pagboletos.repository.UserRepository;
import com.rvpedroso.pagboletos.repository.VendedorRepository;
import com.rvpedroso.pagboletos.security.AuthoritiesConstants;
import com.rvpedroso.pagboletos.service.util.Criptografia;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Usuário: william
 * Data: 03/04/19
 * Horário: 13
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service.pagboletos
 **/
@Service
public class SellerPagBoletos {

    private final PasswordEncoder passwordEncoder;
    private final AuthorityRepository authorityRepository;
    private final UserRepository userRepository;
    private final VendedorRepository vendedorRepository;

    public SellerPagBoletos(PasswordEncoder passwordEncoder, AuthorityRepository authorityRepository, UserRepository userRepository
        , VendedorRepository vendedorRepository) {
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
        this.userRepository = userRepository;
        this.vendedorRepository = vendedorRepository;
    }

    /**
     * Criar novo vendedor do tipo indivíduo - Pessoa Física
     */
    public User createIndividuals(Map<String, Object> usuario) {
        User usuarioNovo = createSeller(usuario, AuthoritiesConstants.USER);
        return userRepository.save(usuarioNovo);
    }

    /**
     * Criar novo vendedor do tipo indivíduo - Pessoa Física
     */
    public void createBusinesses(Map<String, Object> usuario) {
        User usuarioNovo = createSeller(usuario, AuthoritiesConstants.PARCEIRO);
        userRepository.save(usuarioNovo);
    }

    /**
     * Classe genérica para criação do vendedor (seller)
     */
    private User createSeller(Map<String, Object> usuario, String tipo) {
        User usuarioNovo = new User();
        Set<Authority> authorities = new HashSet<>();

        // Atribuir dados do usuário
        Map<String, String> senha = getPassword(usuario);
        usuarioNovo.setPassword(senha.get("passwordEncoder"));
        usuarioNovo.setSenha(senha.get("criptografada"));

        usuarioNovo.setLogin(usuario.get("taxpayer_id").toString());
        usuarioNovo.setFirstName(usuario.get("first_name").toString());
        usuarioNovo.setLastName(usuario.get("last_name").toString());
        usuarioNovo.setEmail(usuario.get("email").toString().toLowerCase());
        usuarioNovo.setActivated(true);
        authorityRepository.findById(tipo).ifPresent(authorities::add);
        usuarioNovo.setAuthorities(authorities);
        usuarioNovo.setActivated(true);
        usuarioNovo.setActivationKey(null);

        // Salva o usuário e id do vendedor no sistema do Zoop
        Vendedor vendedor = vendedorRepository.save(new Vendedor(usuario.get("id").toString()));
        usuarioNovo.setVendedor(vendedor);

        return usuarioNovo;
    }

    private Map<String, String> getPassword(Map<String, Object> seller) {

        Map<String, String> senhaDoUsuario = new HashMap<>();
        String senha;

        try {
            senha = seller.get("password").toString();
        } catch (NullPointerException ex) {
            Random rnd = new Random();
            senha = Integer.toString(100000 + rnd.nextInt(900000));
        }

        senhaDoUsuario.put("criptografada", Criptografia.codificar(senha));
        senhaDoUsuario.put("passwordEncoder", passwordEncoder.encode(senha));
        return senhaDoUsuario;

    }


}
