package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.TokenZoopZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class TokenZoopService {


    private final TokenZoopZoop tokenZoopZoop;

    public TokenZoopService(TokenZoopZoop tokenZoopZoop) {
        this.tokenZoopZoop = tokenZoopZoop;
    }

    /**
     * Criar novo token de cartão
     */
    public Map<String, Object> createToken(Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.tokenZoopZoop.createToken(body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Criar novo token de conta bancária
     */
    public Map<String, Object> createTokenBankAccounts(Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.tokenZoopZoop.createTokenBankAccounts(body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Recuperar detalhes de token de cartão/conta bancária
     */
    public Map<String, Object> get(String idToken) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.tokenZoopZoop.get(idToken);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }
}
