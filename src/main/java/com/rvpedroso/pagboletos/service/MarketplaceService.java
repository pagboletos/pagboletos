package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.MarketplaceZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class MarketplaceService {

    private final MarketplaceZoop marketplaceZoop;

    public MarketplaceService(MarketplaceZoop marketplaceZoop) {
        this.marketplaceZoop = marketplaceZoop;
    }

    /**
     * Recuperar detalhes do marketplace
     */
    public Map<String, Object> get() {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.marketplaceZoop.get();
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

}
