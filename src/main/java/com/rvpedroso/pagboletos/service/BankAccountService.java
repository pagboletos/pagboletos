package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.BankAccountZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class BankAccountService {


    private final BankAccountZoop bankAccountZoop;
    private final UserService userService;

    public BankAccountService(BankAccountZoop bankAccountZoop, UserService userService) {
        this.bankAccountZoop = bankAccountZoop;
        this.userService = userService;
    }

    /**
     * Recuperar detalhes de conta bancária por identificador
     */
    public Map<String, Object> get(String bankAccountiId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.bankAccountZoop.get(bankAccountiId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Alterar detalhes de conta bancária
     */
    public Map<String, Object> updateIndividuals(String bankAccountiId, Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.bankAccountZoop.updateIndividuals(bankAccountiId, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Deleta conta bancária
     */
    public Map<String, Object> delete(String bankAccountiId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.bankAccountZoop.delete(bankAccountiId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Listar contas bancárias por marketplace
     */
    public Map<String, Object> list() {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.bankAccountZoop.list();
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Associar conta bancaria com customer Associar token de cartão com
     * comprador
     */
    public Map<String, Object> associate(Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.bankAccountZoop.associate(body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Listar contas bancárias por seller
     */
    public Map<String, Object> listSeller(String sellerId) {
        Map<String, Object> balanceRetorno;
        String id = userService.getIdSeller(sellerId);

        try {
            balanceRetorno = this.bankAccountZoop.listSeller(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

}
