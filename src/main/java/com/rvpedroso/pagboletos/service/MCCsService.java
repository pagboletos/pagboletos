package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.MCCsZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class MCCsService {

    private final MCCsZoop mcCsZoop;

    public MCCsService(MCCsZoop mcCsZoop) {
        this.mcCsZoop = mcCsZoop;
    }

    /**
     * Listar MCCs (Merchant Category Codes)
     */
    public Map<String, Object> list() {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.mcCsZoop.list();
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

}
