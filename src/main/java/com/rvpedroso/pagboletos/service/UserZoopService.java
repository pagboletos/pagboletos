package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.UserZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class UserZoopService {

    private final UserZoop userZoop;

    public UserZoopService(UserZoop userZoop) {
        this.userZoop = userZoop;
    }

    /**
     * Criar novo usuário de API por marketplace
     */
    public Map<String, Object> create(Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.userZoop.create(body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Relizar login por usuário/senha
     */
    public Map<String, Object> login(Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.userZoop.login(body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Alterar detalhes de usuário
     */
    public Map<String, Object> update(Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.userZoop.update(body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Este método é usado para obter os dados de um usuário criado pelo usuário
     * PagBoletos external ID.
     */
    public Map<String, Object> get(String userId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.userZoop.get(userId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Remover usuário
     */
    public Map<String, Object> delete(String userId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.userZoop.delete(userId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }
}
