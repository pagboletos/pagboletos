package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.PermissionZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class PermissionService {

    private final UserService userService;
    private final PermissionZoop permissionZoop;

    public PermissionService(UserService userService, PermissionZoop permissionZoop) {
        this.userService = userService;
        this.permissionZoop = permissionZoop;
    }

    /**
     * Permissão do usuário Busca todas as permissões que usuário possui pelo
     * user_id
     */
    public Map<String, Object> getPermissionUser(String userId) {
        Map<String, Object> balanceRetorno;
        String id = userService.getIdSeller(userId);

        try {
            balanceRetorno = this.permissionZoop.getPermissionUser(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Criar permissão para usuário Cria uma nova permissão para o usuário,
     * podendo vincular à marketplace, seller ou grupo
     */
    public Map<String, Object> createPermission(String userId, Map<String, Object> body) {
        Map<String, Object> balanceRetorno;
        String id = userService.getIdSeller(userId);

        try {
            balanceRetorno = this.permissionZoop.createPermission(id, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Permissão do usuário por permission_id Permissões que usuário possui pelo
     * permission_id
     */
    public Map<String, Object> getPermissionId(String userId, String permissionId) {
        Map<String, Object> balanceRetorno;
        String id = userService.getIdSeller(userId);

        try {
            balanceRetorno = this.permissionZoop.getPermissionId(id, permissionId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }


    /**
     * Deleta uma permissão Remove uma permissões do usuário pelo permission_id
     */
    public Map<String, Object> deletePermission(String userId, String permissionId) {
        Map<String, Object> balanceRetorno;
        String id = userService.getIdSeller(userId);

        try {
            balanceRetorno = this.permissionZoop.deletePermission(id, permissionId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }
}
