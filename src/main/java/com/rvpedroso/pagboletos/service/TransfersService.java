package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.TransfersZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class TransfersService {


    private final TransfersZoop transfersZoop;
    private final UserService userService;

    public TransfersService(TransfersZoop transfersZoop, UserService userService) {
        this.transfersZoop = transfersZoop;
        this.userService = userService;
    }

    /**
     * Listar transferências por seller
     */
    public Map<String, Object> listTransfersSeller(String idSeller) {
        Map<String, Object> balanceRetorno;
        String id = userService.getIdSeller(idSeller);

        try {
            balanceRetorno = this.transfersZoop.listTransfersSeller(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Criar transferência para bancária
     */
    public Map<String, Object> createTransfersBank(String bankAccountId, Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.transfersZoop.createTransfersBank(bankAccountId, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Listar transferência por marketplace
     */
    public Map<String, Object> listTransfersMarketplaces() {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.transfersZoop.listTransfersMarketplaces();
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Recuperar detalhes de transferência
     */
    public Map<String, Object> get(String transferId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.transfersZoop.get(transferId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Listar transações associadas a transferência
     */
    public Map<String, Object> list(String transferId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.transfersZoop.list(transferId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Criar transferência P2P
     */
    public Map<String, Object> transfersP2P(String ownerId, String receiverId, Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.transfersZoop.transfersP2P(ownerId, receiverId, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

}
