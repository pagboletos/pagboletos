package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.SubscriptionsZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class SubscriptionsService {

    private final SubscriptionsZoop subscriptionsZoop;

    public SubscriptionsService(SubscriptionsZoop subscriptionsZoop) {
        this.subscriptionsZoop = subscriptionsZoop;
    }


    /**
     * Criar uma assinatura entre um comprador e um plano
     */
    public Map<String, Object> create(Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.subscriptionsZoop.create(body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Recuperar detalhes de assinatura de plano de venda
     */
    public Map<String, Object> getSubscription(String subscriptionId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.subscriptionsZoop.getSubscription(subscriptionId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Alterar os detalhes de uma assinatura pelo identificador
     */
    public Map<String, Object> update(String subscriptionId, Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.subscriptionsZoop.update(subscriptionId, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Alterar os detalhes de uma assinatura pelo identificador
     */
    public Map<String, Object> delete(String subscriptionId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.subscriptionsZoop.delete(subscriptionId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Suspender uma assinatura pelo identificador
     */
    public Map<String, Object> suspendSignature(String subscriptionId, Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.subscriptionsZoop.suspendSignature(subscriptionId, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Suspender uma assinatura pelo identificador
     */
    public Map<String, Object> reactivateSignature(String subscriptionId, Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.subscriptionsZoop.reactivateSignature(subscriptionId, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }
}
