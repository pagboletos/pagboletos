package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.BoletoZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class BoletoService {

    private final BoletoZoop boletoZoop;

    public BoletoService(BoletoZoop boletoZoop) {
        this.boletoZoop = boletoZoop;
    }


    /**
     * Enviar cobrança de boleto por email
     */
    public Map<String, Object> sendBillingEmail(String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.boletoZoop.sendBillingEmail(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Recuperar detalhes de boleto
     */
    public Map<String, Object> get(String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.boletoZoop.get(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    public Map<String, Object> cadastrar(Map<String, Object> boletoMap) {
        Map<String, Object> retorno = null;
        return retorno;
    }

    public Map<String, Object> buscar(Map<String, Object> boletoMap) {
        Map<String, Object> retorno = null;
        return retorno;
    }

    public Map<String, Object> listar(Map<String, Object> boletoMap) {
        Map<String, Object> retorno = null;
        return retorno;

    }

    public Map<String, Object> validarBoleto(Map<String, Object> boletoMap) {
        Map<String, Object> retorno = null;
        return retorno;

    }

    public Map<String, Object> cancelar(Map<String, Object> boletoMap) {
        Map<String, Object> retorno = null;
        return retorno;
    }

}
