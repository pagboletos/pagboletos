package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.ReceiptZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class ReceiptService {

    private final ReceiptZoop receiptZoop;


    public ReceiptService(ReceiptZoop receiptZoop) {
        this.receiptZoop = receiptZoop;
    }

    /**
     * Enviar recibo por email
     */
    public Map<String, Object> sendEmail(String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.receiptZoop.sendEmail(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Enviar recibo por SMS
     */
    public Map<String, Object> sendSMS(String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.receiptZoop.sendSMS(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Enviar recibo por sms.email
     */
    public Map<String, Object> sendSMSEmail(String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.receiptZoop.sendSMSEmail(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Recuperar detalhes do recibo
     */
    public Map<String, Object> get(String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.receiptZoop.get(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Alterar detalhes do recibo
     */
    public Map<String, Object> update(String id, Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.receiptZoop.update(id, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Renderizar template de recibo HTML
     */
    public Map<String, Object> render(String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.receiptZoop.render(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

}
