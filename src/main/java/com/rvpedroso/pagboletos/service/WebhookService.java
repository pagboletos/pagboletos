package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.WebhookZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class WebhookService {

    private final WebhookZoop webhookZoop;

    public WebhookService(WebhookZoop webhookZoop) {
        this.webhookZoop = webhookZoop;
    }

    /**
     * Cria webhook por marketplace
     */
    public Map<String, Object> create(Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.webhookZoop.create(body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Listar webhooks por marketplace
     */
    public Map<String, Object> list() {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.webhookZoop.list();
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Recuperar detalhes de webhook
     */
    public Map<String, Object> get(String webhookId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.webhookZoop.get(webhookId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Remover webhook
     */
    public Map<String, Object> delete(String webhookId) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.webhookZoop.delete(webhookId);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

}
