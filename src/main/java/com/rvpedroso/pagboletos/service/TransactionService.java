package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.service.zoop.TransactionZoop;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class TransactionService {

    private final UserService userService;
    private final TransactionZoop transactionZoop;

    public TransactionService(UserService userService, TransactionZoop transactionZoop) {
        this.userService = userService;
        this.transactionZoop = transactionZoop;
    }

    /**
     * Listar transaçoes do marketplace
     */
    public Map<String, Object> list() {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.transactionZoop.list();
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Criar transação Cartão Não Presente
     */
    public Map<String, Object> create(Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.transactionZoop.create(body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Listar documentos de um vendedor
     */
    public Map<String, Object> listTransactionSeller(String sellerId) {
        Map<String, Object> balanceRetorno;
        String id = userService.getIdSeller(sellerId);

        try {
            balanceRetorno = this.transactionZoop.listTransactionSeller(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Recuperar detalhes de transação pelo identificador
     */
    public Map<String, Object> get(String id) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.transactionZoop.get(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Alterar detalhes de transação pelo identificador
     */
    public Map<String, Object> update(String idTransaction, Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.transactionZoop.update(idTransaction, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Alterar detalhes de transação pelo identificador
     */
    public Map<String, Object> reverseTransaction(String idTransaction, Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.transactionZoop.reverseTransaction(idTransaction, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

    /**
     * Capturar transação cartão não presente
     */
    public Map<String, Object> capture(String idTransaction, Map<String, Object> body) {
        Map<String, Object> balanceRetorno;

        try {
            balanceRetorno = this.transactionZoop.capture(idTransaction, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return balanceRetorno;
    }

}
