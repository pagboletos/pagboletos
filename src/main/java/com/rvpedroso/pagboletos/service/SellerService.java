package com.rvpedroso.pagboletos.service;

import com.rvpedroso.pagboletos.domain.User;
import com.rvpedroso.pagboletos.repository.UserRepository;
import com.rvpedroso.pagboletos.service.pagboletos.SellerPagBoletos;
import com.rvpedroso.pagboletos.service.util.Criptografia;
import com.rvpedroso.pagboletos.service.zoop.SellerZoop;
import com.rvpedroso.pagboletos.web.rest.errors.EmailAlreadyUsedException;
import com.rvpedroso.pagboletos.web.rest.errors.InternalServerErrorException;
import com.rvpedroso.pagboletos.web.rest.errors.LoginAlreadyUsedException;
import com.rvpedroso.pagboletos.web.rest.errors.ZoopServerErrorException;
import com.rvpedroso.pagboletos.web.rest.model.Paginate;
import com.rvpedroso.pagboletos.zoop.exception.ValidationException;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service
 **/
@Service
public class SellerService {

    private final SellerZoop sellerZoop;
    private final SellerPagBoletos sellerPagBoletos;
    private final UserService userService;
    private final NotificacaoUsuario notificacaoUsuario;
    private final UserRepository userRepository;

    public SellerService(SellerZoop sellerZoop, SellerPagBoletos sellerPagBoletos, UserService userService,
                         NotificacaoUsuario notificacaoUsuario, UserRepository userRepository) {
        this.sellerZoop = sellerZoop;
        this.sellerPagBoletos = sellerPagBoletos;
        this.userService = userService;
        this.notificacaoUsuario = notificacaoUsuario;
        this.userRepository = userRepository;
    }


    /**
     * Criar novo vendedor do tipo indivíduo - Pessoa Física
     */
    public Map<String, Object> createIndividuals(Map<String, Object> body) {

        Map<String, Object> vendedorRetorno = null;

        try {

            verificarLoginExistente(body);

            vendedorRetorno = this.sellerZoop.createIndividuals(body);
            User user = this.sellerPagBoletos.createIndividuals(vendedorRetorno);

            this.notificacaoUsuario.cadastro(user.getEmail(), user.getLogin(), user.getFirstName());

        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        } catch (Exception e) {
            if (vendedorRetorno != null) {
                this.sellerZoop.delete(vendedorRetorno.get("id").toString());
            }
            throw new InternalServerErrorException(e.getMessage());
        }

        return vendedorRetorno;
    }

    /**
     * Criar novo vendedor do tipo indivíduo a partir do parceiro
     */
    public Map<String, Object> createIndividualsParceiro(Map<String, Object> body) {

        String id = userService.getIdSeller("*");
        Map<String, Object> vendedorRetorno = null;

        try {

            verificarLoginExistente(body);

            Map<String, Object> parceiro = get(id);
            vendedorRetorno = this.sellerZoop.createIndividuals(body);
            User user = this.sellerPagBoletos.createIndividuals(vendedorRetorno);

            this.notificacaoUsuario.cadastroPeloParceiro(parceiro.get("first_name").toString(), user.getEmail(),
                user.getLogin(), user.getFirstName(), Criptografia.decodificar(user.getSenha()));

        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        } catch (Exception e) {
            if (vendedorRetorno != null) {
                this.sellerZoop.delete(vendedorRetorno.get("id").toString());
            }
            throw new InternalServerErrorException(e.getMessage());
        }

        return vendedorRetorno;
    }

    /**
     * Alterar um vendedor do tipo indivíduo - Pessoa Física
     */
    public Map<String, Object> updateIndividuals(String idSeller, Map<String, Object> body) {
        Map<String, Object> vendedorRetorno;
        String id = userService.getIdSeller(idSeller);

        try {
            vendedorRetorno = this.sellerZoop.updateIndividuals(id, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return vendedorRetorno;
    }

    /**
     * Criar novo vendedor do tipo indivíduo - Pessoa Jurídica
     */
    public Map<String, Object> createBusinesses(Map<String, Object> body) {
        Map<String, Object> vendedorRetorno;

        try {
            verificarLoginExistente(body);
            vendedorRetorno = this.sellerZoop.createBusinesses(body);
            this.sellerPagBoletos.createBusinesses(vendedorRetorno);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return vendedorRetorno;
    }

    /**
     * Alterar um vendedor do tipo indivíduo - Pessoa Jurídica
     */
    public Map<String, Object> updateBusinesses(String idSeller, Map<String, Object> body) {
        Map<String, Object> vendedorRetorno;
        String id = userService.getIdSeller(idSeller);

        try {
            vendedorRetorno = this.sellerZoop.updateBusinesses(id, body);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }
        return vendedorRetorno;
    }

    /**
     * Recupera detalhes de vendedor pelo id
     */
    public Map<String, Object> get(String idSeller) {
        Map<String, Object> vendedorRetorno;
        String id = userService.getIdSeller(idSeller);

        try {
            vendedorRetorno = this.sellerZoop.get(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return vendedorRetorno;
    }

    /**
     * Deleta um de vendedor pelo id
     */
    public Map<String, Object> delete(String idSeller) {
        Map<String, Object> vendedorRetorno;
        String id = userService.getIdSeller(idSeller);
        try {
            vendedorRetorno = this.sellerZoop.delete(id);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return vendedorRetorno;
    }

    /**
     * Listando vendedores
     */
    public Map<String, Object> list(Paginate pageable) {
        Map<String, Object> vendedorRetorno;
        try {
            vendedorRetorno = this.sellerZoop.list(pageable);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return vendedorRetorno;
    }

    /**
     * Pesquisando vendedor pelo CPF
     */
    public Map<String, Object> searchIndividuals(String cpf) {
        Map<String, Object> vendedorRetorno;

        try {
            vendedorRetorno = this.sellerZoop.searchIndividuals(cpf);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return vendedorRetorno;
    }

    /**
     * Pesquisando vendedor pelo CNPJ
     */
    public Map<String, Object> search(String cnpj) {
        Map<String, Object> vendedorRetorno;

        try {
            vendedorRetorno = this.sellerZoop.searchBusinesses(cnpj);
        } catch (ValidationException e) {
            throw new ZoopServerErrorException(e);
        }

        return vendedorRetorno;
    }

    /**
     * Verifica se já existe um login e e-mail cadastrado no sistema da pagboletos
     */
    public void verificarLoginExistente(Map<String, Object> body) {

        userRepository.findOneByLogin(body.get("taxpayer_id").toString().toLowerCase()).ifPresent(existingUser -> {
            boolean removed = userService.removeNonActivatedUser(existingUser);
            if (!removed) {
                throw new LoginAlreadyUsedException();
            }
        });

        userRepository.findOneByEmailIgnoreCase(body.get("email").toString().toLowerCase()).ifPresent(existingUser -> {
            boolean removed = userService.removeNonActivatedUser(existingUser);
            if (!removed) {
                throw new EmailAlreadyUsedException();
            }
        });

    }

}
