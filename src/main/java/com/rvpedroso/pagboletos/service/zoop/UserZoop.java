package com.rvpedroso.pagboletos.service.zoop;

import com.rvpedroso.pagboletos.zoop.PagBoletos;
import com.rvpedroso.pagboletos.zoop.auth.Authentication;
import com.rvpedroso.pagboletos.zoop.auth.BasicAuth;
import com.rvpedroso.pagboletos.zoop.models.Setup;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service.zoop
 **/
@Service
public class UserZoop {

    /**
     * Criar novo usuário de API por marketplace
     */
    public Map<String, Object> create(Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.user().create(body, setup);
    }

    /**
     * Relizar login por usuário/senha
     */
    public Map<String, Object> login(Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.user().login(body, setup);
    }

    /**
     * Alterar detalhes de usuário
     */
    public Map<String, Object> update(Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.user().update(body, setup);
    }

    /**
     * Este método é usado para obter os dados de um usuário criado pelo usuário
     * PagBoletos external ID.
     */
    public Map<String, Object> get(String userId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.user().get(userId, setup);
    }

    /**
     * Remover usuário
     */
    public Map<String, Object> delete(String userId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.user().delete(userId, setup);
    }
}
