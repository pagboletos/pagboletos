package com.rvpedroso.pagboletos.service.zoop;

import com.rvpedroso.pagboletos.zoop.PagBoletos;
import com.rvpedroso.pagboletos.zoop.auth.Authentication;
import com.rvpedroso.pagboletos.zoop.auth.BasicAuth;
import com.rvpedroso.pagboletos.zoop.models.Setup;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service.zoop
 **/
@Service
public class PlansZoop {

    /**
     * Criar plano de vendas Planos com taxa e políticas para credito de
     * recebíveis
     */
    public Map<String, Object> create(Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.plans().create(body, setup);
    }

    /**
     * Listar plano de vendas por marketplace
     */
    public Map<String, Object> list() {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.plans().list(setup);
    }

    /**
     * Recuperar detalhes de plano de venda
     */
    public Map<String, Object> get(String planId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.plans().get(planId, setup);
    }

    /**
     * Remover plano de venda
     */
    public Map<String, Object> delete(String id) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.plans().delete(id, setup);
    }

    /**
     * Criar assinatura de plano de venda
     */
    public Map<String, Object> createSubscriptions(Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.plans().createSubscriptions(body, setup);
    }

    /**
     * Listar assinaturas de plano de venda
     */
    public Map<String, Object> listSubscriptions() {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.plans().listSubscriptions(setup);
    }

    /**
     * Recuperar detalhes de assinatura de plano de venda
     */
    public Map<String, Object> getSubscription(String subscriptionId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.plans().getSubscription(subscriptionId, setup);
    }

    /**
     * Remover plano de venda
     */
    public Map<String, Object> deleteSubscriptions(String id) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.plans().deleteSubscriptions(id, setup);
    }

    /**
     * Recuperar detalhes de assinatura de plano de venda
     */
    public Map<String, Object> getSubscriptionSeller(String sellerId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.plans().getSubscriptionSeller(sellerId, setup);
    }

    /**
     * Alterar plano pelo identificador Alterar detalhes de um plano existente
     * pelo identificador
     */
    public Map<String, Object> update(String id, Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.plans().update(id, body, setup);
    }

    /**
     * Deletar um plano pelo identificador
     */
    public Map<String, Object> deletePlans(String id) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.plans().deletePlans(id, setup);
    }

}
