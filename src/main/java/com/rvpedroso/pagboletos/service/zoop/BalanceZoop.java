package com.rvpedroso.pagboletos.service.zoop;

import com.rvpedroso.pagboletos.zoop.PagBoletos;
import com.rvpedroso.pagboletos.zoop.auth.Authentication;
import com.rvpedroso.pagboletos.zoop.auth.BasicAuth;
import com.rvpedroso.pagboletos.zoop.models.Setup;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service.zoop
 **/
@Service
public class BalanceZoop {


    /**
     * Recuperar saldo de conta por seller Recupera o saldo corrente e saldo
     * total de conta
     */
    public Map<String, Object> get(String sellerId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.balance().get(sellerId, setup);
    }

    /**
     * Lista contas por seller
     */
    public Map<String, Object> listBalanceSeller(String sellerId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.balance().listBalanceSeller(sellerId, setup);
    }

    /**
     * Lista contas por buyer
     */
    public Map<String, Object> listBalanceBuyer(String buyerId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.balance().listBalanceBuyer(buyerId, setup);
    }

    /**
     * Listar histórico de lançamentos pelo identificador da conta
     */
    public Map<String, Object> listHistory(String balanceId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.balance().listHistory(balanceId, setup);
    }

    /**
     * Listar histórico de lançamentos de conta por seller Listagem de histórico
     * da conta principal do seller
     */
    public Map<String, Object> listHistorySeller(String sellerId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.balance().listHistorySeller(sellerId, setup);
    }

    /**
     * Listar histórico de lançamentos de conta por buyer Listagem de histórico
     * da conta principal do buyer
     */
    public Map<String, Object> listHistoryBuyer(String buyerId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.balance().listHistoryBuyer(buyerId, setup);
    }

}
