package com.rvpedroso.pagboletos.service.zoop;

import com.rvpedroso.pagboletos.zoop.PagBoletos;
import com.rvpedroso.pagboletos.zoop.auth.Authentication;
import com.rvpedroso.pagboletos.zoop.auth.BasicAuth;
import com.rvpedroso.pagboletos.zoop.models.Setup;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service.zoop
 **/
@Service
public class SubscriptionsZoop {

    /**
     * Recuperar todas as assinatura de um marketplace
     */
    public Map<String, Object> list() {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.subscriptions().list(setup);
    }

    /**
     * Criar uma assinatura entre um comprador e um plano
     */
    public Map<String, Object> create(Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.subscriptions().create(body, setup);
    }

    /**
     * Recuperar detalhes de assinatura de plano de venda
     */
    public Map<String, Object> getSubscription(String subscriptionId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.subscriptions().getSubscription(subscriptionId, setup);
    }

    /**
     * Alterar os detalhes de uma assinatura pelo identificador
     */
    public Map<String, Object> update(String subscriptionId, Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.subscriptions().update(subscriptionId, body, setup);
    }

    /**
     * Alterar os detalhes de uma assinatura pelo identificador
     */
    public Map<String, Object> delete(String subscriptionId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.subscriptions().delete(subscriptionId, setup);
    }

    /**
     * Suspender uma assinatura pelo identificador
     */
    public Map<String, Object> suspendSignature(String subscriptionId, Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.subscriptions().suspendSignature(subscriptionId, body, setup);
    }

    /**
     * Suspender uma assinatura pelo identificador
     */
    public Map<String, Object> reactivateSignature(String subscriptionId, Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.subscriptions().reactivateSignature(subscriptionId, body, setup);
    }
}
