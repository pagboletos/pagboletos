package com.rvpedroso.pagboletos.service.zoop;

import com.rvpedroso.pagboletos.zoop.PagBoletos;
import com.rvpedroso.pagboletos.zoop.auth.Authentication;
import com.rvpedroso.pagboletos.zoop.auth.BasicAuth;
import com.rvpedroso.pagboletos.zoop.models.Setup;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service.zoop
 **/
@Service
public class ReceivablesZoop {

    /**
     * Recuperar detalhes de recebível
     */
    public Map<String, Object> get(String receivablesId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.receivables().get(receivablesId, setup);
    }

    /**
     * Listar recebíveis por transação Listar todas as parcelas de recebimento
     * por transação
     */
    public Map<String, Object> getTransactions(String receivablesId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.receivables().getTransactions(receivablesId, setup);
    }

    /**
     * Listar recebíveis por seller
     * Listar todas as parcelas de recebimento por seller
     */
    public Map<String, Object> getSeller(String sellerId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.receivables().getSeller(sellerId, setup);
    }

}
