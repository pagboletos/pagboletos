package com.rvpedroso.pagboletos.service.zoop;

import com.rvpedroso.pagboletos.zoop.PagBoletos;
import com.rvpedroso.pagboletos.zoop.auth.Authentication;
import com.rvpedroso.pagboletos.zoop.auth.BasicAuth;
import com.rvpedroso.pagboletos.zoop.models.Setup;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service.zoop
 **/
@Service
public class TransfersZoop {

    /**
     * Listar transferências por seller
     */
    public Map<String, Object> listTransfersSeller(String idSeller) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.transfers().listTransfersSeller(idSeller, setup);
    }

    /**
     * Criar transferência para bancária
     */
    public Map<String, Object> createTransfersBank(String bankAccountId, Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.transfers().createTransfersBank(bankAccountId, body, setup);
    }

    /**
     * Listar transferência por marketplace
     */
    public Map<String, Object> listTransfersMarketplaces() {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.transfers().listTransfersMarketplaces(setup);
    }

    /**
     * Recuperar detalhes de transferência
     */
    public Map<String, Object> get(String transferId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.transfers().get(transferId, setup);
    }

    /**
     * Cancelar transferência agendada anteriormente à data prevista para efetivaçã
     */
    public Map<String, Object> cancelarAgendada(String transferId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.transfers().cancelarAgendada(transferId, setup);
    }

    /**
     * Listar transações associadas a transferência
     */
    public Map<String, Object> list(String transferId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.transfers().list(transferId, setup);
    }

    /**
     * Criar transferência P2P
     */
    public Map<String, Object> transfersP2P(String ownerId, String receiverId, Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.transfers().transfersP2P(ownerId, receiverId, body, setup);
    }

}
