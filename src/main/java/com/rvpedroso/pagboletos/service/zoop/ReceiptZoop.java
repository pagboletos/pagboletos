package com.rvpedroso.pagboletos.service.zoop;

import com.rvpedroso.pagboletos.zoop.PagBoletos;
import com.rvpedroso.pagboletos.zoop.auth.Authentication;
import com.rvpedroso.pagboletos.zoop.auth.BasicAuth;
import com.rvpedroso.pagboletos.zoop.models.Setup;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service.zoop
 **/
@Service
public class ReceiptZoop {

    /**
     * Enviar recibo por email
     */
    public Map<String, Object> sendEmail(String id) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.receipe().sendEmail(id, setup);
    }

    /**
     * Enviar recibo por SMS
     */
    public Map<String, Object> sendSMS(String id) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.receipe().sendSMS(id, setup);
    }

    /**
     * Enviar recibo por sms.email
     */
    public Map<String, Object> sendSMSEmail(String id) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.receipe().sendSMSEmail(id, setup);
    }

    /**
     * Recuperar detalhes do recibo
     */
    public Map<String, Object> get(String id) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.receipe().get(id, setup);
    }

    /**
     * Alterar detalhes do recibo
     */
    public Map<String, Object> update(String id, Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.receipe().update(id, body, setup);
    }

    /**
     * Renderizar template de recibo HTML
     */
    public Map<String, Object> render(String id) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.receipe().render(id, setup);
    }

}
