package com.rvpedroso.pagboletos.service.zoop;

import com.rvpedroso.pagboletos.zoop.PagBoletos;
import com.rvpedroso.pagboletos.zoop.auth.Authentication;
import com.rvpedroso.pagboletos.zoop.auth.BasicAuth;
import com.rvpedroso.pagboletos.zoop.models.Setup;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service.zoop
 **/
@Service
public class InvoicesZoop {

    /**
     * Recuperar todas as faturas de um marketplace
     */
    public Map<String, Object> list() {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.invoices().list(setup);
    }

    /**
     * Criar uma fatura avulsa
     */
    public Map<String, Object> create(Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.invoices().create(body, setup);
    }

    /**
     * Recuperar os detalhes de uma fatura pelo identificador - Você pode
     * recuperar os detalhes de uma determinada fatura através do identificador
     */
    public Map<String, Object> get(String invoiceId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.invoices().get(invoiceId, setup);
    }

    /**
     * Alterar detalhes de uma fatura pelo identificador
     */
    public Map<String, Object> update(String invoiceId, Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.invoices().update(invoiceId, body, setup);
    }

    /**
     * Remover uma fatura pelo identificador
     */
    public Map<String, Object> delete(String invoiceId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.invoices().delete(invoiceId, setup);
    }

    /**
     * Aprovar fatura pendente - Aprovar manualmente uma fatura pendente, sem
     * que seja feita a cobrança ao cobrador
     */
    public Map<String, Object> approveInvoicePending(String invoiceId, Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.invoices().approveInvoicePending(invoiceId, body, setup);
    }

    /**
     * Estornar e reembolsar fatura - Estornar uma fatura paga, sendo feito o
     * processamento do estorno da forma de cobrança associada ao pagamento da
     * fatura
     */
    public Map<String, Object> reverseInvoicePending(String invoiceId, Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.invoices().reverseInvoicePending(invoiceId, body, setup);
    }

    /**
     * Recuperar faturas associadas a um vendedor pelo identificador - Recuperar
     * os detalhes de todas as faturas associadas a um determinado vendedor pelo
     * identificador
     */
    public Map<String, Object> getinvoicesSeller(String customerId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.invoices().getinvoicesSeller(customerId, setup);
    }

}
