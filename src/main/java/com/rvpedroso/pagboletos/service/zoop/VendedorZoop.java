package com.rvpedroso.pagboletos.service.zoop;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rvpedroso.pagboletos.service.UserService;
import com.rvpedroso.pagboletos.web.rest.errors.InternalServerErrorException;
import com.rvpedroso.pagboletos.zoop.PagBoletos;
import com.rvpedroso.pagboletos.zoop.auth.Authentication;
import com.rvpedroso.pagboletos.zoop.auth.BasicAuth;
import com.rvpedroso.pagboletos.zoop.exception.PagBoletosAPIException;
import com.rvpedroso.pagboletos.zoop.models.Setup;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.TreeMap;

/**
 * Usuário: william
 * Data: 04/04/19
 * Horário: 11
 * Projeto: pagboletos
 * Pacote: Pacote: com.rvpedroso.pagboletos.service.zoop
 **/

@Service
public class VendedorZoop {

    private final UserService userService;


    public VendedorZoop(UserService userService) {
        this.userService = userService;
    }

    public Map<String, Object> cadastrar(Map<String, Object> vendedor) {
        Map<String, Object> retorno;
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);

        retorno = PagBoletos.API.sellers().createIndividuals(vendedor, setup);
        return retorno;
    }

    public Map<String, Object> atualizar(Map<String, Object> vendedor) {
        Map<String, Object> retorno;
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);

        try {
            retorno = PagBoletos.API.sellers().updateIndividuals(this.userService.getUsuarioLogado(), vendedor, setup);

        } catch (PagBoletosAPIException e) {
            throw new InternalServerErrorException(e.getMessage());
        }

        return retorno;
    }

    public Map<String, Object> buscar() {
        Map<String, Object> retorno;
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);

        try {
            retorno = PagBoletos.API.sellers().get(this.userService.getUsuarioLogado(), setup);

        } catch (PagBoletosAPIException e) {
            throw new InternalServerErrorException(e.getMessage());
        }

        return retorno;
    }

    public Map<String, Object> deletar(String id) {
        Map<String, Object> retorno;
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);

        try {
            retorno = PagBoletos.API.sellers().delete(id, setup);

        } catch (PagBoletosAPIException e) {
            throw new InternalServerErrorException(e.getMessage());
        }

        return retorno;
    }


    /*  ##################################
                     EVENTOS
        ##################################
     */
    public Map<String, Object> notificarCredenciamento(Map<String, Object> evento) {
        ObjectMapper oMapper = new ObjectMapper();
        Map<String, Object> retorno = new TreeMap<>(evento);

        Map payload = oMapper.convertValue(evento.get("payload"), Map.class);
        Map objectMap = oMapper.convertValue(payload.get("object"), Map.class);

        String id = objectMap.get("id").toString();

        return objectMap;
    }

}
