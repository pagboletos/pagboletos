package com.rvpedroso.pagboletos.service.zoop;

import com.rvpedroso.pagboletos.zoop.PagBoletos;
import com.rvpedroso.pagboletos.zoop.auth.Authentication;
import com.rvpedroso.pagboletos.zoop.auth.BasicAuth;
import com.rvpedroso.pagboletos.zoop.models.Setup;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service.zoop
 **/
@Service
public class PermissionZoop {

    /**
     * Permissão do usuário Busca todas as permissões que usuário possui pelo
     * user_id
     */
    public Map<String, Object> getPermissionUser(String userId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.permission().getPermissionUser(userId, setup);
    }

    /**
     * Criar permissão para usuário Cria uma nova permissão para o usuário,
     * podendo vincular à marketplace, seller ou grupo
     */
    public Map<String, Object> createPermission(String userId, Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.permission().createPermission(userId, body, setup);
    }

    /**
     * Permissão do usuário por permission_id Permissões que usuário possui pelo
     * permission_id
     */
    public Map<String, Object> getPermissionId(String userId, String permissionId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.permission().getPermissionId(userId, permissionId, setup);
    }

    /**
     * Deleta uma permissão Remove uma permissões do usuário pelo permission_id
     */
    public Map<String, Object> deletePermission(String userId, String permissionId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.permission().deletePermission(userId, permissionId, setup);
    }
}
