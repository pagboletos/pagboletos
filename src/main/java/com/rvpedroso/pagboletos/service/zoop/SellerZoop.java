package com.rvpedroso.pagboletos.service.zoop;

import com.rvpedroso.pagboletos.web.rest.model.Paginate;
import com.rvpedroso.pagboletos.zoop.PagBoletos;
import com.rvpedroso.pagboletos.zoop.auth.Authentication;
import com.rvpedroso.pagboletos.zoop.auth.BasicAuth;
import com.rvpedroso.pagboletos.zoop.models.Setup;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service.zoop
 **/
@Service
public class SellerZoop {

    /**
     * Criar novo vendedor do tipo indivíduo - Pessoa Física
     */
    public Map<String, Object> createIndividuals(Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.sellers().createIndividuals(body, setup);
    }

    /**
     * Alterar um vendedor do tipo indivíduo - Pessoa Física
     */
    public Map<String, Object> updateIndividuals(String idSeller, Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.sellers().updateIndividuals(idSeller, body, setup);
    }

    /**
     * Criar novo vendedor do tipo indivíduo - Pessoa Jurídica
     */
    public Map<String, Object> createBusinesses(Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.sellers().createBusinesses(body, setup);
    }

    /**
     * Alterar um vendedor do tipo indivíduo - Pessoa Jurídica
     */
    public Map<String, Object> updateBusinesses(String idSeller, Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.sellers().updateBusinesses(idSeller, body, setup);
    }

    /**
     * Recupera detalhes de vendedor pelo id
     */
    public Map<String, Object> get(String id) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.sellers().get(id, setup);
    }

    /**
     * Deleta um de vendedor pelo id
     */
    public Map<String, Object> delete(String id) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.sellers().delete(id, setup);
    }

    /**
     * Listando vendedores
     */
    public Map<String, Object> list(Paginate pageable) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.sellers().list(setup, pageable);
    }

    /**
     * Pesquisando Vendedor pelo CPF
     */
    public Map<String, Object> searchIndividuals(String cpf) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.sellers().searchIndividuals(cpf, setup);
    }


    /**
     * Pesquisando Vendedor pelo CNPJ
     */
    public Map<String, Object> searchBusinesses(String cnpj) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.sellers().searchBusinesses(cnpj, setup);
    }
}
