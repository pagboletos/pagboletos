package com.rvpedroso.pagboletos.service.zoop;

import com.rvpedroso.pagboletos.zoop.PagBoletos;
import com.rvpedroso.pagboletos.zoop.auth.Authentication;
import com.rvpedroso.pagboletos.zoop.auth.BasicAuth;
import com.rvpedroso.pagboletos.zoop.models.Setup;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service.zoop
 **/
@Service
public class BankAccountZoop {

    /**
     * Recuperar detalhes de conta bancária por identificador
     */
    public Map<String, Object> get(String bankAccountiId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.bankAccount().get(bankAccountiId, setup);
    }

    /**
     * Alterar detalhes de conta bancária
     */
    public Map<String, Object> updateIndividuals(String bankAccountiId, Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.bankAccount().updateIndividuals(bankAccountiId, body, setup);
    }

    /**
     * Deleta conta bancária
     */
    public Map<String, Object> delete(String bankAccountiId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.bankAccount().delete(bankAccountiId, setup);
    }

    /**
     * Listar contas bancárias por marketplace
     */
    public Map<String, Object> list() {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.bankAccount().list(setup);
    }

    /**
     * Associar conta bancaria com customer Associar token de cartão com
     * comprador
     */
    public Map<String, Object> associate(Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.bankAccount().associate(setup, body);
    }

    /**
     * Listar contas bancárias por seller
     */
    public Map<String, Object> listSeller(String sellerId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.bankAccount().listSeller(sellerId, setup);
    }

}
