package com.rvpedroso.pagboletos.service.zoop;

import com.rvpedroso.pagboletos.zoop.PagBoletos;
import com.rvpedroso.pagboletos.zoop.auth.Authentication;
import com.rvpedroso.pagboletos.zoop.auth.BasicAuth;
import com.rvpedroso.pagboletos.zoop.models.Setup;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service.zoop
 **/
@Service
public class SplitRulesZoop {

    /**
     * Recuperar detalhes de regra de divisão por transação
     */
    public Map<String, Object> get(String id) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.splitRules().get(id, setup);
    }

    /**
     * Criar regra de divisão por transação
     */
    public Map<String, Object> create(Map<String, Object> body, String transactionId) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.splitRules().create(body, transactionId, setup);
    }

    /**
     * Recupera detalhes de regra de divisão por transação
     */
    public Map<String, Object> details(String transactionId, String id) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.splitRules().details(transactionId, id, setup);
    }

    /**
     * Alterar regra de divisão por transação
     */
    public Map<String, Object> update(Map<String, Object> body, String transactionId, String id) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.splitRules().update(body, transactionId, id, setup);
    }

    /**
     * Remover regra de divisão por transação
     */
    public Map<String, Object> delete(String transactionId, String id) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.splitRules().delete(transactionId, id, setup);
    }

}
