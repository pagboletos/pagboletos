package com.rvpedroso.pagboletos.service.zoop;

import com.rvpedroso.pagboletos.web.rest.model.Paginate;
import com.rvpedroso.pagboletos.zoop.PagBoletos;
import com.rvpedroso.pagboletos.zoop.api.request.RequestMaker;
import com.rvpedroso.pagboletos.zoop.auth.Authentication;
import com.rvpedroso.pagboletos.zoop.auth.BasicAuth;
import com.rvpedroso.pagboletos.zoop.models.Setup;
import org.apache.http.entity.ContentType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * Usuário: William Fernandes
 * Data: 09/04/19
 * Horário: 19:40
 * Projeto: pagboletos
 * Pacote: com.rvpedroso.pagboletos.service.zoop
 **/
@Service
public class DocumentsZoop {

    public static final String ENDPOINT = "sellers";
    public static final String DOCUMENTS = "documents";
    public static final ContentType CONTENT_TYPE = ContentType.APPLICATION_JSON;
    public static final ContentType CONTENT_TYPE_MULTIPART_FORM_DATA = ContentType.MULTIPART_FORM_DATA;
    private RequestMaker requestMaker;

    /**
     * Criar documento de cadastro de vendedor
     */
    public Map<String, Object> create(String sellerId, MultipartFile files, String tipo) {
        return PagBoletos.API.documents().create(sellerId, files, tipo);
    }

    /**
     * Listar documentos de um vendedor
     */
    public Map<String, Object> list(String sellerId, Paginate paginate) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.documents().list(sellerId, setup, paginate);
    }

    /**
     * Atualiza os dados de um documento de um vendedor
     */
    public Map<String, Object> update(String idDocuments, Map<String, Object> body) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.documents().update(idDocuments, body, setup);
    }

}
