package com.rvpedroso.pagboletos.service.zoop;

import com.rvpedroso.pagboletos.zoop.PagBoletos;
import com.rvpedroso.pagboletos.zoop.auth.Authentication;
import com.rvpedroso.pagboletos.zoop.auth.BasicAuth;
import com.rvpedroso.pagboletos.zoop.models.Setup;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Usuário: william
 * Data: 04/04/19
 * Horário: 11
 * Projeto: pagboletos
 * Pacote: Pacote: com.rvpedroso.pagboletos.service.zoop
 **/

@Service
public class BoletoZoop {

    /**
     * Enviar cobrança de boleto por email
     */
    public Map<String, Object> sendBillingEmail(String id) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.boleto().sendBillingEmail(id, setup);
    }

    /**
     * Recuperar detalhes de boleto
     */
    public Map<String, Object> get(String id) {
        Authentication auth = new BasicAuth(PagBoletos.API_KEY);
        Setup setup = new Setup().setAuthentication(auth).setEnvironment(
            Setup.Environment.PRODUCTION_V1);
        return PagBoletos.API.boleto().get(id, setup);
    }

}
