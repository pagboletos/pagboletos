package com.rvpedroso.pagboletos.repository;

import com.rvpedroso.pagboletos.domain.Parcelamento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Parcelamento entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ParcelamentoRepository extends JpaRepository<Parcelamento, Long> {

}
