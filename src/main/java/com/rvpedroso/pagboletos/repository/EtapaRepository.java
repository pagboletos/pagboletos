package com.rvpedroso.pagboletos.repository;

import com.rvpedroso.pagboletos.domain.Etapa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Etapa entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EtapaRepository extends JpaRepository<Etapa, Long> {

}
