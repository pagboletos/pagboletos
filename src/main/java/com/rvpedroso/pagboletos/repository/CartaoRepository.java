package com.rvpedroso.pagboletos.repository;

import com.rvpedroso.pagboletos.domain.Cartao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Cartao entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CartaoRepository extends JpaRepository<Cartao, Long> {

}
