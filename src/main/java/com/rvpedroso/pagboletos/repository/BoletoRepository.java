package com.rvpedroso.pagboletos.repository;

import com.rvpedroso.pagboletos.domain.Boleto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Boleto entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BoletoRepository extends JpaRepository<Boleto, Long> {

}
