import { ILocalizacao } from 'app/shared/model/localizacao.model';
import { IDocumentos } from 'app/shared/model/documentos.model';
import { ICartao } from 'app/shared/model/cartao.model';
import { IBoleto } from 'app/shared/model/boleto.model';

export interface IVendedor {
    id?: number;
    idVendedor?: string;
    selfieContentType?: string;
    selfie?: any;
    localizacao?: ILocalizacao;
    documentos?: IDocumentos[];
    cartaos?: ICartao[];
    boletos?: IBoleto[];
}

export class Vendedor implements IVendedor {
    constructor(
        public id?: number,
        public idVendedor?: string,
        public selfieContentType?: string,
        public selfie?: any,
        public localizacao?: ILocalizacao,
        public documentos?: IDocumentos[],
        public cartaos?: ICartao[],
        public boletos?: IBoleto[]
    ) {}
}
