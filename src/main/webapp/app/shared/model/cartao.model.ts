import { IVendedor } from 'app/shared/model/vendedor.model';

export interface ICartao {
    id?: number;
    idToken?: string;
    vendedor?: IVendedor;
}

export class Cartao implements ICartao {
    constructor(public id?: number, public idToken?: string, public vendedor?: IVendedor) {}
}
