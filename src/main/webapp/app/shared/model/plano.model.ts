import { IParcelamento } from 'app/shared/model/parcelamento.model';

export interface IPlano {
    id?: number;
    idPlano?: string;
    recebimento?: number;
    parcelamentos?: IParcelamento[];
}

export class Plano implements IPlano {
    constructor(public id?: number, public idPlano?: string, public recebimento?: number, public parcelamentos?: IParcelamento[]) {}
}
