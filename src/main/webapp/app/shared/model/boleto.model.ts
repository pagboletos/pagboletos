import { Moment } from 'moment';
import { IPagamento } from 'app/shared/model/pagamento.model';
import { IVendedor } from 'app/shared/model/vendedor.model';

export interface IBoleto {
    id?: number;
    idBoletos?: string;
    codigoDeBarras?: string;
    vencimento?: Moment;
    beneficiario?: string;
    descricao?: string;
    dataDePagamento?: Moment;
    pagamento?: IPagamento;
    vendedor?: IVendedor;
}

export class Boleto implements IBoleto {
    constructor(
        public id?: number,
        public idBoletos?: string,
        public codigoDeBarras?: string,
        public vencimento?: Moment,
        public beneficiario?: string,
        public descricao?: string,
        public dataDePagamento?: Moment,
        public pagamento?: IPagamento,
        public vendedor?: IVendedor
    ) {}
}
