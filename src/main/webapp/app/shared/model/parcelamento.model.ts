import { IPlano } from 'app/shared/model/plano.model';

export interface IParcelamento {
    id?: number;
    idParcelamento?: string;
    jurosCliente?: number;
    jurosPagBoletos?: number;
    plano?: IPlano;
}

export class Parcelamento implements IParcelamento {
    constructor(
        public id?: number,
        public idParcelamento?: string,
        public jurosCliente?: number,
        public jurosPagBoletos?: number,
        public plano?: IPlano
    ) {}
}
