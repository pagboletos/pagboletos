import { IEtapa } from 'app/shared/model/etapa.model';
import { IBoleto } from 'app/shared/model/boleto.model';

export interface IPagamento {
    id?: number;
    idTransacaoCartao?: string;
    idTransacaoBoleto?: string;
    etapas?: IEtapa[];
    vendedor?: IBoleto;
}

export class Pagamento implements IPagamento {
    constructor(
        public id?: number,
        public idTransacaoCartao?: string,
        public idTransacaoBoleto?: string,
        public etapas?: IEtapa[],
        public vendedor?: IBoleto
    ) {}
}
