import { Moment } from 'moment';
import { IPagamento } from 'app/shared/model/pagamento.model';

export interface IEtapa {
    id?: number;
    nome?: string;
    descricao?: string;
    dataEfetuacao?: Moment;
    finalizado?: boolean;
    pagamento?: IPagamento;
}

export class Etapa implements IEtapa {
    constructor(
        public id?: number,
        public nome?: string,
        public descricao?: string,
        public dataEfetuacao?: Moment,
        public finalizado?: boolean,
        public pagamento?: IPagamento
    ) {
        this.finalizado = this.finalizado || false;
    }
}
