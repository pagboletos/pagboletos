import { IVendedor } from 'app/shared/model/vendedor.model';

export interface ILocalizacao {
    id?: number;
    latitude?: number;
    longitude?: number;
    vendedor?: IVendedor;
}

export class Localizacao implements ILocalizacao {
    constructor(public id?: number, public latitude?: number, public longitude?: number, public vendedor?: IVendedor) {}
}
