import { IVendedor } from 'app/shared/model/vendedor.model';

export const enum CategoriaDocumento {
    ATIVIDADE = 'ATIVIDADE',
    RESIDENCIA = 'RESIDENCIA',
    IDENTIFICACAO = 'IDENTIFICACAO'
}

export interface IDocumentos {
    id?: number;
    idDocumento?: string;
    nome?: string;
    categoria?: CategoriaDocumento;
    imagemContentType?: string;
    imagem?: any;
    status?: boolean;
    vendedor?: IVendedor;
}

export class Documentos implements IDocumentos {
    constructor(
        public id?: number,
        public idDocumento?: string,
        public nome?: string,
        public categoria?: CategoriaDocumento,
        public imagemContentType?: string,
        public imagem?: any,
        public status?: boolean,
        public vendedor?: IVendedor
    ) {
        this.status = this.status || false;
    }
}
