import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'vendedor',
                loadChildren: './vendedor/vendedor.module#PagboletosVendedorModule'
            },
            {
                path: 'documentos',
                loadChildren: './documentos/documentos.module#PagboletosDocumentosModule'
            },
            {
                path: 'cartao',
                loadChildren: './cartao/cartao.module#PagboletosCartaoModule'
            },
            {
                path: 'localizacao',
                loadChildren: './localizacao/localizacao.module#PagboletosLocalizacaoModule'
            },
            {
                path: 'boleto',
                loadChildren: './boleto/boleto.module#PagboletosBoletoModule'
            },
            {
                path: 'pagamento',
                loadChildren: './pagamento/pagamento.module#PagboletosPagamentoModule'
            },
            {
                path: 'etapa',
                loadChildren: './etapa/etapa.module#PagboletosEtapaModule'
            },
            {
                path: 'plano',
                loadChildren: './plano/plano.module#PagboletosPlanoModule'
            },
            {
                path: 'parcelamento',
                loadChildren: './parcelamento/parcelamento.module#PagboletosParcelamentoModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PagboletosEntityModule {}
