import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IPagamento } from 'app/shared/model/pagamento.model';
import { PagamentoService } from './pagamento.service';
import { IBoleto } from 'app/shared/model/boleto.model';
import { BoletoService } from 'app/entities/boleto';

@Component({
    selector: 'jhi-pagamento-update',
    templateUrl: './pagamento-update.component.html'
})
export class PagamentoUpdateComponent implements OnInit {
    pagamento: IPagamento;
    isSaving: boolean;

    boletos: IBoleto[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected pagamentoService: PagamentoService,
        protected boletoService: BoletoService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ pagamento }) => {
            this.pagamento = pagamento;
        });
        this.boletoService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IBoleto[]>) => mayBeOk.ok),
                map((response: HttpResponse<IBoleto[]>) => response.body)
            )
            .subscribe((res: IBoleto[]) => (this.boletos = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.pagamento.id !== undefined) {
            this.subscribeToSaveResponse(this.pagamentoService.update(this.pagamento));
        } else {
            this.subscribeToSaveResponse(this.pagamentoService.create(this.pagamento));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IPagamento>>) {
        result.subscribe((res: HttpResponse<IPagamento>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackBoletoById(index: number, item: IBoleto) {
        return item.id;
    }
}
