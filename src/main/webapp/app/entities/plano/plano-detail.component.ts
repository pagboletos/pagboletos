import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPlano } from 'app/shared/model/plano.model';

@Component({
    selector: 'jhi-plano-detail',
    templateUrl: './plano-detail.component.html'
})
export class PlanoDetailComponent implements OnInit {
    plano: IPlano;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ plano }) => {
            this.plano = plano;
        });
    }

    previousState() {
        window.history.back();
    }
}
