import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PagboletosSharedModule } from 'app/shared';
import {
    PlanoComponent,
    PlanoDeleteDialogComponent,
    PlanoDeletePopupComponent,
    PlanoDetailComponent,
    planoPopupRoute,
    planoRoute,
    PlanoUpdateComponent
} from './';

const ENTITY_STATES = [...planoRoute, ...planoPopupRoute];

@NgModule({
    imports: [PagboletosSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [PlanoComponent, PlanoDetailComponent, PlanoUpdateComponent, PlanoDeleteDialogComponent, PlanoDeletePopupComponent],
    entryComponents: [PlanoComponent, PlanoUpdateComponent, PlanoDeleteDialogComponent, PlanoDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PagboletosPlanoModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
