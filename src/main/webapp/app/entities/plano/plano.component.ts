import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { IPlano } from 'app/shared/model/plano.model';
import { AccountService } from 'app/core';
import { PlanoService } from './plano.service';

@Component({
    selector: 'jhi-plano',
    templateUrl: './plano.component.html'
})
export class PlanoComponent implements OnInit, OnDestroy {
    planos: IPlano[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected planoService: PlanoService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.planoService
            .query()
            .pipe(
                filter((res: HttpResponse<IPlano[]>) => res.ok),
                map((res: HttpResponse<IPlano[]>) => res.body)
            )
            .subscribe(
                (res: IPlano[]) => {
                    this.planos = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInPlanos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IPlano) {
        return item.id;
    }

    registerChangeInPlanos() {
        this.eventSubscriber = this.eventManager.subscribe('planoListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
