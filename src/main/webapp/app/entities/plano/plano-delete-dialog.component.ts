import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPlano } from 'app/shared/model/plano.model';
import { PlanoService } from './plano.service';

@Component({
    selector: 'jhi-plano-delete-dialog',
    templateUrl: './plano-delete-dialog.component.html'
})
export class PlanoDeleteDialogComponent {
    plano: IPlano;

    constructor(protected planoService: PlanoService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.planoService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'planoListModification',
                content: 'Deleted an plano'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-plano-delete-popup',
    template: ''
})
export class PlanoDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ plano }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(PlanoDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.plano = plano;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/plano', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/plano', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
