import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IPlano, Plano } from 'app/shared/model/plano.model';
import { PlanoService } from './plano.service';
import { PlanoComponent } from './plano.component';
import { PlanoDetailComponent } from './plano-detail.component';
import { PlanoUpdateComponent } from './plano-update.component';
import { PlanoDeletePopupComponent } from './plano-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class PlanoResolve implements Resolve<IPlano> {
    constructor(private service: PlanoService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPlano> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Plano>) => response.ok),
                map((plano: HttpResponse<Plano>) => plano.body)
            );
        }
        return of(new Plano());
    }
}

export const planoRoute: Routes = [
    {
        path: '',
        component: PlanoComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.plano.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: PlanoDetailComponent,
        resolve: {
            plano: PlanoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.plano.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: PlanoUpdateComponent,
        resolve: {
            plano: PlanoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.plano.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: PlanoUpdateComponent,
        resolve: {
            plano: PlanoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.plano.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const planoPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: PlanoDeletePopupComponent,
        resolve: {
            plano: PlanoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.plano.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
