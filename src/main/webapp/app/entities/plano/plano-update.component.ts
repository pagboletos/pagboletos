import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IPlano } from 'app/shared/model/plano.model';
import { PlanoService } from './plano.service';

@Component({
    selector: 'jhi-plano-update',
    templateUrl: './plano-update.component.html'
})
export class PlanoUpdateComponent implements OnInit {
    plano: IPlano;
    isSaving: boolean;

    constructor(protected planoService: PlanoService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ plano }) => {
            this.plano = plano;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.plano.id !== undefined) {
            this.subscribeToSaveResponse(this.planoService.update(this.plano));
        } else {
            this.subscribeToSaveResponse(this.planoService.create(this.plano));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IPlano>>) {
        result.subscribe((res: HttpResponse<IPlano>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
