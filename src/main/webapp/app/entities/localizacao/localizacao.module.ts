import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PagboletosSharedModule } from 'app/shared';
import {
    LocalizacaoComponent,
    LocalizacaoDeleteDialogComponent,
    LocalizacaoDeletePopupComponent,
    LocalizacaoDetailComponent,
    localizacaoPopupRoute,
    localizacaoRoute,
    LocalizacaoUpdateComponent
} from './';

const ENTITY_STATES = [...localizacaoRoute, ...localizacaoPopupRoute];

@NgModule({
    imports: [PagboletosSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        LocalizacaoComponent,
        LocalizacaoDetailComponent,
        LocalizacaoUpdateComponent,
        LocalizacaoDeleteDialogComponent,
        LocalizacaoDeletePopupComponent
    ],
    entryComponents: [LocalizacaoComponent, LocalizacaoUpdateComponent, LocalizacaoDeleteDialogComponent, LocalizacaoDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PagboletosLocalizacaoModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
