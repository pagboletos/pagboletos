import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ILocalizacao } from 'app/shared/model/localizacao.model';
import { LocalizacaoService } from './localizacao.service';
import { IVendedor } from 'app/shared/model/vendedor.model';
import { VendedorService } from 'app/entities/vendedor';

@Component({
    selector: 'jhi-localizacao-update',
    templateUrl: './localizacao-update.component.html'
})
export class LocalizacaoUpdateComponent implements OnInit {
    localizacao: ILocalizacao;
    isSaving: boolean;

    vendedors: IVendedor[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected localizacaoService: LocalizacaoService,
        protected vendedorService: VendedorService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ localizacao }) => {
            this.localizacao = localizacao;
        });
        this.vendedorService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IVendedor[]>) => mayBeOk.ok),
                map((response: HttpResponse<IVendedor[]>) => response.body)
            )
            .subscribe((res: IVendedor[]) => (this.vendedors = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.localizacao.id !== undefined) {
            this.subscribeToSaveResponse(this.localizacaoService.update(this.localizacao));
        } else {
            this.subscribeToSaveResponse(this.localizacaoService.create(this.localizacao));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ILocalizacao>>) {
        result.subscribe((res: HttpResponse<ILocalizacao>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackVendedorById(index: number, item: IVendedor) {
        return item.id;
    }
}
