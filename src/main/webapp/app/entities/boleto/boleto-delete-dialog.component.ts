import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBoleto } from 'app/shared/model/boleto.model';
import { BoletoService } from './boleto.service';

@Component({
    selector: 'jhi-boleto-delete-dialog',
    templateUrl: './boleto-delete-dialog.component.html'
})
export class BoletoDeleteDialogComponent {
    boleto: IBoleto;

    constructor(protected boletoService: BoletoService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.boletoService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'boletoListModification',
                content: 'Deleted an boleto'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-boleto-delete-popup',
    template: ''
})
export class BoletoDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ boleto }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(BoletoDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.boleto = boleto;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/boleto', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/boleto', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
