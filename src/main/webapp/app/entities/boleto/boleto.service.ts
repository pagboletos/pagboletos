import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBoleto } from 'app/shared/model/boleto.model';

type EntityResponseType = HttpResponse<IBoleto>;
type EntityArrayResponseType = HttpResponse<IBoleto[]>;

@Injectable({ providedIn: 'root' })
export class BoletoService {
    public resourceUrl = SERVER_API_URL + 'api/boletos';

    constructor(protected http: HttpClient) {}

    create(boleto: IBoleto): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(boleto);
        return this.http
            .post<IBoleto>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(boleto: IBoleto): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(boleto);
        return this.http
            .put<IBoleto>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IBoleto>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IBoleto[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(boleto: IBoleto): IBoleto {
        const copy: IBoleto = Object.assign({}, boleto, {
            vencimento: boleto.vencimento != null && boleto.vencimento.isValid() ? boleto.vencimento.format(DATE_FORMAT) : null,
            dataDePagamento:
                boleto.dataDePagamento != null && boleto.dataDePagamento.isValid() ? boleto.dataDePagamento.format(DATE_FORMAT) : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.vencimento = res.body.vencimento != null ? moment(res.body.vencimento) : null;
            res.body.dataDePagamento = res.body.dataDePagamento != null ? moment(res.body.dataDePagamento) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((boleto: IBoleto) => {
                boleto.vencimento = boleto.vencimento != null ? moment(boleto.vencimento) : null;
                boleto.dataDePagamento = boleto.dataDePagamento != null ? moment(boleto.dataDePagamento) : null;
            });
        }
        return res;
    }
}
