import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PagboletosSharedModule } from 'app/shared';
import {
    BoletoComponent,
    BoletoDeleteDialogComponent,
    BoletoDeletePopupComponent,
    BoletoDetailComponent,
    boletoPopupRoute,
    boletoRoute,
    BoletoUpdateComponent
} from './';

const ENTITY_STATES = [...boletoRoute, ...boletoPopupRoute];

@NgModule({
    imports: [PagboletosSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [BoletoComponent, BoletoDetailComponent, BoletoUpdateComponent, BoletoDeleteDialogComponent, BoletoDeletePopupComponent],
    entryComponents: [BoletoComponent, BoletoUpdateComponent, BoletoDeleteDialogComponent, BoletoDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PagboletosBoletoModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
