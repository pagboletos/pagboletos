import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { IBoleto } from 'app/shared/model/boleto.model';
import { AccountService } from 'app/core';
import { BoletoService } from './boleto.service';

@Component({
    selector: 'jhi-boleto',
    templateUrl: './boleto.component.html'
})
export class BoletoComponent implements OnInit, OnDestroy {
    boletos: IBoleto[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected boletoService: BoletoService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.boletoService
            .query()
            .pipe(
                filter((res: HttpResponse<IBoleto[]>) => res.ok),
                map((res: HttpResponse<IBoleto[]>) => res.body)
            )
            .subscribe(
                (res: IBoleto[]) => {
                    this.boletos = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInBoletos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IBoleto) {
        return item.id;
    }

    registerChangeInBoletos() {
        this.eventSubscriber = this.eventManager.subscribe('boletoListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
