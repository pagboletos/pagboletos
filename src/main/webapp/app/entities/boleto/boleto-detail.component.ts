import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBoleto } from 'app/shared/model/boleto.model';

@Component({
    selector: 'jhi-boleto-detail',
    templateUrl: './boleto-detail.component.html'
})
export class BoletoDetailComponent implements OnInit {
    boleto: IBoleto;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ boleto }) => {
            this.boleto = boleto;
        });
    }

    previousState() {
        window.history.back();
    }
}
