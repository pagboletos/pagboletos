import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Boleto, IBoleto } from 'app/shared/model/boleto.model';
import { BoletoService } from './boleto.service';
import { BoletoComponent } from './boleto.component';
import { BoletoDetailComponent } from './boleto-detail.component';
import { BoletoUpdateComponent } from './boleto-update.component';
import { BoletoDeletePopupComponent } from './boleto-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class BoletoResolve implements Resolve<IBoleto> {
    constructor(private service: BoletoService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBoleto> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Boleto>) => response.ok),
                map((boleto: HttpResponse<Boleto>) => boleto.body)
            );
        }
        return of(new Boleto());
    }
}

export const boletoRoute: Routes = [
    {
        path: '',
        component: BoletoComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.boleto.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: BoletoDetailComponent,
        resolve: {
            boleto: BoletoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.boleto.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: BoletoUpdateComponent,
        resolve: {
            boleto: BoletoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.boleto.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: BoletoUpdateComponent,
        resolve: {
            boleto: BoletoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.boleto.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const boletoPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: BoletoDeletePopupComponent,
        resolve: {
            boleto: BoletoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.boleto.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
