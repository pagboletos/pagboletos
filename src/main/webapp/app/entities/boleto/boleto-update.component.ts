import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IBoleto } from 'app/shared/model/boleto.model';
import { BoletoService } from './boleto.service';
import { IPagamento } from 'app/shared/model/pagamento.model';
import { PagamentoService } from 'app/entities/pagamento';
import { IVendedor } from 'app/shared/model/vendedor.model';
import { VendedorService } from 'app/entities/vendedor';

@Component({
    selector: 'jhi-boleto-update',
    templateUrl: './boleto-update.component.html'
})
export class BoletoUpdateComponent implements OnInit {
    boleto: IBoleto;
    isSaving: boolean;

    pagamentos: IPagamento[];

    vendedors: IVendedor[];
    vencimentoDp: any;
    dataDePagamentoDp: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected boletoService: BoletoService,
        protected pagamentoService: PagamentoService,
        protected vendedorService: VendedorService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ boleto }) => {
            this.boleto = boleto;
        });
        this.pagamentoService
            .query({ filter: 'vendedor-is-null' })
            .pipe(
                filter((mayBeOk: HttpResponse<IPagamento[]>) => mayBeOk.ok),
                map((response: HttpResponse<IPagamento[]>) => response.body)
            )
            .subscribe(
                (res: IPagamento[]) => {
                    if (!this.boleto.pagamento || !this.boleto.pagamento.id) {
                        this.pagamentos = res;
                    } else {
                        this.pagamentoService
                            .find(this.boleto.pagamento.id)
                            .pipe(
                                filter((subResMayBeOk: HttpResponse<IPagamento>) => subResMayBeOk.ok),
                                map((subResponse: HttpResponse<IPagamento>) => subResponse.body)
                            )
                            .subscribe(
                                (subRes: IPagamento) => (this.pagamentos = [subRes].concat(res)),
                                (subRes: HttpErrorResponse) => this.onError(subRes.message)
                            );
                    }
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        this.vendedorService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IVendedor[]>) => mayBeOk.ok),
                map((response: HttpResponse<IVendedor[]>) => response.body)
            )
            .subscribe((res: IVendedor[]) => (this.vendedors = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.boleto.id !== undefined) {
            this.subscribeToSaveResponse(this.boletoService.update(this.boleto));
        } else {
            this.subscribeToSaveResponse(this.boletoService.create(this.boleto));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IBoleto>>) {
        result.subscribe((res: HttpResponse<IBoleto>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackPagamentoById(index: number, item: IPagamento) {
        return item.id;
    }

    trackVendedorById(index: number, item: IVendedor) {
        return item.id;
    }
}
