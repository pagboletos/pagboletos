import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { IVendedor } from 'app/shared/model/vendedor.model';
import { VendedorService } from './vendedor.service';
import { ILocalizacao } from 'app/shared/model/localizacao.model';
import { LocalizacaoService } from 'app/entities/localizacao';

@Component({
    selector: 'jhi-vendedor-update',
    templateUrl: './vendedor-update.component.html'
})
export class VendedorUpdateComponent implements OnInit {
    vendedor: IVendedor;
    isSaving: boolean;

    localizacaos: ILocalizacao[];

    constructor(
        protected dataUtils: JhiDataUtils,
        protected jhiAlertService: JhiAlertService,
        protected vendedorService: VendedorService,
        protected localizacaoService: LocalizacaoService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ vendedor }) => {
            this.vendedor = vendedor;
        });
        this.localizacaoService
            .query({ filter: 'vendedor-is-null' })
            .pipe(
                filter((mayBeOk: HttpResponse<ILocalizacao[]>) => mayBeOk.ok),
                map((response: HttpResponse<ILocalizacao[]>) => response.body)
            )
            .subscribe(
                (res: ILocalizacao[]) => {
                    if (!this.vendedor.localizacao || !this.vendedor.localizacao.id) {
                        this.localizacaos = res;
                    } else {
                        this.localizacaoService
                            .find(this.vendedor.localizacao.id)
                            .pipe(
                                filter((subResMayBeOk: HttpResponse<ILocalizacao>) => subResMayBeOk.ok),
                                map((subResponse: HttpResponse<ILocalizacao>) => subResponse.body)
                            )
                            .subscribe(
                                (subRes: ILocalizacao) => (this.localizacaos = [subRes].concat(res)),
                                (subRes: HttpErrorResponse) => this.onError(subRes.message)
                            );
                    }
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.vendedor.id !== undefined) {
            this.subscribeToSaveResponse(this.vendedorService.update(this.vendedor));
        } else {
            this.subscribeToSaveResponse(this.vendedorService.create(this.vendedor));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IVendedor>>) {
        result.subscribe((res: HttpResponse<IVendedor>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackLocalizacaoById(index: number, item: ILocalizacao) {
        return item.id;
    }
}
