import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PagboletosSharedModule } from 'app/shared';
import {
    VendedorComponent,
    VendedorDeleteDialogComponent,
    VendedorDeletePopupComponent,
    VendedorDetailComponent,
    vendedorPopupRoute,
    vendedorRoute,
    VendedorUpdateComponent
} from './';

const ENTITY_STATES = [...vendedorRoute, ...vendedorPopupRoute];

@NgModule({
    imports: [PagboletosSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        VendedorComponent,
        VendedorDetailComponent,
        VendedorUpdateComponent,
        VendedorDeleteDialogComponent,
        VendedorDeletePopupComponent
    ],
    entryComponents: [VendedorComponent, VendedorUpdateComponent, VendedorDeleteDialogComponent, VendedorDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PagboletosVendedorModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
