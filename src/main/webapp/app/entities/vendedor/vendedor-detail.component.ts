import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IVendedor } from 'app/shared/model/vendedor.model';

@Component({
    selector: 'jhi-vendedor-detail',
    templateUrl: './vendedor-detail.component.html'
})
export class VendedorDetailComponent implements OnInit {
    vendedor: IVendedor;

    constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ vendedor }) => {
            this.vendedor = vendedor;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }
}
