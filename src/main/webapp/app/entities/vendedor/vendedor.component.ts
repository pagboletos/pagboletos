import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiDataUtils, JhiEventManager } from 'ng-jhipster';

import { IVendedor } from 'app/shared/model/vendedor.model';
import { AccountService } from 'app/core';
import { VendedorService } from './vendedor.service';

@Component({
    selector: 'jhi-vendedor',
    templateUrl: './vendedor.component.html'
})
export class VendedorComponent implements OnInit, OnDestroy {
    vendedors: IVendedor[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected vendedorService: VendedorService,
        protected jhiAlertService: JhiAlertService,
        protected dataUtils: JhiDataUtils,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.vendedorService
            .query()
            .pipe(
                filter((res: HttpResponse<IVendedor[]>) => res.ok),
                map((res: HttpResponse<IVendedor[]>) => res.body)
            )
            .subscribe(
                (res: IVendedor[]) => {
                    this.vendedors = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInVendedors();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IVendedor) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    registerChangeInVendedors() {
        this.eventSubscriber = this.eventManager.subscribe('vendedorListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
