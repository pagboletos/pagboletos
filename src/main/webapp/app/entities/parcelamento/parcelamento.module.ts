import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PagboletosSharedModule } from 'app/shared';
import {
    ParcelamentoComponent,
    ParcelamentoDeleteDialogComponent,
    ParcelamentoDeletePopupComponent,
    ParcelamentoDetailComponent,
    parcelamentoPopupRoute,
    parcelamentoRoute,
    ParcelamentoUpdateComponent
} from './';

const ENTITY_STATES = [...parcelamentoRoute, ...parcelamentoPopupRoute];

@NgModule({
    imports: [PagboletosSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ParcelamentoComponent,
        ParcelamentoDetailComponent,
        ParcelamentoUpdateComponent,
        ParcelamentoDeleteDialogComponent,
        ParcelamentoDeletePopupComponent
    ],
    entryComponents: [
        ParcelamentoComponent,
        ParcelamentoUpdateComponent,
        ParcelamentoDeleteDialogComponent,
        ParcelamentoDeletePopupComponent
    ],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PagboletosParcelamentoModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
