import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { IParcelamento } from 'app/shared/model/parcelamento.model';
import { AccountService } from 'app/core';
import { ParcelamentoService } from './parcelamento.service';

@Component({
    selector: 'jhi-parcelamento',
    templateUrl: './parcelamento.component.html'
})
export class ParcelamentoComponent implements OnInit, OnDestroy {
    parcelamentos: IParcelamento[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected parcelamentoService: ParcelamentoService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.parcelamentoService
            .query()
            .pipe(
                filter((res: HttpResponse<IParcelamento[]>) => res.ok),
                map((res: HttpResponse<IParcelamento[]>) => res.body)
            )
            .subscribe(
                (res: IParcelamento[]) => {
                    this.parcelamentos = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInParcelamentos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IParcelamento) {
        return item.id;
    }

    registerChangeInParcelamentos() {
        this.eventSubscriber = this.eventManager.subscribe('parcelamentoListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
