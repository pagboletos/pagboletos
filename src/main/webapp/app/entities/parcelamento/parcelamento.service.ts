import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IParcelamento } from 'app/shared/model/parcelamento.model';

type EntityResponseType = HttpResponse<IParcelamento>;
type EntityArrayResponseType = HttpResponse<IParcelamento[]>;

@Injectable({ providedIn: 'root' })
export class ParcelamentoService {
    public resourceUrl = SERVER_API_URL + 'api/parcelamentos';

    constructor(protected http: HttpClient) {}

    create(parcelamento: IParcelamento): Observable<EntityResponseType> {
        return this.http.post<IParcelamento>(this.resourceUrl, parcelamento, { observe: 'response' });
    }

    update(parcelamento: IParcelamento): Observable<EntityResponseType> {
        return this.http.put<IParcelamento>(this.resourceUrl, parcelamento, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IParcelamento>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IParcelamento[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
