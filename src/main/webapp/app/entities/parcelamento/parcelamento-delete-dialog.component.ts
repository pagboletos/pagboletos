import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IParcelamento } from 'app/shared/model/parcelamento.model';
import { ParcelamentoService } from './parcelamento.service';

@Component({
    selector: 'jhi-parcelamento-delete-dialog',
    templateUrl: './parcelamento-delete-dialog.component.html'
})
export class ParcelamentoDeleteDialogComponent {
    parcelamento: IParcelamento;

    constructor(
        protected parcelamentoService: ParcelamentoService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.parcelamentoService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'parcelamentoListModification',
                content: 'Deleted an parcelamento'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-parcelamento-delete-popup',
    template: ''
})
export class ParcelamentoDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ parcelamento }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ParcelamentoDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.parcelamento = parcelamento;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/parcelamento', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/parcelamento', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
