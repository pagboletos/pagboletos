export * from './parcelamento.service';
export * from './parcelamento-update.component';
export * from './parcelamento-delete-dialog.component';
export * from './parcelamento-detail.component';
export * from './parcelamento.component';
export * from './parcelamento.route';
