import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IParcelamento, Parcelamento } from 'app/shared/model/parcelamento.model';
import { ParcelamentoService } from './parcelamento.service';
import { ParcelamentoComponent } from './parcelamento.component';
import { ParcelamentoDetailComponent } from './parcelamento-detail.component';
import { ParcelamentoUpdateComponent } from './parcelamento-update.component';
import { ParcelamentoDeletePopupComponent } from './parcelamento-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class ParcelamentoResolve implements Resolve<IParcelamento> {
    constructor(private service: ParcelamentoService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IParcelamento> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Parcelamento>) => response.ok),
                map((parcelamento: HttpResponse<Parcelamento>) => parcelamento.body)
            );
        }
        return of(new Parcelamento());
    }
}

export const parcelamentoRoute: Routes = [
    {
        path: '',
        component: ParcelamentoComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.parcelamento.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: ParcelamentoDetailComponent,
        resolve: {
            parcelamento: ParcelamentoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.parcelamento.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: ParcelamentoUpdateComponent,
        resolve: {
            parcelamento: ParcelamentoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.parcelamento.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: ParcelamentoUpdateComponent,
        resolve: {
            parcelamento: ParcelamentoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.parcelamento.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const parcelamentoPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: ParcelamentoDeletePopupComponent,
        resolve: {
            parcelamento: ParcelamentoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.parcelamento.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
