import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IParcelamento } from 'app/shared/model/parcelamento.model';
import { ParcelamentoService } from './parcelamento.service';
import { IPlano } from 'app/shared/model/plano.model';
import { PlanoService } from 'app/entities/plano';

@Component({
    selector: 'jhi-parcelamento-update',
    templateUrl: './parcelamento-update.component.html'
})
export class ParcelamentoUpdateComponent implements OnInit {
    parcelamento: IParcelamento;
    isSaving: boolean;

    planos: IPlano[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected parcelamentoService: ParcelamentoService,
        protected planoService: PlanoService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ parcelamento }) => {
            this.parcelamento = parcelamento;
        });
        this.planoService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IPlano[]>) => mayBeOk.ok),
                map((response: HttpResponse<IPlano[]>) => response.body)
            )
            .subscribe((res: IPlano[]) => (this.planos = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.parcelamento.id !== undefined) {
            this.subscribeToSaveResponse(this.parcelamentoService.update(this.parcelamento));
        } else {
            this.subscribeToSaveResponse(this.parcelamentoService.create(this.parcelamento));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IParcelamento>>) {
        result.subscribe((res: HttpResponse<IParcelamento>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackPlanoById(index: number, item: IPlano) {
        return item.id;
    }
}
