import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IParcelamento } from 'app/shared/model/parcelamento.model';

@Component({
    selector: 'jhi-parcelamento-detail',
    templateUrl: './parcelamento-detail.component.html'
})
export class ParcelamentoDetailComponent implements OnInit {
    parcelamento: IParcelamento;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ parcelamento }) => {
            this.parcelamento = parcelamento;
        });
    }

    previousState() {
        window.history.back();
    }
}
