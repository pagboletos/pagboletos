import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IDocumentos } from 'app/shared/model/documentos.model';

@Component({
    selector: 'jhi-documentos-detail',
    templateUrl: './documentos-detail.component.html'
})
export class DocumentosDetailComponent implements OnInit {
    documentos: IDocumentos;

    constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ documentos }) => {
            this.documentos = documentos;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }
}
