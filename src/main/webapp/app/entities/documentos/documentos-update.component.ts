import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { IDocumentos } from 'app/shared/model/documentos.model';
import { DocumentosService } from './documentos.service';
import { IVendedor } from 'app/shared/model/vendedor.model';
import { VendedorService } from 'app/entities/vendedor';

@Component({
    selector: 'jhi-documentos-update',
    templateUrl: './documentos-update.component.html'
})
export class DocumentosUpdateComponent implements OnInit {
    documentos: IDocumentos;
    isSaving: boolean;

    vendedors: IVendedor[];

    constructor(
        protected dataUtils: JhiDataUtils,
        protected jhiAlertService: JhiAlertService,
        protected documentosService: DocumentosService,
        protected vendedorService: VendedorService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ documentos }) => {
            this.documentos = documentos;
        });
        this.vendedorService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IVendedor[]>) => mayBeOk.ok),
                map((response: HttpResponse<IVendedor[]>) => response.body)
            )
            .subscribe((res: IVendedor[]) => (this.vendedors = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.documentos.id !== undefined) {
            this.subscribeToSaveResponse(this.documentosService.update(this.documentos));
        } else {
            this.subscribeToSaveResponse(this.documentosService.create(this.documentos));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IDocumentos>>) {
        result.subscribe((res: HttpResponse<IDocumentos>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackVendedorById(index: number, item: IVendedor) {
        return item.id;
    }
}
