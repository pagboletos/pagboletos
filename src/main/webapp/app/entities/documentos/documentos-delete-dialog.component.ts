import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDocumentos } from 'app/shared/model/documentos.model';
import { DocumentosService } from './documentos.service';

@Component({
    selector: 'jhi-documentos-delete-dialog',
    templateUrl: './documentos-delete-dialog.component.html'
})
export class DocumentosDeleteDialogComponent {
    documentos: IDocumentos;

    constructor(
        protected documentosService: DocumentosService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.documentosService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'documentosListModification',
                content: 'Deleted an documentos'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-documentos-delete-popup',
    template: ''
})
export class DocumentosDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ documentos }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(DocumentosDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.documentos = documentos;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/documentos', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/documentos', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
