import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Documentos, IDocumentos } from 'app/shared/model/documentos.model';
import { DocumentosService } from './documentos.service';
import { DocumentosComponent } from './documentos.component';
import { DocumentosDetailComponent } from './documentos-detail.component';
import { DocumentosUpdateComponent } from './documentos-update.component';
import { DocumentosDeletePopupComponent } from './documentos-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class DocumentosResolve implements Resolve<IDocumentos> {
    constructor(private service: DocumentosService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDocumentos> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Documentos>) => response.ok),
                map((documentos: HttpResponse<Documentos>) => documentos.body)
            );
        }
        return of(new Documentos());
    }
}

export const documentosRoute: Routes = [
    {
        path: '',
        component: DocumentosComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.documentos.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: DocumentosDetailComponent,
        resolve: {
            documentos: DocumentosResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.documentos.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: DocumentosUpdateComponent,
        resolve: {
            documentos: DocumentosResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.documentos.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: DocumentosUpdateComponent,
        resolve: {
            documentos: DocumentosResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.documentos.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const documentosPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: DocumentosDeletePopupComponent,
        resolve: {
            documentos: DocumentosResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.documentos.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
