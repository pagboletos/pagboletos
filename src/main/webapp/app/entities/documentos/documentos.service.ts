import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDocumentos } from 'app/shared/model/documentos.model';

type EntityResponseType = HttpResponse<IDocumentos>;
type EntityArrayResponseType = HttpResponse<IDocumentos[]>;

@Injectable({ providedIn: 'root' })
export class DocumentosService {
    public resourceUrl = SERVER_API_URL + 'api/documentos';

    constructor(protected http: HttpClient) {}

    create(documentos: IDocumentos): Observable<EntityResponseType> {
        return this.http.post<IDocumentos>(this.resourceUrl, documentos, { observe: 'response' });
    }

    update(documentos: IDocumentos): Observable<EntityResponseType> {
        return this.http.put<IDocumentos>(this.resourceUrl, documentos, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IDocumentos>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IDocumentos[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
