export * from './documentos.service';
export * from './documentos-update.component';
export * from './documentos-delete-dialog.component';
export * from './documentos-detail.component';
export * from './documentos.component';
export * from './documentos.route';
