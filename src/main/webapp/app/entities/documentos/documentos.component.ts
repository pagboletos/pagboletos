import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiDataUtils, JhiEventManager } from 'ng-jhipster';

import { IDocumentos } from 'app/shared/model/documentos.model';
import { AccountService } from 'app/core';
import { DocumentosService } from './documentos.service';

@Component({
    selector: 'jhi-documentos',
    templateUrl: './documentos.component.html'
})
export class DocumentosComponent implements OnInit, OnDestroy {
    documentos: IDocumentos[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected documentosService: DocumentosService,
        protected jhiAlertService: JhiAlertService,
        protected dataUtils: JhiDataUtils,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.documentosService
            .query()
            .pipe(
                filter((res: HttpResponse<IDocumentos[]>) => res.ok),
                map((res: HttpResponse<IDocumentos[]>) => res.body)
            )
            .subscribe(
                (res: IDocumentos[]) => {
                    this.documentos = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInDocumentos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IDocumentos) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    registerChangeInDocumentos() {
        this.eventSubscriber = this.eventManager.subscribe('documentosListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
