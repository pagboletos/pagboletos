import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PagboletosSharedModule } from 'app/shared';
import {
    DocumentosComponent,
    DocumentosDeleteDialogComponent,
    DocumentosDeletePopupComponent,
    DocumentosDetailComponent,
    documentosPopupRoute,
    documentosRoute,
    DocumentosUpdateComponent
} from './';

const ENTITY_STATES = [...documentosRoute, ...documentosPopupRoute];

@NgModule({
    imports: [PagboletosSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        DocumentosComponent,
        DocumentosDetailComponent,
        DocumentosUpdateComponent,
        DocumentosDeleteDialogComponent,
        DocumentosDeletePopupComponent
    ],
    entryComponents: [DocumentosComponent, DocumentosUpdateComponent, DocumentosDeleteDialogComponent, DocumentosDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PagboletosDocumentosModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
