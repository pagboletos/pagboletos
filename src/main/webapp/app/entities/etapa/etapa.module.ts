import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PagboletosSharedModule } from 'app/shared';
import {
    EtapaComponent,
    EtapaDeleteDialogComponent,
    EtapaDeletePopupComponent,
    EtapaDetailComponent,
    etapaPopupRoute,
    etapaRoute,
    EtapaUpdateComponent
} from './';

const ENTITY_STATES = [...etapaRoute, ...etapaPopupRoute];

@NgModule({
    imports: [PagboletosSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [EtapaComponent, EtapaDetailComponent, EtapaUpdateComponent, EtapaDeleteDialogComponent, EtapaDeletePopupComponent],
    entryComponents: [EtapaComponent, EtapaUpdateComponent, EtapaDeleteDialogComponent, EtapaDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PagboletosEtapaModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
