import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Etapa, IEtapa } from 'app/shared/model/etapa.model';
import { EtapaService } from './etapa.service';
import { EtapaComponent } from './etapa.component';
import { EtapaDetailComponent } from './etapa-detail.component';
import { EtapaUpdateComponent } from './etapa-update.component';
import { EtapaDeletePopupComponent } from './etapa-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class EtapaResolve implements Resolve<IEtapa> {
    constructor(private service: EtapaService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IEtapa> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Etapa>) => response.ok),
                map((etapa: HttpResponse<Etapa>) => etapa.body)
            );
        }
        return of(new Etapa());
    }
}

export const etapaRoute: Routes = [
    {
        path: '',
        component: EtapaComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.etapa.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: EtapaDetailComponent,
        resolve: {
            etapa: EtapaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.etapa.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: EtapaUpdateComponent,
        resolve: {
            etapa: EtapaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.etapa.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: EtapaUpdateComponent,
        resolve: {
            etapa: EtapaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.etapa.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const etapaPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: EtapaDeletePopupComponent,
        resolve: {
            etapa: EtapaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagboletosApp.etapa.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
