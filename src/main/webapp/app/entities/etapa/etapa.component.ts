import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { IEtapa } from 'app/shared/model/etapa.model';
import { AccountService } from 'app/core';
import { EtapaService } from './etapa.service';

@Component({
    selector: 'jhi-etapa',
    templateUrl: './etapa.component.html'
})
export class EtapaComponent implements OnInit, OnDestroy {
    etapas: IEtapa[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected etapaService: EtapaService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.etapaService
            .query()
            .pipe(
                filter((res: HttpResponse<IEtapa[]>) => res.ok),
                map((res: HttpResponse<IEtapa[]>) => res.body)
            )
            .subscribe(
                (res: IEtapa[]) => {
                    this.etapas = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInEtapas();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IEtapa) {
        return item.id;
    }

    registerChangeInEtapas() {
        this.eventSubscriber = this.eventManager.subscribe('etapaListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
