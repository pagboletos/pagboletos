import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IEtapa } from 'app/shared/model/etapa.model';
import { EtapaService } from './etapa.service';
import { IPagamento } from 'app/shared/model/pagamento.model';
import { PagamentoService } from 'app/entities/pagamento';

@Component({
    selector: 'jhi-etapa-update',
    templateUrl: './etapa-update.component.html'
})
export class EtapaUpdateComponent implements OnInit {
    etapa: IEtapa;
    isSaving: boolean;

    pagamentos: IPagamento[];
    dataEfetuacaoDp: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected etapaService: EtapaService,
        protected pagamentoService: PagamentoService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ etapa }) => {
            this.etapa = etapa;
        });
        this.pagamentoService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IPagamento[]>) => mayBeOk.ok),
                map((response: HttpResponse<IPagamento[]>) => response.body)
            )
            .subscribe((res: IPagamento[]) => (this.pagamentos = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.etapa.id !== undefined) {
            this.subscribeToSaveResponse(this.etapaService.update(this.etapa));
        } else {
            this.subscribeToSaveResponse(this.etapaService.create(this.etapa));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IEtapa>>) {
        result.subscribe((res: HttpResponse<IEtapa>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackPagamentoById(index: number, item: IPagamento) {
        return item.id;
    }
}
