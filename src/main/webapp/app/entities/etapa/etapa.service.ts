import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IEtapa } from 'app/shared/model/etapa.model';

type EntityResponseType = HttpResponse<IEtapa>;
type EntityArrayResponseType = HttpResponse<IEtapa[]>;

@Injectable({ providedIn: 'root' })
export class EtapaService {
    public resourceUrl = SERVER_API_URL + 'api/etapas';

    constructor(protected http: HttpClient) {}

    create(etapa: IEtapa): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(etapa);
        return this.http
            .post<IEtapa>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(etapa: IEtapa): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(etapa);
        return this.http
            .put<IEtapa>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IEtapa>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IEtapa[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(etapa: IEtapa): IEtapa {
        const copy: IEtapa = Object.assign({}, etapa, {
            dataEfetuacao: etapa.dataEfetuacao != null && etapa.dataEfetuacao.isValid() ? etapa.dataEfetuacao.format(DATE_FORMAT) : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.dataEfetuacao = res.body.dataEfetuacao != null ? moment(res.body.dataEfetuacao) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((etapa: IEtapa) => {
                etapa.dataEfetuacao = etapa.dataEfetuacao != null ? moment(etapa.dataEfetuacao) : null;
            });
        }
        return res;
    }
}
