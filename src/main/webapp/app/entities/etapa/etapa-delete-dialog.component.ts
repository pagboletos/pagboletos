import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IEtapa } from 'app/shared/model/etapa.model';
import { EtapaService } from './etapa.service';

@Component({
    selector: 'jhi-etapa-delete-dialog',
    templateUrl: './etapa-delete-dialog.component.html'
})
export class EtapaDeleteDialogComponent {
    etapa: IEtapa;

    constructor(protected etapaService: EtapaService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.etapaService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'etapaListModification',
                content: 'Deleted an etapa'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-etapa-delete-popup',
    template: ''
})
export class EtapaDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ etapa }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(EtapaDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.etapa = etapa;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/etapa', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/etapa', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
