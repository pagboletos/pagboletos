import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEtapa } from 'app/shared/model/etapa.model';

@Component({
    selector: 'jhi-etapa-detail',
    templateUrl: './etapa-detail.component.html'
})
export class EtapaDetailComponent implements OnInit {
    etapa: IEtapa;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ etapa }) => {
            this.etapa = etapa;
        });
    }

    previousState() {
        window.history.back();
    }
}
