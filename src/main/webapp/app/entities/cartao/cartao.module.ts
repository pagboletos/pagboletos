import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PagboletosSharedModule } from 'app/shared';
import {
    CartaoComponent,
    CartaoDeleteDialogComponent,
    CartaoDeletePopupComponent,
    CartaoDetailComponent,
    cartaoPopupRoute,
    cartaoRoute,
    CartaoUpdateComponent
} from './';

const ENTITY_STATES = [...cartaoRoute, ...cartaoPopupRoute];

@NgModule({
    imports: [PagboletosSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [CartaoComponent, CartaoDetailComponent, CartaoUpdateComponent, CartaoDeleteDialogComponent, CartaoDeletePopupComponent],
    entryComponents: [CartaoComponent, CartaoUpdateComponent, CartaoDeleteDialogComponent, CartaoDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PagboletosCartaoModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
