import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { ICartao } from 'app/shared/model/cartao.model';
import { AccountService } from 'app/core';
import { CartaoService } from './cartao.service';

@Component({
    selector: 'jhi-cartao',
    templateUrl: './cartao.component.html'
})
export class CartaoComponent implements OnInit, OnDestroy {
    cartaos: ICartao[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected cartaoService: CartaoService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.cartaoService
            .query()
            .pipe(
                filter((res: HttpResponse<ICartao[]>) => res.ok),
                map((res: HttpResponse<ICartao[]>) => res.body)
            )
            .subscribe(
                (res: ICartao[]) => {
                    this.cartaos = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInCartaos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ICartao) {
        return item.id;
    }

    registerChangeInCartaos() {
        this.eventSubscriber = this.eventManager.subscribe('cartaoListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
