import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ICartao } from 'app/shared/model/cartao.model';
import { CartaoService } from './cartao.service';
import { IVendedor } from 'app/shared/model/vendedor.model';
import { VendedorService } from 'app/entities/vendedor';

@Component({
    selector: 'jhi-cartao-update',
    templateUrl: './cartao-update.component.html'
})
export class CartaoUpdateComponent implements OnInit {
    cartao: ICartao;
    isSaving: boolean;

    vendedors: IVendedor[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected cartaoService: CartaoService,
        protected vendedorService: VendedorService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ cartao }) => {
            this.cartao = cartao;
        });
        this.vendedorService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IVendedor[]>) => mayBeOk.ok),
                map((response: HttpResponse<IVendedor[]>) => response.body)
            )
            .subscribe((res: IVendedor[]) => (this.vendedors = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.cartao.id !== undefined) {
            this.subscribeToSaveResponse(this.cartaoService.update(this.cartao));
        } else {
            this.subscribeToSaveResponse(this.cartaoService.create(this.cartao));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ICartao>>) {
        result.subscribe((res: HttpResponse<ICartao>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackVendedorById(index: number, item: IVendedor) {
        return item.id;
    }
}
